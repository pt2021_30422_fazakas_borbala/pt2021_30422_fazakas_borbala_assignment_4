import BLL.DeliveryService.DeliveryService;
import BLL.DeliveryService.IDeliveryService;
import Presenter.Login.LoginPaneController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

/**
 * Starter class of the JavaFX application.
 */
public class MainApp extends Application {
    private AnchorPane root;
    private Scene scene;
    private DeliveryService deliveryService;

    /** Method called when the application is started. */
    @Override
    public void start(Stage primaryStage) throws Exception {
        deliveryService = new DeliveryService();
        loadRoot();

        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Restaurant");
        primaryStage.show();
    }

    private void  loadRoot() throws IOException {
        FXMLLoader mainStageLoader = new FXMLLoader(getClass().getResource("/login_pane.fxml"));
        root = mainStageLoader.load();
        LoginPaneController controller = mainStageLoader.getController();
        controller.initDeliveryService(deliveryService);
        controller.showEmployeeWindow();
        root.setPadding(new Insets(10, 10, 10, 10));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
