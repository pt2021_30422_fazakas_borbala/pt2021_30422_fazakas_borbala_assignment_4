package BLL.Filters;

import Model.Products.MenuItem;

/**
 * NoComponentFilter filters out menu items which contain as a direct or indirect component the
 * menu item with the specified name.
 */
public class NoComponentFilter implements MenuItemFilter {
    private final String componentName;

    /**
     * Constructs the filter.
     *
     * @param componentName is the name of the menu item which should not be component, directly
     *                      or indirectly, of the filtered product. If it is a component, then
     *                      the product is filtered out.
     */
    public NoComponentFilter(String componentName) {
        this.componentName = componentName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return !item.contains(componentName);
    }
}