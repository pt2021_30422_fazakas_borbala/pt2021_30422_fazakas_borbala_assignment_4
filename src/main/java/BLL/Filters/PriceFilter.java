package BLL.Filters;

import Model.Products.MenuItem;

/**
 * PriceFilter filters out menu items which cost less or more than the values
 * specified as limits.
 */
public class PriceFilter implements MenuItemFilter {
    private final double minPrice;
    private final double maxPrice;

    /**
     * Constructs the filter.
     *
     * @param minPrice is the min. requested price. Items with a lower price
     *                 are filtered out.
     * @param maxPrice is the max. requested price. Items with a higher price
     *                 are filtered out.
     */
    public PriceFilter(double minPrice, double maxPrice) {
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.computePrice() >= minPrice && item.computePrice() <= maxPrice;
    }
}
