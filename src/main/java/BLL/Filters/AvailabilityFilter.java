package BLL.Filters;

import Model.Products.MenuItem;

/**
 * AvailabilityFilter filters out menu items which don't have the same availability status as the
 * specified parameter.
 */
public class AvailabilityFilter implements MenuItemFilter {
    private final boolean available;

    /**
     * Constructs the filter.
     *
     * @param available is the requested availability. Items with another availability are
     *                  filtered out.
     */
    public AvailabilityFilter(boolean available) {
        this.available = available;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.isAvailable() == available;
    }
}
