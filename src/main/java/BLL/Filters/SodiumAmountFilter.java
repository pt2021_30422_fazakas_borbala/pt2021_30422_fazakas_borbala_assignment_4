package BLL.Filters;

import Model.Products.MenuItem;

/**
 * SodiumAmountFilter filters out menu items which contain less or more sodium than the values
 * specified as limits.
 */
public class SodiumAmountFilter implements MenuItemFilter {
    private final int minSodiumAmount;
    private final int maxSodiumAmount;

    /**
     * Constructs the filter.
     *
     * @param minSodiumAmount is the min. requested sodium amount. Items with a lower sodium level
     *                        are filtered out.
     * @param maxSodiumAmount is the max. requested sodium amount. Items with a higher sodium
     *                        level are filtered out.
     */
    public SodiumAmountFilter(int minSodiumAmount, int maxSodiumAmount) {
        this.minSodiumAmount = minSodiumAmount;
        this.maxSodiumAmount = maxSodiumAmount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.getSodiumAmount() >= minSodiumAmount && item.getSodiumAmount() <= maxSodiumAmount;
    }
}
