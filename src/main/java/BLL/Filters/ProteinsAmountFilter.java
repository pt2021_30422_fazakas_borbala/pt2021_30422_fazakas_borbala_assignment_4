package BLL.Filters;

import Model.Products.MenuItem;

/**
 * ProteinsAmountFilter filters out menu items which contain less or more proteins than the values
 * specified as limits.
 */
public class ProteinsAmountFilter implements MenuItemFilter {
    private final int minProteinsAmount;
    private final int maxProteinsAmount;

    /**
     * Constructs the filter.
     *
     * @param minProteinsAmount is the min. requested proteins amount. Items with a lower protein
     *                          level are filtered out.
     * @param maxProteinsAmount is the max. requested proteins amount. Items with a higher protein
     *                          level are filtered out.
     */
    public ProteinsAmountFilter(int minProteinsAmount, int maxProteinsAmount) {
        this.minProteinsAmount = minProteinsAmount;
        this.maxProteinsAmount = maxProteinsAmount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.getProteinsAmount() >= minProteinsAmount && item.getProteinsAmount() <= maxProteinsAmount;
    }
}
