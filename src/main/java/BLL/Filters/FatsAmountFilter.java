package BLL.Filters;

import Model.Products.MenuItem;

/**
 * FatAmountFilter filters out menu items which contain less or more fat than the values
 * specified as limits.
 */
public class FatsAmountFilter implements MenuItemFilter {
    private final int minFatsAmount;
    private final int maxFatsAmount;

    /**
     * Constructs the filter.
     *
     * @param minFatsAmount is the min. requested fats amount. Items with a lower fat amount
     *                      are filtered out.
     * @param maxFatsAmount is the max. requested fats amount. Items with a higher fat amount
     *                      are filtered out.
     */
    public FatsAmountFilter(int minFatsAmount, int maxFatsAmount) {
        this.minFatsAmount = minFatsAmount;
        this.maxFatsAmount = maxFatsAmount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.getFatsAmount() >= minFatsAmount && item.getFatsAmount() <= maxFatsAmount;
    }
}
