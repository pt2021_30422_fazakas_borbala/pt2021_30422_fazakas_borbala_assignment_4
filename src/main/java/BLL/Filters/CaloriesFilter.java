package BLL.Filters;

import Model.Products.MenuItem;

/**
 * CaloriesFilter filters out menu items which contain less or more calories than the values
 * specified as limits.
 */
public class CaloriesFilter implements MenuItemFilter {
    private final int minCalories;
    private final int maxCalories;

    /**
     * Constructs the filter.
     *
     * @param minCalories is the min. requested calories amount. Items with a lower calorie level
     *                    are filtered out.
     * @param maxCalories is the max. requested calories amount. Items with a higher calorie level
     *                    are filtered out.
     */
    public CaloriesFilter(int minCalories, int maxCalories) {
        this.minCalories = minCalories;
        this.maxCalories = maxCalories;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.getCalories() >= minCalories && item.getCalories() <= maxCalories;
    }
}
