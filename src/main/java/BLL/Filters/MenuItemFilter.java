package BLL.Filters;

import Model.Products.MenuItem;

/**
 * Interface for filtering menu items by a specific attribute.
 */
@FunctionalInterface
public interface MenuItemFilter {
    /**
     * Returns true if and only if the menu item fulfills the filtering criteria.
     *
     * @param item is the item to filter.
     * @return true if the item fulfills the filtering criteria, false otherwise.
     */
    boolean apply(MenuItem item);
}
