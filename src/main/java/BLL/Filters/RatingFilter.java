package BLL.Filters;

import Model.Products.MenuItem;

/**
 * RatingFilter filters out menu items which have a rating lower than the specified threshold.
 */
public class RatingFilter implements MenuItemFilter {
    private final double minRating;

    /**
     * Constructs the filter.
     *
     * @param minRating is the min. requested rating. Items with a lower rating
     *                  are filtered out.
     */
    public RatingFilter(double minRating) {
        this.minRating = minRating;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.getRating().isPresent() && item.getRating().get() >= minRating;
    }
}
