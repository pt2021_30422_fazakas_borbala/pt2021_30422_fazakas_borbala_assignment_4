package BLL.Filters;

import Model.Products.MenuItem;

import java.util.Locale;

/**
 * KeywordFilter filters out menu items which don't contain in their title the specified keyword.
 */
public class KeywordFilter implements MenuItemFilter {
    private final String keyword;

    /**
     * Constructs the filter.
     *
     * @param keyword is the word(substring) which needs to be contained in the title of the
     *                menuItem. If not contained, the menuItem will be filtered out.
     */
    public KeywordFilter(String keyword) {
        this.keyword = keyword;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean apply(MenuItem item) {
        return item.getTitle().toLowerCase(Locale.ROOT).contains(keyword.toLowerCase(Locale.ROOT));
    }
}
