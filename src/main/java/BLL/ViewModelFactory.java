package BLL;

import Model.Orders.Order;
import Model.Products.CompositeProduct;
import Model.Products.MenuItem;
import Model.Users.User;
import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.AdminView.ReportViewModels.ClientToNoExpensiveOrdersViewModel;
import Presenter.AdminView.ReportViewModels.MenuItemToNoOrdersViewModel;
import Presenter.AdminView.ReportViewModels.OrderViewModel;
import Presenter.ClientView.CartItemClientViewModel;
import Presenter.EmployeeView.OrderEmployeeViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Builds view models from bll models.
 * Remark that the BLL never provides bll models to the presenter, in order not to expose the
 * data to possible safety threats.
 */
public class ViewModelFactory {
    /**
     * Builds a client view model for a menu item.
     * @param menuItem is the entity to convert.
     * @return the view model corresponding to the given menuItem.
     */
    public static CartItemClientViewModel getMenuItemClientViewModel(MenuItem menuItem) {
        List<String> components = new ArrayList<>();
        if (menuItem instanceof CompositeProduct) {
            components.addAll(((CompositeProduct) menuItem).getComponents().stream().map(MenuItem::getTitle).collect(Collectors.toList()));
        }
        return new CartItemClientViewModel(menuItem.getTitle(),
                                            menuItem.computePrice(),
                menuItem.getProteinsAmount(),
                menuItem.getFatsAmount(),
                menuItem.getSodiumAmount(),
                menuItem.getCalories(),
                menuItem.getRating().orElse(null),
                0,
                components);
    }

    /**
     * Builds a admin view model for a menu item.
     * @param menuItem is the entity to convert.
     * @return the view model corresponding to the given menuItem.
     */
    public static MenuItemAdminViewModel getMenuItemAdminViewModel(MenuItem menuItem) {
        List<String> components = new ArrayList<>();
        if (menuItem instanceof CompositeProduct) {
            components.addAll(((CompositeProduct) menuItem).getComponents().stream().map(MenuItem::getTitle).collect(Collectors.toList()));
        }
        return new MenuItemAdminViewModel(menuItem.getTitle(),
                menuItem.computePrice(),
                menuItem.getProteinsAmount(),
                menuItem.getFatsAmount(),
                menuItem.getSodiumAmount(),
                menuItem.getCalories(),
                menuItem.getRating().orElse(null),
                components,
                menuItem.isAvailable());
    }

    /**
     * Builds a employee view model for an order.
     * @param order is the entity to convert.
     * @param client is the user who places the order.
     * @param orderedProductsToAmount is the map of the ordered products to the ordered amount.
     * @return the view model corresponding to the given order.
     */
    public static OrderEmployeeViewModel getOrderEmployeeViewModel(Order order, User client,
                                                                   Map<MenuItem, Integer> orderedProductsToAmount) {
       return new OrderEmployeeViewModel(client.getUserName(), order.getDateTime(),
               orderedProductsToAmount.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey().getTitle(), Map.Entry::getValue)));
    }

    /**
     * Builds an admin view model for an order.
     * @param order is the entity to convert.
     * @param client is the user who places the order.
     * @param orderedItemsToQuantity is the map of the ordered products to the ordered amount.
     * @return the view model corresponding to the given order.
     */
    public static OrderViewModel getOrderViewModel(Order order, User client, Map<MenuItem,
            Integer> orderedItemsToQuantity) {
        return new OrderViewModel(order.getOrderId(), client.getUserName(), order.getDateTime(),
                Order.computePrice(orderedItemsToQuantity));
    }

    /**
     * Builds a employee view model for a menu item, together with the number of times it was
     * ordered.
     * @param menuItem os the item to convert.
     * @param noOrders is the number of times ot was ordered.
     * @return the view model corresponding to menuItem.
     */
    public static MenuItemToNoOrdersViewModel getMenuItemToNoOrdersViewModel(MenuItem menuItem,
                                                                             int noOrders) {
        return new MenuItemToNoOrdersViewModel(menuItem.getTitle(),
                menuItem.computePrice(),
                menuItem.getCalories(),
                menuItem.getProteinsAmount(),
                menuItem.getFatsAmount(),
                menuItem.getSodiumAmount(),
                menuItem.getRating().orElse(null),
                noOrders);
    }

    /**
     * Builds a view model for a client, together with the number of times it they placed an
     * expensive order.
     * @param client os the user to convert.
     * @param noExpensiveOrders is the number of times they places an expensive order.
     * @return the view model corresponding to client.
     */
    public static ClientToNoExpensiveOrdersViewModel getClientToNoExpensiveOrdersViewModel(User client, long noExpensiveOrders) {
        return new ClientToNoExpensiveOrdersViewModel(client.getUserName(), noExpensiveOrders);
    }
}
