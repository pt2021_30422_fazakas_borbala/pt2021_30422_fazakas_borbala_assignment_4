package BLL.Validators.CompositeProductValidator;

import BLL.Validators.ValidationResult;
import Model.Products.CompositeProduct;

/**
 * Holds the result of the validation of composite product data.
 */
public enum CompositeProductValidationResult implements ValidationResult<CompositeProduct> {
    VALID(true, ""),
    EMPTY_MENU_ITEM_NAME(false, "The name of a menu item cannot be empty");

    private final boolean valid;
    private final String errorMessage;

    CompositeProductValidationResult(boolean valid, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid() {
        return valid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
