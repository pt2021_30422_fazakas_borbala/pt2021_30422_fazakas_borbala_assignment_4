package BLL.Validators.CompositeProductValidator;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.CompositeProduct;

/**
 * Validates the title field of a composite product.
 */
public class CompositeProductTitleValidator implements EntityValidator<CompositeProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<CompositeProduct> validate(CompositeProduct entity) {
        if (entity.getTitle() == null || entity.getTitle().isEmpty()) {
            return CompositeProductValidationResult.EMPTY_MENU_ITEM_NAME;
        }
        return CompositeProductValidationResult.VALID;
    }
}
