package BLL.Validators.CompositeProductValidator;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.CompositeProduct;

import java.util.List;

/**
 * Validates a composite product for all its attributes.
 */
public class CompositeProductValidator implements EntityValidator<CompositeProduct> {
    private final List<EntityValidator<CompositeProduct>> validators =
            List.of(new CompositeProductTitleValidator());

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<CompositeProduct> validate(CompositeProduct entity) {
        for (EntityValidator<CompositeProduct> validator : validators) {
            ValidationResult<CompositeProduct> validationResult = validator.validate(entity);
            if (!validationResult.isValid()) {
                return validationResult;
            }
        }
        return CompositeProductValidationResult.VALID;
    }

    /**
     * Validates the data of a composite product.
     *
     * @param title     is the title of the product.
     * @param available is the availability status of the product.
     * @return the result of the validation.
     */
    public ValidationResult<CompositeProduct> validate(String title, boolean available) {
        return validate(new CompositeProduct(0, title, null, available, List.of()));
    }
}
