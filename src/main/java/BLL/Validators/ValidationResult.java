package BLL.Validators;

/**
 * Stores the result of the validation.
 *
 * @param <T> is the type of the validated entity.
 */
public interface ValidationResult<T> {
    /**
     * Shows whether the validated entity was valid.
     *
     * @return true if and only if the validated entity is valid.
     */
    boolean isValid();

    /**
     * Specifies a message explaining the reason the validated entity was considered incorrect.
     *
     * @return the error message.
     */
    String getErrorMessage();
}
