package BLL.Validators;

/**
 * Validates an entity.
 *
 * @param <T> is the type of the validated entity.
 */
public interface EntityValidator<T> {
    /**
     * Validates the entity to check whether its data is correct.
     *
     * @param entity is the validated entity.
     * @return the result of the validation.
     */
    ValidationResult<T> validate(T entity);
}
