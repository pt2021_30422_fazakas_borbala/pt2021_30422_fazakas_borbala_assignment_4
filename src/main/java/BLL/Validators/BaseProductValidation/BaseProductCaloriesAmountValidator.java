package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the calories amount field of a base product.
 */
public class BaseProductCaloriesAmountValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.getCalories() <= 0) {
            return BaseProductValidationResult.NEGATIVE_CALORIES_AMOUNT;
        }
        return BaseProductValidationResult.VALID;
    }
}
