package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the proteins amount field of a base product.
 */
public class BaseProductProteinsAmountValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.getProteinsAmount() <= 0) {
            return BaseProductValidationResult.NEGATIVE_PROTEINS_AMOUNT;
        }
        return BaseProductValidationResult.VALID;
    }
}