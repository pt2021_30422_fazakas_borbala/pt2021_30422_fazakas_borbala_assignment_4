package BLL.Validators.BaseProductValidation;

import BLL.DeliveryService.DeliveryServiceProductManager;
import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the rating field of a base product.
 */
public class BaseProductRatingValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.getRating().isPresent() && (entity.getRating().get() < 0 || entity.getRating().get() > DeliveryServiceProductManager.MAX_RATING)) {
            return BaseProductValidationResult.NEGATIVE_CALORIES_AMOUNT;
        }
        return BaseProductValidationResult.VALID;
    }
}