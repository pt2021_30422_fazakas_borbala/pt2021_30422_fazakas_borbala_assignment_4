package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the title field of a base product.
 */
public class BaseProductTitleValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.getTitle() == null || entity.getTitle().isEmpty()) {
            return BaseProductValidationResult.EMPTY_MENU_ITEM_NAME;
        }
        return BaseProductValidationResult.VALID;
    }
}
