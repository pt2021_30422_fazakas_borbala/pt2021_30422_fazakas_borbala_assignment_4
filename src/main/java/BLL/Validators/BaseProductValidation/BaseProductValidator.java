package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

import java.util.List;

/**
 * Validates a user for all its attributes.
 */
public class BaseProductValidator implements EntityValidator<BaseProduct> {
    private final List<EntityValidator<BaseProduct>> validators = List.of(new BaseProductTitleValidator(),
            new BaseproductPriceValidator(),
            new BaseProductCaloriesAmountValidator(),
            new BaseProductFatsAmountValidator(),
            new BaseProductProteinsAmountValidator(),
            new BaseProductSodiumAmountValidator(),
            new BaseProductRatingValidator());

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        for (EntityValidator<BaseProduct> validator : validators) {
            ValidationResult<BaseProduct> validationResult = validator.validate(entity);
            if (!validationResult.isValid()) {
                return validationResult;
            }
        }
        return BaseProductValidationResult.VALID;
    }

    /**
     * Validates the data of a base product.
     *
     * @param title          is the name of the base product.
     * @param calories       specifies the number of calories in one portion of the product.
     * @param proteinsAmount specifies the amount of proteins in one portion of the product.
     * @param fatAmount      specifies the amount of fat in one portion of the product.
     * @param sodiumAmount   specifies the amount of sodium in one portion of the product.
     * @param price          specifies cost of buying one portion of the product.
     * @param available      specified whether the product is available to customers.
     * @return the validation result.
     */
    public ValidationResult<BaseProduct> validate(String title, int calories, int proteinsAmount, int fatAmount,
                                                  int sodiumAmount, double price, boolean available) {
        return validate(new BaseProduct(1, title, null, calories, proteinsAmount, fatAmount,
                sodiumAmount, price, available));
    }
}
