package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the fats amount field of a base product.
 */
public class BaseProductFatsAmountValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.getFatsAmount() <= 0) {
            return BaseProductValidationResult.NEGATIVE_FATS_AMOUNT;
        }
        return BaseProductValidationResult.VALID;
    }
}
