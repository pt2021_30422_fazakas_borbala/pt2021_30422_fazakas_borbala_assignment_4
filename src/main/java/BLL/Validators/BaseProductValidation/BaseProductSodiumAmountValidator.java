package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the sodium amount field of a base product.
 */
public class BaseProductSodiumAmountValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.getSodiumAmount() <= 0) {
            return BaseProductValidationResult.NEGATIVE_SODIUM_AMOUNT;
        }
        return BaseProductValidationResult.VALID;
    }
}