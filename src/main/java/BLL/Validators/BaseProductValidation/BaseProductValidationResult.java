package BLL.Validators.BaseProductValidation;

import BLL.DeliveryService.DeliveryServiceProductManager;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Holds the validation result of base product data.
 */
public enum BaseProductValidationResult implements ValidationResult<BaseProduct> {
    VALID(true, ""),
    EMPTY_MENU_ITEM_NAME(false, "The name of a menu item cannot be empty"),
    NEGATIVE_PRICE(false, "The price of a product must be a positive value"),
    NEGATIVE_CALORIES_AMOUNT(false, "The amount of calories in a product must be a positive value"),
    NEGATIVE_PROTEINS_AMOUNT(false, "The amount of proteins in a product must be a positive value"),
    NEGATIVE_FATS_AMOUNT(false, "The amount of fats in a product must be a positive value"),
    NEGATIVE_SODIUM_AMOUNT(false, "The amount of sodium in a product must be a positive value"),
    INVALID_RATING_VALUE(false,
            "The rating of a product must be empty, or between 0 and " + DeliveryServiceProductManager.MAX_RATING);


    private final boolean valid;
    private final String errorMessage;

    BaseProductValidationResult(boolean valid, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid() {
        return valid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
