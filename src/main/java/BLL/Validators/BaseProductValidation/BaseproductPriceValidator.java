package BLL.Validators.BaseProductValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;

/**
 * Validates the price field of a base product.
 */
public class BaseproductPriceValidator implements EntityValidator<BaseProduct> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<BaseProduct> validate(BaseProduct entity) {
        if (entity.computePrice() <= 0) {
            return BaseProductValidationResult.NEGATIVE_PRICE;
        }
        return BaseProductValidationResult.VALID;
    }
}
