package BLL.Validators.UserValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Users.User;

/**
 * Validates the username field of a user.
 */
public class UsernameValidator implements EntityValidator<User> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<User> validate(User entity) {
        if (entity.getUserName() == null || entity.getUserName().equals("")) {
            return UserValidationResult.EMPTY_USERNAME;
        }
        return UserValidationResult.VALID;
    }
}
