package BLL.Validators.UserValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Users.User;

/**
 * Validates the password field of a user.
 */
public class PasswordValidator implements EntityValidator<User> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<User> validate(User entity) {
        if (entity.getPassword() == null || entity.getPassword().equals("")) {
            return UserValidationResult.EMPTY_PASSWORD;
        }
        return UserValidationResult.VALID;
    }
}
