package BLL.Validators.UserValidation;

import BLL.Validators.ValidationResult;
import Model.Users.User;

/**
 * Holds the result of the validation of user data.
 */
public enum UserValidationResult implements ValidationResult<User> {
    VALID("", true),
    EMPTY_USERNAME("Username cannot be empty", false),
    EMPTY_PASSWORD("Password cannot be empty", false),
    MISSING_TYPE("User's role must be specified", false);

    private final boolean valid;
    private final String errorMessage;

    UserValidationResult(String errorMessage, boolean valid) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid() {
        return valid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
