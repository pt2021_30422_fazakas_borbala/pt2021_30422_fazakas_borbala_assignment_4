package BLL.Validators.UserValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Users.User;

/**
 * Validates the type field of a user.
 */
public class UserTypeValidator implements EntityValidator<User> {
    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<User> validate(User entity) {
        if (entity.getType() == null) {
            return UserValidationResult.MISSING_TYPE;
        }
        return UserValidationResult.VALID;
    }
}
