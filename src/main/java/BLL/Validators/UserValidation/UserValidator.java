package BLL.Validators.UserValidation;

import BLL.Validators.EntityValidator;
import BLL.Validators.ValidationResult;
import Model.Users.User;

import java.util.List;

/**
 * Validates a user for all its attributes.
 */
public class UserValidator implements EntityValidator<User> {

    private final List<EntityValidator<User>> validators = List.of(new UsernameValidator(),
            new PasswordValidator(),
            new UserTypeValidator());

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult<User> validate(User entity) {
        for (EntityValidator<User> validator : validators) {
            ValidationResult<User> validationResult = validator.validate(entity);
            if (!validationResult.isValid()) {
                return validationResult;
            }
        }
        return UserValidationResult.VALID;
    }

    /**
     * Validates the data of a user.
     *
     * @param userName is the userName of the user.
     * @param password is the password of the user.
     * @return the result of the validation.
     */
    public ValidationResult<User> validate(String userName, String password) {
        return validate(new User(1, userName, password, User.UserType.CLIENT));
    }
}
