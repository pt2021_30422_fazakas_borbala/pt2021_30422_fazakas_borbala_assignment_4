package BLL.DeliveryService;

import BLL.Validators.UserValidation.UserValidator;
import DAL.Serialization.Serializer;
import Model.Users.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class responsible for managing user-related data for a general delivery service.
 */
public class DeliveryServiceUserManager {
    private final Map<String, User> namesToUsers;
    private User loggedInUser;
    private final UserValidator userValidator;

    /**
     * Initializes a DeliveryServiceUserManager by deserializing user-related data.
     */
    public DeliveryServiceUserManager() {
        namesToUsers = Serializer.deserializeUsers();
        userValidator = new UserValidator();
    }

    /**
     * If a user with the provided userName exists and they have the provided password, then they
     * log in, and the method returns true. Otherwise, the methor returns false.
     *
     * @param userName is the userName of the user to log in.
     * @param password is the assumed password provided by the user.
     * @return true is the login was successful, false otherwise.
     * @pre !existsLoggedInUser()
     * @post if !exists user with (userName, password), then !existsLoggedInUser(),
     * else existsLoggedInUser() and loggedInUser is the one with userName and password.
     */
    public boolean logIn(String userName, String password) {
        assert loggedInUser == null;
        User loggingUser = this.namesToUsers.get(userName);
        boolean loginStatus = false;
        if (loggingUser != null) {
            if (loggingUser.getPassword().equals(password)) {
                this.loggedInUser = loggingUser;
                loginStatus = true;
            }
        }
        assert ((namesToUsers.get(userName) == null || !namesToUsers.get(userName).getPassword().equals(password)) && loggedInUser == null) ||
                (loggedInUser != null && namesToUsers.containsValue(loggedInUser) && loggedInUser.getUserName().equals(userName) && loggingUser.getPassword().equals(password));
        return loginStatus;
    }

    Map<String, User> getNamesToUsers() {
        return namesToUsers;
    }

    /**
     * Specifies the type of the logged in user, if any.
     *
     * @return the type of the logged in user, if existsLoggedInUser(), else returnss an empty
     * optional.
     * @pre true.
     * @post @nochange.
     */
    public Optional<User.UserType> getLoggedInUserType() {
        if (loggedInUser != null) {
            return Optional.of(loggedInUser.getType());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Logs out the currently logged in user.
     *
     * @pre existsLoggedInUser()
     * @post !existsLoggedInUser()
     */
    public void logOut() {
        assert loggedInUser != null;
        this.loggedInUser = null;
        assert loggedInUser == null;
    }

    /**
     * Registers a new client with the specified data.
     *
     * @param userName is the userName of the newly registered client.
     * @param password is the password of the newly registered client.
     * @pre new UserValidator.validate(userName, password).isValid()
     * @pre !existsUserWithUsername()
     * @post exists a user with userName, password
     * @post number of users = @pre number of users + 1
     */
    public void registerClient(String userName, String password) {
        assert userValidator.validate(userName, password).isValid();
        assert !existsUserWithUsername(userName);
        int preNoUsers = namesToUsers.size();
        User newClient = new User(generateClientId(), userName, password, User.UserType.CLIENT);
        namesToUsers.put(newClient.getUserName(), newClient);
        assert preNoUsers + 1 == namesToUsers.size();
        assert namesToUsers.get(userName) != null && namesToUsers.get(userName).getPassword().equals(password);
    }

    /**
     * Returns true if a user with the specified userName exists, otherwise false.
     *
     * @param userName is the searched userName.
     * @return true if a user with the specified userName exists, otherwise false.
     * @pre true.
     * @post @nochange.
     */
    public boolean existsUserWithUsername(String userName) {
        return namesToUsers.get(userName) != null;
    }

    Optional<User> getUserWithId(int userId) {
        List<User> matchingUsers =
                namesToUsers.values().stream().filter(user -> user.getId() == userId).collect(Collectors.toList());
        if (matchingUsers.size() > 0) {
            return Optional.of(matchingUsers.get(0));
        } else {
            return Optional.empty();
        }
    }

    boolean isWellFormed() {
        return noNullValues() && correctUserNamesMapping() && usersHaveUniqueIds();
    }

    User getLoggedInUser() {
        return loggedInUser;
    }

    private boolean noNullValues() {
        return namesToUsers != null && !namesToUsers.containsValue(null);
    }

    private boolean correctUserNamesMapping() {
        return namesToUsers.entrySet().stream()
                .allMatch(entry -> entry.getKey().equals(entry.getValue().getUserName()));
    }

    private boolean usersHaveUniqueIds() {
        return namesToUsers.values().stream()
                .map(User::getId)
                .collect(Collectors.groupingBy(id -> id, Collectors.counting()))
                .entrySet()
                .stream().noneMatch(idToFrequency -> idToFrequency.getValue() > 1);
    }

    void saveState() {
        Serializer.serializeUsers(new ArrayList<>(namesToUsers.values()));
    }

    private int generateClientId() {
        int trialId;
        do {
            trialId = (int) (Math.random() * Integer.MAX_VALUE);
        } while (existsUserWithId(trialId));
        return trialId;
    }

    private boolean existsUserWithId(int id) {
        return namesToUsers.values().stream().anyMatch(user -> user.getId() == id);
    }
}
