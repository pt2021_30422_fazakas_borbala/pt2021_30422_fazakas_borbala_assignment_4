package BLL.DeliveryService;

import DAL.BillGeneration.BillGenerator;
import DAL.Serialization.Serializer;
import Model.Orders.Order;
import Model.Products.MenuItem;
import Model.Users.User;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class responsible for managing order-related data for a general delivery service.
 */
public class DeliveryServiceOrderManager {
    private final Map<Order, Map<MenuItem, Integer>> ordersToItemQuantity;

    /**
     * Initializes a DeliveryServiceOrderManager by deserializing order-related data.
     * @param namesToMenuItems is the map of the names of menu items to the actual menu items,
     *                         used to recreate the data of orders from the serialized files.
     */
    public DeliveryServiceOrderManager(Map<String, MenuItem> namesToMenuItems) {
        ordersToItemQuantity = Serializer.deserializeOrders(namesToMenuItems);
    }

    /**
     * Creates a new order with the specified items and ordered quantities, in the name of the
     * currently logged in user, who must be a client.
     *
     * @param orderItemNamesToQuantity specifies the ordered items and the ordered quantities.
     * @param user                     is the client who places the order.
     * @param existingItemNamesToItems is a map from the product names to the products in the menu.
     * @return the created order.
     * @pre orderItemNamesToQuantity != null.
     * @pre @forall menuItemName in orderItemNamesToQuantity.keySet(): exists menuItem with
     * menuItemName.
     * @pre @forall quantity in orderItemNamesToQuantity.values(): quantity is greater then 0.
     * @pre @forall menuItemName in orderItemNamesToQuantity.keySet(): the menuItem with name
     * menuItem is available for ordering.
     * @post number of orders = @pre number of orders + 1.
     * @post exists an order for the current user with orderItemNamesToQuantity.
     */
    public Order createOrder(User user, Map<String, Integer> orderItemNamesToQuantity, Map<String,
            MenuItem> existingItemNamesToItems) {
        assert orderItemNamesToQuantity != null;
        assert orderItemNamesToQuantity.keySet().stream().allMatch(existingItemNamesToItems::
                containsKey);
        assert orderItemNamesToQuantity.values().stream().allMatch(quantity -> quantity > 0);
        assert orderItemNamesToQuantity.keySet().stream().allMatch(name -> existingItemNamesToItems.get(name).isAvailable());
        Order order = new Order(generateOrderId(), user.getId(),
                LocalDateTime.now());
        Map<MenuItem, Integer> itemsToQuantity = new HashMap<>();
        Set<String> orderItemNames = orderItemNamesToQuantity.keySet();
        for (String itemName : orderItemNames) {
            itemsToQuantity.put(existingItemNamesToItems.get(itemName),
                    orderItemNamesToQuantity.get(itemName));
        }
        int prevNoOrders = ordersToItemQuantity.size();
        ordersToItemQuantity.put(order, itemsToQuantity);
        BillGenerator.createBill(user, order, itemsToQuantity);
        assert prevNoOrders + 1 == ordersToItemQuantity.size();
        return order;
    }

    Map<Order, Map<MenuItem, Integer>> getOrdersToItemQuantity() {
        return ordersToItemQuantity;
    }

    Map<MenuItem, Integer> getOrderedItemsToQuantity(Order order) {
        return ordersToItemQuantity.get(order);
    }

    void saveState() {
        Serializer.serializeOrders(ordersToItemQuantity);
    }

    boolean isWellFormed() {
        return noNullValues() &&
                allOrderedAmountsArePositive() &&
                orderIdsAreUnique();
    }

    private boolean noNullValues() {
        return ordersToItemQuantity != null &&
                !ordersToItemQuantity.containsKey(null) &&
                !ordersToItemQuantity.containsValue(null) &&
                ordersToItemQuantity.values().stream()
                        .flatMap(itemToQuantityMap -> itemToQuantityMap.entrySet().stream())
                        .noneMatch(entry -> (entry.getKey() == null || entry.getValue() == null));
    }

    private boolean allOrderedAmountsArePositive() {
        return ordersToItemQuantity.values().stream()
                .flatMap(itemToQuantityMap -> itemToQuantityMap.entrySet().stream())
                .allMatch(itemToQuantity -> itemToQuantity.getValue() > 0);
    }

    private boolean orderIdsAreUnique() {
        return ordersToItemQuantity.keySet().stream()
                .map(Order::getOrderId)
                .collect(Collectors.groupingBy(id -> id, Collectors.counting()))
                .entrySet()
                .stream().noneMatch(idToFrequency -> idToFrequency.getValue() > 1);
    }

    private int generateOrderId() {
        int trialId;
        do {
            trialId = (int) (Math.random() * Integer.MAX_VALUE);
        } while (existsOrderWithId(trialId));
        return trialId;
    }

    private boolean existsOrderWithId(int id) {
        return ordersToItemQuantity.keySet().stream().anyMatch(order -> order.getOrderId() == id);
    }
}
