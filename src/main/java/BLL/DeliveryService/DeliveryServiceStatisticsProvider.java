package BLL.DeliveryService;

import BLL.ViewModelFactory;
import Model.Orders.Order;
import Model.Products.MenuItem;
import Model.Users.User;
import Presenter.AdminView.ReportViewModels.ClientToNoExpensiveOrdersViewModel;
import Presenter.AdminView.ReportViewModels.MenuItemToNoOrdersViewModel;
import Presenter.AdminView.ReportViewModels.OrderViewModel;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class responsible for generating reports for a delivery service.
 */
public class DeliveryServiceStatisticsProvider {

    /**
     * Returns the list of orders, from the keys of ordersToItemQuantity, together with the name of
     * the client and the total price
     * (included in an OrderViewModel object), which were placed between startTime and endTime,
     * on any dat.
     *
     * @param startTime            is the start of the interval considered for orders.
     * @param endTime              is the end of the interval considered for orders.
     * @param ordersToItemQuantity is a map from the filtered orders to the ordered items and
     *                             the ordered quantities.
     * @param namesToUsers         is a map from the existing usernames to the users.
     * @return the orders placed in the interval (startTime, endTime).
     * @pre startTime != null.
     * @pre endTime != null.
     * @pre ordersToItemQuantity != null
     * @pre namesToUsers != null.
     * @post @nochange.
     */
    public List<OrderViewModel> getOrdersInTimeInterval(LocalTime startTime, LocalTime endTime,
                                                        Map<Order, Map<MenuItem, Integer>> ordersToItemQuantity,
                                                        Map<String, User> namesToUsers) {
        assert startTime != null;
        assert endTime != null;
        assert ordersToItemQuantity != null;
        assert namesToUsers != null;
        return ordersToItemQuantity.keySet().stream()
                .filter(order -> order.getDateTime().toLocalTime().isAfter(startTime) &&
                        order.getDateTime().toLocalTime().isBefore(endTime))
                .map(order -> ViewModelFactory.getOrderViewModel(order,
                        getUserWithId(order.getClientId(), namesToUsers).get(),
                        ordersToItemQuantity.get(order)))
                .collect(Collectors.toList());
    }

    /**
     * Returns the list of menu items, together with the number of portions ordered from them,
     * from which at least minTimes portions were ordered.
     *
     * @param minTimes             is the minimum number of portions ordered from a menuItem.
     * @param ordersToItemQuantity is a map from the filtered orders to the ordered items and
     *                             the ordered quantities.
     * @return the list of menu items, together with the number of portions ordered from them,
     * from which at least minTimes portions were ordered.
     * @pre ordersToItemQUantity != null.
     * @post @nochange.
     */
    public List<MenuItemToNoOrdersViewModel> getProductsOrderedManyTimes(int minTimes, Map<Order, Map<MenuItem, Integer>> ordersToItemQuantity) {
        assert ordersToItemQuantity != null;
        return ordersToItemQuantity.values().stream()
                .flatMap(itemToQuantityMap -> itemToQuantityMap.entrySet().stream())
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        Collectors.summingInt(Map.Entry::getValue)))
                .entrySet()
                .stream()
                .filter(itemToNoOrders -> itemToNoOrders.getValue() >= minTimes)
                .map(itemToNoOrders -> ViewModelFactory.getMenuItemToNoOrdersViewModel(itemToNoOrders.getKey(), itemToNoOrders.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * Returns the list of clients who placed at least minOrders orders of price is greater then
     * or equal to minPrice.
     *
     * @param minOrders            is the minimum number of considered orders placed by a client.
     * @param minPrice             is the minimum price of an order to be considered for a client.
     * @param ordersToItemQuantity is a map from the filtered orders to the ordered items and
     *                             the ordered quantities.
     * @param namesToUsers         is a map from the existing usernames to the users.
     * @return the list of clients who placed at least minOrders orders of price is greater then
     * or equal to minPrice.
     * @pre namesToUsers != null.
     * @pre ordersToItemQuantity != null.
     * @post @nochange.
     */
    public List<ClientToNoExpensiveOrdersViewModel> getClientsWithManyExpensiveOrders(int minOrders, int minPrice,
                                                                                      Map<Order, Map<MenuItem, Integer>> ordersToItemQuantity,
                                                                                      Map<String, User> namesToUsers) {
        return ordersToItemQuantity.keySet().stream()
                .filter(order -> Order.computePrice(ordersToItemQuantity.get(order)) >= minPrice)
                .collect(Collectors.groupingBy(Order::getClientId, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(clientIdToOrderNr -> clientIdToOrderNr.getValue() >= minOrders)
                .map(clientIdToOrderNr -> ViewModelFactory.getClientToNoExpensiveOrdersViewModel(
                        getUserWithId(clientIdToOrderNr.getKey(), namesToUsers).get(),
                        clientIdToOrderNr.getValue()))
                .collect(Collectors.toList());

    }

    /**
     * Returns the list of menu items which were ordered at least once on the specified day.
     *
     * @param day                  is the day on which the returned items were ordered.
     * @param ordersToItemQuantity is a map from the filtered orders to the ordered items and
     *                             the ordered quantities.
     * @return the list of menu items which were ordered at least once on the specified day
     * @pre day != null.
     * @post @nochange.
     */
    public List<MenuItemToNoOrdersViewModel> getMenuItemsOrderedOnSpecificDay(LocalDate day, Map<Order, Map<MenuItem, Integer>> ordersToItemQuantity) {
        assert day != null;
        assert ordersToItemQuantity != null;
        return ordersToItemQuantity.entrySet().stream()
                .filter(orderToItemQuantity -> orderToItemQuantity.getKey().getDateTime().toLocalDate().equals(day))
                .map(Map.Entry::getValue)
                .flatMap(itemToQuantityMap -> itemToQuantityMap.entrySet().stream())
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        Collectors.summingInt(Map.Entry::getValue)))
                .entrySet()
                .stream()
                .map(itemToNoOrders -> ViewModelFactory.getMenuItemToNoOrdersViewModel(itemToNoOrders.getKey(), itemToNoOrders.getValue()))
                .collect(Collectors.toList());

    }

    private Optional<User> getUserWithId(int userId, Map<String, User> namesToUsers) {
        List<User> matchingUsers =
                namesToUsers.values().stream().filter(user -> user.getId() == userId).collect(Collectors.toList());
        if (matchingUsers.size() > 0) {
            return Optional.of(matchingUsers.get(0));
        } else {
            return Optional.empty();
        }
    }
}
