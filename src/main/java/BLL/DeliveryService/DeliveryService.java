package BLL.DeliveryService;

import BLL.Filters.MenuItemFilter;
import BLL.Observable.PropertyChangeObservable;
import BLL.ViewModelFactory;
import Model.Orders.Order;
import Model.Users.User;
import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.AdminView.ReportViewModels.ClientToNoExpensiveOrdersViewModel;
import Presenter.AdminView.ReportViewModels.MenuItemToNoOrdersViewModel;
import Presenter.AdminView.ReportViewModels.OrderViewModel;
import Presenter.ClientView.CartItemClientViewModel;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Implementation for the overall business logic side of the application, defining all methods
 * through which the conrollers can interact with the delivery service for product management, user
 * management, order creation and report generation.
 */
public class DeliveryService implements IDeliveryService, PropertyChangeObservable {
    private final DeliveryServiceOrderManager ordersManager;
    private final DeliveryServiceUserManager userManager;
    private final DeliveryServiceProductManager productManager;
    private final DeliveryServiceStatisticsProvider statisticsProvider;

    private final PropertyChangeSupport support;

    /**
     * Event types fired by the DeliveryService for which listeners can register and listen to.
     */
    public enum EventType {
        NEW_ORDER,
        MENU_ITEM_DATA_CHANGE,
    }

    /**
     * Initialises the delivery service for the restaurant, and restores the data from the
     * serialized files.
     */
    public DeliveryService() {
        this.userManager = new DeliveryServiceUserManager();
        this.productManager = new DeliveryServiceProductManager();
        this.ordersManager = new DeliveryServiceOrderManager(productManager.getNamesToMenuItems());
        this.support = new PropertyChangeSupport(this);
        this.statisticsProvider = new DeliveryServiceStatisticsProvider();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadBaseProductsFromCSV() {
        assert isWellFormed();
        productManager.loadBaseProductsFromCSV();
        saveState();
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createBaseProduct(String title, int calories, int proteinsAmount, int fatAmount,
                                  int sodiumAmount, double price, boolean available) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.ADMIN;
        productManager.createBaseProduct(title, calories, proteinsAmount, fatAmount, sodiumAmount, price, available);
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsProductWithTitle(String title) {
        assert isWellFormed();
        return productManager.getNamesToMenuItems().containsKey(title);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<MenuItemAdminViewModel> getProductWithTitle(String title) {
        assert isWellFormed();
        return productManager.getProductWithTitle(title);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteProduct(String title) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.ADMIN;
        productManager.deleteProduct(title);
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void restoreProduct(String title) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.ADMIN;
        productManager.restoreProduct(title);
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyBaseProduct(String oldTitle, String newTitle, int newCalories,
                                  int newProteinsAmount, int newFatAmount, int newSodiumAmount,
                                  double newPrice, boolean newAvailability) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.ADMIN;
        productManager.modifyBaseProduct(oldTitle, newTitle, newCalories, newProteinsAmount,
                newFatAmount, newSodiumAmount, newPrice, newAvailability);
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createCompositeProduct(String title, List<String> componentNames,
                                       boolean available) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.ADMIN;
        productManager.createCompositeProduct(title, componentNames, available);
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modifyCompositeProduct(String oldTitle, String newTitle,
                                       List<String> newComponentNames, boolean newAVailability) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.ADMIN;
        productManager.modifyCompositeProduct(oldTitle, newTitle, newComponentNames, newAVailability);
        support.firePropertyChange(EventType.MENU_ITEM_DATA_CHANGE.name(), null, 1);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MenuItemAdminViewModel> getFilteredMenuItems(List<MenuItemFilter> filters) {
        assert isWellFormed();
        return productManager.getFilteredMenuItems(filters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CartItemClientViewModel> getAvailableFilteredMenuItems(List<MenuItemFilter> filters) {
        assert isWellFormed();
        return productManager.getAvailableFilteredMenuItems(filters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean logIn(String userName, String password) {
        assert isWellFormed();
        boolean loginStatus = userManager.logIn(userName, password);
        assert isWellFormed();
        return loginStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User.UserType> getLoggedInUserType() {
        assert isWellFormed();
        return userManager.getLoggedInUserType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsLoggedInUser() {
        return userManager.getLoggedInUser() != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logOut() {
        assert isWellFormed();
        userManager.logOut();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerClient(String userName, String password) {
        assert isWellFormed();
        userManager.registerClient(userName, password);
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean existsUserWithUsername(String userName) {
        assert isWellFormed();
        return userManager.existsUserWithUsername(userName);
    }

    @Override
    public void createOrder(Map<String, Integer> orderItemNamesToQuantity) {
        assert isWellFormed();
        assert existsLoggedInUser();
        assert userManager.getLoggedInUserType().get() == User.UserType.CLIENT;
        Order order = ordersManager.createOrder(userManager.getLoggedInUser(),
                orderItemNamesToQuantity,
                productManager.getNamesToMenuItems());
        support.firePropertyChange(EventType.NEW_ORDER.name(), null,
                ViewModelFactory.getOrderEmployeeViewModel(order, userManager.getLoggedInUser(),
                        ordersManager.getOrderedItemsToQuantity(order)));
        saveState();
        assert isWellFormed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<OrderViewModel> getOrdersInTimeInterval(LocalTime startTime, LocalTime endTime) {
        assert isWellFormed();
        return statisticsProvider.getOrdersInTimeInterval(startTime, endTime,
                ordersManager.getOrdersToItemQuantity(), userManager.getNamesToUsers());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MenuItemToNoOrdersViewModel> getProductsOrderedManyTimes(int minTimes) {
        assert isWellFormed();
        return statisticsProvider.getProductsOrderedManyTimes(minTimes,
                ordersManager.getOrdersToItemQuantity());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ClientToNoExpensiveOrdersViewModel> getClientsWithManyExpensiveOrders(int minOrders, int minPrice) {
        assert isWellFormed();
        return statisticsProvider.getClientsWithManyExpensiveOrders(minOrders, minPrice,
                ordersManager.getOrdersToItemQuantity(), userManager.getNamesToUsers());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MenuItemToNoOrdersViewModel> getMenuItemsOrderedOnSpecificDay(LocalDate day) {
        assert isWellFormed();
        return statisticsProvider.getMenuItemsOrderedOnSpecificDay(day,
                ordersManager.getOrdersToItemQuantity());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        support.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        support.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isWellFormed() {
        return userManager.isWellFormed() &&
                productManager.isWellFormed() &&
                ordersManager.isWellFormed() &&
                allOrderSendersExist() &&
                allOrderSendersAreClients() &&
                allOrderedItemsExist();
    }

    private boolean allOrderSendersExist() {
        return ordersManager.getOrdersToItemQuantity().keySet().stream()
                .allMatch(order -> userManager.getUserWithId(order.getClientId()).isPresent());
    }

    private boolean allOrderSendersAreClients() {
        return ordersManager.getOrdersToItemQuantity().keySet().stream()
                .allMatch(order -> userManager.getUserWithId(order.getClientId()).get().getType().equals(User.UserType.CLIENT));
    }

    private boolean allOrderedItemsExist() {
        return ordersManager.getOrdersToItemQuantity().values().stream()
                .flatMap(itemToQuantityMap -> itemToQuantityMap.entrySet().stream())
                .map(Map.Entry::getKey)
                .distinct()
                .allMatch(productManager.getNamesToMenuItems()::containsValue);
    }

    private void saveState() {
        userManager.saveState();
        productManager.saveState();
        ordersManager.saveState();
    }
}