package BLL.DeliveryService;

import BLL.Filters.MenuItemFilter;
import Model.Users.User;
import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.AdminView.ReportViewModels.ClientToNoExpensiveOrdersViewModel;
import Presenter.AdminView.ReportViewModels.MenuItemToNoOrdersViewModel;
import Presenter.AdminView.ReportViewModels.OrderViewModel;
import Presenter.ClientView.CartItemClientViewModel;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * General interface for the business logic side of the application, defining all methods through
 * which the conrollers can interact with the delivery service for product management, user
 * management, order creation and report generation.
 */
public interface IDeliveryService {

    /**
     * Adds a new base product to the restaurant menu with the specified parameters, but only if
     * the currently logged in user is an administrator, as other users don't have the right to
     * create products.
     *
     * @param title          is the name of the new base product.
     * @param calories       specifies the number of calories in one portion of the new product.
     * @param proteinsAmount specifies the amount of proteins in one portion of the new product.
     * @param fatAmount      specifies the amount of fat in one portion of the new product.
     * @param sodiumAmount   specifies the amount of sodium in one portion of the new product.
     * @param price          specifies cost of buying one portion of the new product.
     * @param available      specified whether the new product is available to customers.
     * @pre new BaseProductValidator.validate(title, calories, proteinsAmount, fatAmount,
     * sodiumAmount, price, available).isValid().
     * @pre !existsProductWithTitle(title).
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == ADMIN.
     * @post exists product with the specified data.
     * @post number of products == @pre number of products + 1.
     * @invariant isWellFormed()
     */
    void createBaseProduct(String title, int calories, int proteinsAmount, int fatAmount,
                           int sodiumAmount, double price, boolean available);

    /**
     * Specifies whether a product with title exists.
     *
     * @param title is the searched title.
     * @return true if a product with title exists, otherwise false.
     * @pre true.
     * @post @nochange.
     * @invariant isWellFormed()
     */
    boolean existsProductWithTitle(String title);

    /**
     * Returns the product with the specified title, if it exists.
     *
     * @param title is the searched title.
     * @return true if the unique product with title title, if it exists, or an empty optional
     * otherwise.
     * @pre true.
     * @post @nochange.
     * @invariant isWellFormed()
     */
    Optional<MenuItemAdminViewModel> getProductWithTitle(String title);

    /**
     * Sets the product with title title to unavailable, meaning that it is invisible to clients
     * and cannot be ordered, but only if the currently logged in user is an administrator, as
     * other users don't have the right to delete products.
     *
     * @param title is the title of the product which is set to unavailable.
     * @pre existsProductWithTitle(title).
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == ADMIN.
     * @post the product with title title is unavailable to clients.
     * @invariant isWellFormed()
     */
    void deleteProduct(String title);

    /**
     * Sets the product with title title to available, meaning that it is visible to clients
     * and can be ordered, but only if the currently logged in user is an administrator,  as
     * other users don't have the right to restore products.
     *
     * @param title is the title of the product which is set to available.
     * @pre existsProductWithTitle(title).
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == ADMIN.
     * @post the product with title title is available to clients.
     * @invariant isWellFormed()
     */
    void restoreProduct(String title);

    /**
     * Modifies the base product with title oldTitle to have the newly specified attributes, but only if
     * the currently logged in user is an administrator, as other users don't have the right to
     * mnodify products.
     *
     * @param oldTitle          is the current title of the product.
     * @param newTitle          is the new title of the product.
     * @param newCalories       is the newly set amount of calories of the product.
     * @param newProteinsAmount is the newly set amount of proteins of the product.
     * @param newFatAmount      is the newly set amount of fat of the product.
     * @param newSodiumAmount   is the newly set amount of sodium of the product.
     * @param newPrice          is the new price of the product.
     * @param available         shows whether the product is available from now on.
     * @pre existsProductWithTitle(oldTitle)
     * @pre oldTitle.equals(newTitle) || !existsProductWithTitle(newTitle)
     * @pre the product with title oldTitle is a BaseProduct.
     * @pre new BaseProductValidator.validate(newTitle, newCalories, newProteinsAmount,
     * newFatAmount, newSodiumAmount, newPrice, available).isValid().
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == ADMIN.
     * @post number of products == @pre number of products.
     * @post existsProductWithTitle(newTitle).
     * @post the product with title newTitle has the specified data.
     * @invariant isWellFormed()
     */
    void modifyBaseProduct(String oldTitle, String newTitle, int newCalories,
                           int newProteinsAmount, int newFatAmount, int newSodiumAmount,
                           double newPrice, boolean available);

    /**
     * Creates a new product composed of the products with names in componentNames, and sets its
     * availability to available, but only if the currently logged in user is an administrator,
     * as other users don't have the right to create products.
     *
     * @param title          is the title of the new product.
     * @param componentNames contains the names of the products from which the new product is
     *                       composed.
     * @param available      shows the availability of the new product.
     * @pre !existsProductWithTitle(title)
     * @pre {@forall} componentName in componentNames: existsProductWithTitle(componentName)
     * @pre new CompositeProductValidator.validate(title, available).isValid()
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == ADMIN.
     * @pre componentNames.size() is greater than 0.
     * @post number of products == @pre number of products + 1.
     * @post exists product with title title and components with names componentNames.
     * @invariant isWellFormed()
     */
    void createCompositeProduct(String title, List<String> componentNames, boolean available);

    /**
     * Modifies the composite product with title oldTitle to the newly specified attributes, but only if
     * the currently logged in user is an administrator, as other users don't have the right to
     * modify products.
     *
     * @param oldTitle          is the current title of the product to modify.
     * @param newTitle          is the new title of the modified product.
     * @param newComponentNames are the names of the new components of the modified prouduct.
     * @param available         shows the new availability of the product.
     * @pre existsProductWithTitle(oldTitle)
     * @pre oldTitle.equals(newTitle) || !existsProductWithTitle(newTitle)
     * @pre the product with title oldTitle is a CompositeProduct.
     * @post @forall componentName in componentNames: existsProductWithTitle(componentName)
     * @pre new CompositeProductValidator.validate(newTitle, available).isValid()
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == ADMIN.
     * @pre @forall componentName in componentNames: the component with componentName doesn't
     * contain the product with title oldTitle.
     * @pre newComponentNames.size() is greater than 0.
     * @post number of products == @pre number of products.
     * @post exists product with title newTitle and components with names componentNames.
     * @invariant isWellFormed().
     */
    void modifyCompositeProduct(String oldTitle, String newTitle, List<String> newComponentNames,
                                boolean available);

    /**
     * Returns a list of menu items which fulfill all criteria specified by filters and which are
     * available to clients for ordering.
     *
     * @param filters are the criteria which need to be satisfied by the menuitems.
     * @return the list of menu items which fulfill all criteria specified by filters and which are
     * available to clients for ordering.
     * @pre filters != null.
     * @post @nochange.
     * @invariant isWellFormed().
     */
    List<CartItemClientViewModel> getAvailableFilteredMenuItems(List<MenuItemFilter> filters);

    /**
     * Returns a list of menu items which fulfill all criteria specified by filters.
     *
     * @param filters are the criteria which need to be satisfied by the menuitems.
     * @return the list of menu items which fulfill all criteria specified by filters.
     * @pre filters != null.
     * @post @nochange.
     * @invariant isWellFormed().
     */
    List<MenuItemAdminViewModel> getFilteredMenuItems(List<MenuItemFilter> filters);

    /**
     * If a user with the provided userName exists and they have the provided password, then they
     * log in, and the method returns true. Otherwise, the methor returns false.
     *
     * @param userName is the userName of the user to log in.
     * @param password is the assumed password provided by the user.
     * @return true is the login was successful, false otherwise.
     * @pre !existsLoggedInUser().
     * @post if !exists user with (userName, password), then !existsLoggedInUser(),
     * else loggedInUser is the one with userName and password.
     * @invariant isWellFormed().
     */
    boolean logIn(String userName, String password);

    /**
     * Specifies the type of the logged in user, if any.
     *
     * @return the type of the logged in user, if existsLoggedInUser(), else returnss an empty
     * optional.
     * @pre true.
     * @post @nochange.
     * @invariant isWellFormed()
     */
    Optional<User.UserType> getLoggedInUserType();

    /**
     * Specifies whether any user is logged in.
     *
     * @return true if any user is logged in, otherwise false.
     * @pre true.
     * @post @noChange.
     * @invariant isWellFormed().
     */
    boolean existsLoggedInUser();

    /**
     * Logs out the currently logged in user.
     *
     * @pre existsLoggedInUser().
     * @post !existsLoggedInUser().
     * @invariant isWellFormed()
     */
    void logOut();

    /**
     * Registers a new client with the specified data.
     *
     * @param userName is the username of the new client.
     * @param password is the password of the new client.
     * @pre new UserValidator.validate(userName, password).isValid().
     * @pre !existsUserWithUsername(userName).
     * @post exists a user with userName, password.
     * @post number of users = @pre number of users + 1.
     * @invariant isWellFormed()
     */
    void registerClient(String userName, String password);

    /**
     * Returns true if a user with the specified userName exists, otherwise false.
     *
     * @param userName is the searched userName.
     * @return true if a user with the specified userName exists, otherwise false.
     * @pre true.
     * @post @nochange.
     * @invariant isWellFormed()
     */
    boolean existsUserWithUsername(String userName);

    /**
     * Creates a new order with the specified items and ordered quantities, in the name of the
     * currently logged in user, who must be a client.
     *
     * @param orderItemNamesToQuantity specifies the ordered items and the ordered quantities.
     * @pre existsLoggedInUser().
     * @pre getLoggedInUserType() == CLIENT.
     * @pre orderItemNamesToQuantity != null.
     * @pre @forall menuItemName in orderItemNamesToQuantity.keySet(): exists menuItem with
     * menuItemName.
     * @pre @forall quantity in orderItemNamesToQuantity.values(): quantity is greater than 0.
     * @pre @forall menuItemName in orderItemNamesToQuantity.keySet(): the menuItem with name
     * menuItem is available for ordering.
     * @post number of orders = @pre number of orders + 1.
     * @post exists an order for the current user with orderItemNamesToQuantity.
     * @invariant isWellFormed().
     */
    void createOrder(Map<String, Integer> orderItemNamesToQuantity);

    /**
     * Returns the list of orders, together with the name of the client and the total price
     * (included in an OrderViewModel object), which were placed between startTime and endTime,
     * on any dat.
     *
     * @param startTime is the start of the interval considered for orders.
     * @param endTime   is the end of the interval considered for orders.
     * @return the orders placed in the interval (startTime, endTime).
     * @pre startTime != null.
     * @pre endTime != null.
     * @post @nochange.
     * @invariant isWellFormed().
     */
    List<OrderViewModel> getOrdersInTimeInterval(LocalTime startTime, LocalTime endTime);

    /**
     * Returns the list of menu items, together with the number of portions ordered from them,
     * from which at least minTimes portions were ordered.
     *
     * @param minTimes is the minimum number of portions ordered from a menuItem.
     * @return the list of menu items, together with the number of portions ordered from them,
     * from which at least minTimes portions were ordered.
     * @pre true.
     * @post @nochange.
     * @invariant isWellFormed().
     */
    List<MenuItemToNoOrdersViewModel> getProductsOrderedManyTimes(int minTimes);

    /**
     * Returns the list of clients who placed at least minOrders orders of price is greater than
     * or equal to minPrice.
     *
     * @param minOrders is the minimum number of considered orders placed by a client.
     * @param minPrice  is the minimum price of an order to be considered for a client.
     * @return the list of clients who placed at least minOrders orders of price is greater than
     * or equal to minPrice.
     * @pre true.
     * @post @nochange.
     * @invariant isWellFormed().
     */
    List<ClientToNoExpensiveOrdersViewModel> getClientsWithManyExpensiveOrders(int minOrders,
                                                                               int minPrice);

    /**
     * Returns the list of menu items which were ordered at least once on the specified day.
     *
     * @param day is the day on which the returned items were ordered.
     * @return the list of menu items which were ordered at least once on the specified day.
     * @pre day != null.
     * @post @nochange.
     * @invariant isWellFormed().
     */
    List<MenuItemToNoOrdersViewModel> getMenuItemsOrderedOnSpecificDay(LocalDate day);

    /**
     * Shows whether the object is in a valid state.
     *
     * @return true if the object is in a valid state, false otherwise.
     */
    boolean isWellFormed();

    /**
     * Adds all the base products from the default CSV file to the list of products, possibly
     * overwriting existing products with the same names.
     *
     * @pre true.
     * @invariant isWellFormed().
     */
    void loadBaseProductsFromCSV();
}
