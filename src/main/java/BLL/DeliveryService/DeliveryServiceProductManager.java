package BLL.DeliveryService;

import BLL.Filters.AvailabilityFilter;
import BLL.Filters.MenuItemFilter;
import BLL.Validators.BaseProductValidation.BaseProductValidator;
import BLL.Validators.CompositeProductValidator.CompositeProductValidator;
import BLL.ViewModelFactory;
import DAL.CSVReader.CSVBaseItemsReader;
import DAL.Serialization.Serializer;
import Model.Products.BaseProduct;
import Model.Products.CompositeProduct;
import Model.Products.MenuItem;
import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.ClientView.CartItemClientViewModel;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class responsible for managing product-related data for a general delivery service.
 */
public class DeliveryServiceProductManager {
    private final Map<String, MenuItem> namesToMenuItems;
    private final BaseProductValidator baseProductValidator;
    private final CompositeProductValidator compositeProductValidator;

    private static final String BASE_PRODUCTS_DATA_FILE_NAME = "products.csv";

    /**
     * Maximum value for the rating of a menu item. Remark that at this moment ratings are
     * randomly generated.
     */
    public static final double MAX_RATING = 5.0;

    /**
     * Initializes a DeliveryServiceProductManager by deserializing product-related data.
     */
    public DeliveryServiceProductManager() {
        this.namesToMenuItems = Serializer.deserializeMenuItems();
        this.baseProductValidator = new BaseProductValidator();
        this.compositeProductValidator = new CompositeProductValidator();
    }

    /**
     * Adds all the base products from the default CSV file to the list of products, possibly
     * overwriting existing products with the same names.
     *
     * @pre true.
     */
    public void loadBaseProductsFromCSV() {
        List<BaseProduct> menuItems =
                CSVBaseItemsReader.loadBaseProducts(BASE_PRODUCTS_DATA_FILE_NAME, this);
        for (BaseProduct product : menuItems) {
            if (namesToMenuItems.containsKey(product.getTitle()) && namesToMenuItems.get(product.getTitle()) instanceof BaseProduct) {
                modifyBaseProduct(product.getTitle(), product.getTitle(), product.getCalories(),
                        product.getProteinsAmount(), product.getFatsAmount(),
                        product.getSodiumAmount(), product.getPrice(), product.isAvailable());
            } else if (!namesToMenuItems.containsKey(product.getTitle())) {
                addMenuItem(product);
            }
        }
    }

    /**
     * Adds a new base product to the restaurant menu with the specified parameters.
     *
     * @param title          is the name of the new base product.
     * @param calories       specifies the number of calories in one portion of the new product.
     * @param proteinsAmount specifies the amount of proteins in one portion of the new product.
     * @param fatAmount      specifies the amount of fat in one portion of the new product.
     * @param sodiumAmount   specifies the amount of sodium in one portion of the new product.
     * @param price          specifies cost of buying one portion of the new product.
     * @param available      specified whether the new product is available to customers.
     * @pre new BaseProductValidator.validate(title, null, calories, proteinsAmount, fatAmount,
     * sodiumAmount, price, available).isValid().
     * @pre !exists product with title title.
     * @post exists product with the specified data.
     * @post number of products == @pre number of products + 1.
     */
    public void createBaseProduct(String title, int calories, int proteinsAmount, int fatAmount,
                                  int sodiumAmount, double price, boolean available) {
        assert baseProductValidator.validate(title, calories, proteinsAmount, fatAmount,
                sodiumAmount, price, available).isValid();
        assert !namesToMenuItems.containsKey(title);
        int prevNoProducts = namesToMenuItems.size();
        double rating = Math.random() * MAX_RATING; // generate random rating
        MenuItem item = new BaseProduct(generateMenuItemId(), title, rating, calories,
                proteinsAmount, fatAmount,
                sodiumAmount, price, available);
        addMenuItem(item);
        assert prevNoProducts + 1 == namesToMenuItems.size();
        assert namesToMenuItems.containsKey(title);
        assert namesToMenuItems.get(title).computePrice() == price;
        assert namesToMenuItems.get(title).getCalories() == calories;
        assert namesToMenuItems.get(title).getProteinsAmount() == proteinsAmount;
        assert namesToMenuItems.get(title).getFatsAmount() == fatAmount;
        assert namesToMenuItems.get(title).getSodiumAmount() == sodiumAmount;
        assert namesToMenuItems.get(title).isAvailable() == available;
    }

    /**
     * Returns the product with the specified title, if it exists.
     *
     * @param title is the searched title.
     * @return true ithe unique product with title title, if it exists, or an empty optional
     * otherwise.
     * @pre true.
     * @post @nochange.
     */
    Optional<MenuItemAdminViewModel> getProductWithTitle(String title) {
        if (namesToMenuItems.containsKey(title)) {
            return Optional.of(ViewModelFactory.getMenuItemAdminViewModel(namesToMenuItems.get(title)));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Sets the product with title title to unavailable, meaning that it is invisible to clients
     * and cannot be ordered.
     *
     * @param title is the title of the product which is set to unavailable.
     * @pre existsProductWithTitle(title).
     * @post the product with title title is unavailable to clients.
     */
    public void deleteProduct(String title) {
        assert namesToMenuItems.containsKey(title);
        namesToMenuItems.get(title).setAvailable(false);
        assert !namesToMenuItems.get(title).isAvailable();
    }

    /**
     * Sets the product with title title to available, meaning that it is visible to clients
     * and can be ordered.
     *
     * @param title is the title of the product which is set to available.
     * @pre existsProductWithTitle(title).
     * @post the product with title title is available to clients.
     */
    public void restoreProduct(String title) {
        assert namesToMenuItems.containsKey(title);
        namesToMenuItems.get(title).setAvailable(true);
        assert namesToMenuItems.get(title).isAvailable();
    }

    /**
     * Modifies the base product with title oldTitle to have the newly specified attributes.
     *
     * @param oldTitle          is the current title of the product.
     * @param newTitle          is the new title of the product.
     * @param newCalories       is the newly set amount of calories of the product.
     * @param newProteinsAmount is the newly set amount of proteins of the product.
     * @param newFatAmount      is the newly set amount of fat of the product.
     * @param newSodiumAmount   is the newly set amount of sodium of the product.
     * @param newPrice          is the new price of the product.
     * @param newAvailability   shows whether the product is available from now on.
     * @pre existsProductWithTitle(oldTitle)
     * @pre oldTitle.equals(newTitle) || !existsProductWithTitle(newTitle)
     * @pre new BaseProductValidator.validate(newTitle, newCalories, newProteinsAmount,
     * newFatAmount, newSodiumAmount, newPrice, available).isValid().
     * @post number of products == @pre number of products.
     * @post existsProductWithTitle(newTitle).
     * @post the product with title newTitle has the specified data.
     */
    public void modifyBaseProduct(String oldTitle, String newTitle, int newCalories,
                                  int newProteinsAmount, int newFatAmount, int newSodiumAmount,
                                  double newPrice, boolean newAvailability) {
        assert namesToMenuItems.containsKey(oldTitle);
        assert oldTitle.equals(newTitle) || !namesToMenuItems.containsKey(newTitle);
//        assert baseProductValidator.validate(newTitle, newCalories, newProteinsAmount,
//                newFatAmount, newSodiumAmount, newPrice, newAvailability).isValid();
        assert namesToMenuItems.get(oldTitle) instanceof BaseProduct;
        BaseProduct bp = (BaseProduct) namesToMenuItems.get(oldTitle);
        bp.setTitle(newTitle);
        bp.setCalories(newCalories);
        bp.setFatsAmount(newFatAmount);
        bp.setProteinsAmount(newProteinsAmount);
        bp.setSodiumAmount(newSodiumAmount);
        bp.setPrice(newPrice);
        bp.setAvailable(newAvailability);
        int prevNoProducts = namesToMenuItems.size();
        if (!oldTitle.equals(newTitle)) {
            this.namesToMenuItems.remove(oldTitle);
            addMenuItem(bp);
        }
        assert prevNoProducts == namesToMenuItems.size();
        assert namesToMenuItems.containsKey(newTitle);
        assert namesToMenuItems.get(newTitle).computePrice() == newPrice;
        assert namesToMenuItems.get(newTitle).getCalories() == newCalories;
        assert namesToMenuItems.get(newTitle).getProteinsAmount() == newProteinsAmount;
        assert namesToMenuItems.get(newTitle).getFatsAmount() == newFatAmount;
        assert namesToMenuItems.get(newTitle).getSodiumAmount() == newSodiumAmount;
        assert namesToMenuItems.get(newTitle).isAvailable() == newAvailability;
    }

    /**
     * Creates a new product composed of the products with names in componentNames, and sets its
     * availability to available.
     *
     * @param title          is the title of the new product.
     * @param componentNames contains the names of the products from which the new product is
     *                       composed.
     * @param available      shows the availability of the new product.
     * @pre !existsProductWithTitle(title)
     * @post @forall componentName in componentNames: existsProductWithTitle(componentName).
     * @pre new CompositeProductValidator.validate(title, available).isValid().
     * @pre componentNames.size() is greater then 0.
     * @post number of products == @pre number of products + 1.
     * @post exists product with title title and components with names componentNames.
     */
    public void createCompositeProduct(String title, List<String> componentNames,
                                       boolean available) {
        assert !namesToMenuItems.containsKey(title);
        assert componentNames.size() > 0;
        assert componentNames.stream().allMatch(namesToMenuItems::containsKey);
        assert compositeProductValidator.validate(title, available).isValid();
        int prevNoProducts = namesToMenuItems.size();
        double rating = Math.random() * MAX_RATING; //todo
        addMenuItem(new CompositeProduct(generateMenuItemId(), title, rating, available,
                getMenuItems(componentNames)));
        assert prevNoProducts + 1 == namesToMenuItems.size();
        assert namesToMenuItems.containsKey(title);
        assert componentNames.stream().allMatch(name ->
                ((CompositeProduct) namesToMenuItems.get(title))
                        .getComponents().stream()
                        .map(MenuItem::getTitle)
                        .anyMatch(componentName -> componentName.equals(name)));
        assert namesToMenuItems.get(title).isAvailable() == available;
    }

    /**
     * Modifies the composite product with title oldTitle to the newly specified attributes, but only if
     * the currently logged in user is an administrator, as other users don't have the right to
     * modify products.
     *
     * @param oldTitle          is the current title of the product to modify.
     * @param newTitle          is the new title of the modified product.
     * @param newComponentNames are the names of the new components of the modified prouduct.
     * @param newAVailability   shows the new availability of the product.
     * @pre existsProductWithTitle(oldTitle)
     * @pre oldTitle.equals(newTitle) || !existsProductWithTitle(newTitle)
     * @pre the product with title oldTitle is a CompositeProduct.
     * @pre @forall componentName in componentNames: the component with componentName doesn't
     * contain the product with title oldTitle.
     * @post @forall componentName in componentNames: existsProductWithTitle(componentName)
     * @pre new CompositeProductValidator.validate(newTitle, available).isValid().
     * @pre newComponentNames.size() is greater then 0.
     * @post number of products == @pre number of products.
     * @post exists product with title newTitle and components with names componentNames.
     */
    public void modifyCompositeProduct(String oldTitle, String newTitle,
                                       List<String> newComponentNames, boolean newAVailability) {
        assert namesToMenuItems.containsKey(oldTitle);
        assert newComponentNames.size() > 0;
        assert oldTitle.equals(newTitle) || !namesToMenuItems.containsKey(newTitle);
        assert newComponentNames.stream().allMatch(namesToMenuItems::containsKey);
        assert compositeProductValidator.validate(newTitle, newAVailability).isValid();
        assert namesToMenuItems.get(oldTitle) instanceof CompositeProduct;
        assert newComponentNames.stream().map(namesToMenuItems::get).noneMatch(component -> component.contains(namesToMenuItems.get(oldTitle)));
        int prevNoProducts = namesToMenuItems.size();
        CompositeProduct cp = (CompositeProduct) namesToMenuItems.get(oldTitle);
        cp.setTitle(newTitle);
        cp.updateComponents(getMenuItems(newComponentNames));
        cp.setAvailable(newAVailability);
        if (!oldTitle.equals(newTitle)) {
            namesToMenuItems.remove(oldTitle);
            addMenuItem(cp);
        }
        assert prevNoProducts == namesToMenuItems.size();
        assert namesToMenuItems.containsKey(newTitle);
        assert newComponentNames.stream().allMatch(name ->
                ((CompositeProduct) namesToMenuItems.get(newTitle))
                        .getComponents().stream()
                        .map(MenuItem::getTitle)
                        .anyMatch(componentName -> componentName.equals(name)));
        assert namesToMenuItems.get(newTitle).isAvailable() == newAVailability;
    }

    /**
     * Returns a list of menu items which fulfill all criteria specified by filters.
     *
     * @param filters are the criteria which need to be satisfied by the menuitems.
     * @return the list of menu items which fulfill all criteria specified by filters.
     * @pre filters != null.
     * @post @nochange.
     */
    public List<MenuItemAdminViewModel> getFilteredMenuItems(List<MenuItemFilter> filters) {
        Stream<MenuItem> itemStream =
                namesToMenuItems.values().stream();
        for (MenuItemFilter filter : filters) {
            itemStream = itemStream.filter(filter::apply);
        }
        return itemStream.map(ViewModelFactory::getMenuItemAdminViewModel).collect(Collectors.toList());
    }

    /**
     * Returns a list of menu items which fulfill all criteria specified by filters and which are
     * available to clients for ordering.
     *
     * @param filters are the criteria which need to be satisfied by the menuitems.
     * @return the list of menu items which fulfill all criteria specified by filters and which are
     * available to clients for ordering.
     * @pre filters != null.
     * @post @nochange.
     */
    public List<CartItemClientViewModel> getAvailableFilteredMenuItems(List<MenuItemFilter> filters) {
        assert filters != null;
        Stream<MenuItem> itemStream =
                namesToMenuItems.values().stream();
        for (MenuItemFilter filter : filters) {
            itemStream = itemStream.filter(filter::apply);
        }
        AvailabilityFilter availabilityFilter = new AvailabilityFilter(true);
        itemStream = itemStream.filter(availabilityFilter::apply);
        return itemStream.map(ViewModelFactory::getMenuItemClientViewModel).collect(Collectors.toList());
    }

    /**
     * Generates a unique id for a new menu item.
     * @return the unique id which is not taken by any existing item.
     */
    public int generateMenuItemId() {
        int trialId;
        do {
            trialId = (int) (Math.random() * Integer.MAX_VALUE);
        } while (existsMenuItemWithId(trialId));
        return trialId;
    }

    public Optional<Integer> getMenuItemsId(String title) {
        if (namesToMenuItems.containsKey(title)) {
            return Optional.of(namesToMenuItems.get(title).getId());
        } else {
            return Optional.empty();
        }
    }

    Map<String, MenuItem> getNamesToMenuItems() {
        return new HashMap<>(namesToMenuItems);
    }

    private void addMenuItem(MenuItem item) {
        namesToMenuItems.put(item.getTitle(), item);
    }

    private List<MenuItem> getMenuItems(Collection<String> names) {
        List<MenuItem> items = new ArrayList<>();
        for (String itemName : names) {
            items.add(namesToMenuItems.get(itemName));
        }
        return items;
    }

    boolean isWellFormed() {
        return noNullValues() &&
                correctMenuItemNamesMapping() &&
                !existSelfContainedMenuItems() &&
                allMenuItemPricesArePositive();
    }

    private boolean noNullValues() {
        return namesToMenuItems != null && !namesToMenuItems.containsValue(null) && !namesToMenuItems.containsKey(null);
    }

    private boolean correctMenuItemNamesMapping() {
        return namesToMenuItems.entrySet().stream()
                .allMatch(entry -> entry.getKey().equals(entry.getValue().getTitle()));
    }

    private boolean existSelfContainedMenuItems() {
        return namesToMenuItems.values().stream()
                .filter(menuItem -> menuItem instanceof CompositeProduct)
                .anyMatch(menuItem -> ((CompositeProduct) menuItem).containsAsComponent(menuItem));
    }

    private boolean allMenuItemPricesArePositive() {
        return namesToMenuItems.values().stream()
                .allMatch(menuItem -> menuItem.computePrice() > 0);
    }

    void saveState() {
        Serializer.serializeMenuItems(new ArrayList<>(namesToMenuItems.values()));
    }

    private boolean existsMenuItemWithId(int id) {
        return namesToMenuItems.values().stream().anyMatch(menuItem -> menuItem.getId() == id);
    }
}
