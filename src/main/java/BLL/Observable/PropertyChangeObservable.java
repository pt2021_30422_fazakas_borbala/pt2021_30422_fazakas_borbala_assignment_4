package BLL.Observable;

import java.beans.PropertyChangeListener;

/**
 * Interface for observables to which listeners can register and listen to.
 */
public interface PropertyChangeObservable {

    /**
     * Registers a new listener for all properties.
     *
     * @param listener is the newly registered listener.
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Removes a new listener for all properties.
     *
     * @param listener is the removed listener.
     */
    void removePropertyChangeListener(PropertyChangeListener listener);

    /**
     * Registers a new listener for propertyName property only.
     *
     * @param listener     is the removed listener.
     * @param propertyName is the name of the property for which it is registered.
     */
    void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);

    /**
     * Removes a new listener for propertyName property only.
     *
     * @param listener     is the removed listener.
     * @param propertyName is the name of the property for which it is removed.
     */
    void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);
}
