package Presenter.ClientView;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.text.DecimalFormat;

/**
 * Controls the pane representing a cart item.
 */
public class CartItemController {

    @FXML
    private Label titleLabel;

    @FXML
    private Label priceLabel;

    @FXML
    private Label proteinsAmountLabel;

    @FXML
    private Label fatsAmountLabel;

    @FXML
    private Label sodiumAmountLabel;

    @FXML
    private Label caloriesAmountLabel;

    @FXML
    private Label orderedAmountLabel;

    @FXML
    private Label ratingLabel;

    @FXML
    private Button decrementAmountButton;

    @FXML
    private ListView<String> componentsListView;

    @FXML
    private Label componentsLabel;

    private CartItemClientViewModel menuItem;

    private static final DecimalFormat df = new DecimalFormat("###.###");

    private final ChangeListener<Number> amountChangeListener =
            (observableValue, number, t1) -> onOrderedAmountChanged();

    /**
     * Initialises the pane with the data of a cart item.
     * @param menuItem is the item in the cart.
     */
    public void initMenuItem(CartItemClientViewModel menuItem) {
        if (this.menuItem != null) {
            this.menuItem.getObservableOrderedAmount().removeListener(amountChangeListener);
        }
        this.menuItem = menuItem;
        fillLabels();
        onOrderedAmountChanged();
        showComponentsList();
        this.menuItem.getObservableOrderedAmount().addListener(amountChangeListener);
    }

    /**
     * Shows whether the pane has the item initialised.
     * @return true if the item was initialized, otherwise false.
     */
    public boolean isInitialized() {
        return menuItem != null;
    }

    /** When the increment button is clicked, the ordered amount from the item is increased by 1. */
    public void onIncrementAmountButtonClicked() {
        menuItem.setOrderedAmount(menuItem.getOrderedAmount() + 1);
    }

    /** When the decrement button is clicked, the ordered amount from the item is decreased by 1. */
    public void onDecrementAmountButtonClicked() {
        menuItem.setOrderedAmount(menuItem.getOrderedAmount() - 1);
    }

    private void fillLabels() {
        titleLabel.setText(menuItem.getTitle());
        priceLabel.setText(df.format(menuItem.getPrice()));
        proteinsAmountLabel.setText(String.valueOf(menuItem.getProteinsAmount()));
        fatsAmountLabel.setText(String.valueOf(menuItem.getFatsAmount()));
        sodiumAmountLabel.setText(String.valueOf(menuItem.getSodiumAmount()));
        caloriesAmountLabel.setText(String.valueOf(menuItem.getCaloriesAmount()));
        ratingLabel.setText(df.format(menuItem.getRating()));
        orderedAmountLabel.textProperty().bind(menuItem.getObservableOrderedAmount().asString());
    }

    private void onOrderedAmountChanged() {
        if (menuItem.getOrderedAmount() == 1) {
            decrementAmountButton.setDisable(false);
        } else if (menuItem.getOrderedAmount() == 0) {
            decrementAmountButton.setDisable(true);
        }
    }

    private void showComponentsList() {
        if (menuItem.getComponents().size() > 0) {
            componentsListView.setItems(FXCollections.observableList(menuItem.getComponents()));
        } else {
            componentsLabel.setVisible(false);
            componentsListView.setVisible(false);
        }
    }
}
