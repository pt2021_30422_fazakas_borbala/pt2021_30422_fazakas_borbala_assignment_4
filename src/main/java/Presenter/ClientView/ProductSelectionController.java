package Presenter.ClientView;

import BLL.DeliveryService.IDeliveryService;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controls the items selected for the order and the items available for ordering.
 */
public class ProductSelectionController {

    @FXML
    private Label totalPriceLabel;

    @FXML
    private ListView<CartItemClientViewModel> availableProductsListView;

    private ObservableList<CartItemClientViewModel> selectedItems;

    private ObservableList<CartItemClientViewModel> filteredItems;

    private Map<CartItemClientViewModel, ChangeListener<Number>> itemsToAmountListeners;

    private IDeliveryService deliveryService;

    /** Initialized the controller data. */
    @FXML
    void initialize() {
        availableProductsListView.setCellFactory(list -> new CartItemCell());
        filteredItems = FXCollections.observableArrayList();
        selectedItems = FXCollections.observableArrayList(item -> new Observable[]{item.getObservableOrderedAmount()});
        selectedItems.addListener(this::onSelectedItemsChange);
        availableProductsListView.setItems(filteredItems);
        itemsToAmountListeners = new HashMap<>();
        updatePriceLabel();
    }

    /**
     * Initialises the delivery service.
     * @param deliveryService is the service of the restaurant.
     */
    public void init(IDeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    /**
     * Updates the list of available, filtered items, which are displayed to the client.
     * @param newlyFilteredItems is the list of filtered items after the new filtering.
     */
    public void updateFilteredItemsList(List<CartItemClientViewModel> newlyFilteredItems) {
        // remove old items, which should be not present now
        for (CartItemClientViewModel oldFilteredItem : filteredItems) {
            if (!newlyFilteredItems.contains(oldFilteredItem)) {
                oldFilteredItem.getObservableOrderedAmount().removeListener(itemsToAmountListeners.get(oldFilteredItem));
                itemsToAmountListeners.remove(oldFilteredItem);
            }
        }
        filteredItems.removeIf(menuItem -> !newlyFilteredItems.contains(menuItem));

        // add new filtered items
        for (CartItemClientViewModel menuItem : newlyFilteredItems) {
            if (!filteredItems.contains(menuItem)) {
                if (selectedItems.contains(menuItem)) {
                    CartItemClientViewModel selectedItem =
                            selectedItems.get(selectedItems.indexOf(menuItem));
                    filteredItems.add(selectedItem);
                } else {
                    filteredItems.add(menuItem);
                }
            }
        }

        // listen to amount changes
        listenToFilteredItemAmountChanges();
    }

    private void listenToFilteredItemAmountChanges() {
        for (CartItemClientViewModel filteredItem : filteredItems) {
            if (!itemsToAmountListeners.containsKey(filteredItem)) {
                ChangeListener<Number> amountChangeListener = (observableValue, oldAmount, newAmount) -> {
                    if (oldAmount.equals(0) && newAmount.intValue() > 0) { // new selected item
                        selectedItems.add(filteredItem);
                    }
                };
                filteredItem.getObservableOrderedAmount().addListener(amountChangeListener);
                itemsToAmountListeners.put(filteredItem, amountChangeListener);
            }
        }
    }

    private void onSelectedItemsChange(ListChangeListener.Change<? extends CartItemClientViewModel> change) {
        while (change.next()) {
            if (change.wasUpdated()) {
                List<CartItemClientViewModel> changedItems =
                        selectedItems.subList(change.getFrom(), change.getTo());
                for (CartItemClientViewModel changedItem : changedItems) {
                    if (changedItem.getOrderedAmount() == 0) {
                        selectedItems.remove(changedItem);
                    }
                }
            }
        }
        updatePriceLabel();
    }

    private void updatePriceLabel() {
        totalPriceLabel.setText(String.valueOf(CartItemClientViewModel.getTotalPrice(selectedItems)));
    }

    public void onCartButtonCLicked() {
        FXMLLoader cartPaneLoader = new FXMLLoader(getClass().getResource(
                "/ClientView/cart_pane.fxml"));
        AnchorPane cartPane = null;
        try {
            cartPane = cartPaneLoader.load();
            CartController cartController = cartPaneLoader.getController();
            cartController.init(deliveryService,
                    selectedItems);
            Stage cartStage = new Stage();
            cartStage.setTitle("My Cart");
            cartStage.initOwner(this.totalPriceLabel.getScene().getWindow());
            cartStage.initModality(Modality.WINDOW_MODAL);
            cartStage.setScene(new Scene(cartPane));
            cartStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    public void onPlaceOrderButtonClicked() {
        onCartButtonCLicked();
    }
}
