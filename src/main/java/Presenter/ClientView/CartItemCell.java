package Presenter.ClientView;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * Represents a cell in a list for cart items.
 */
public class CartItemCell extends ListCell<CartItemClientViewModel> {
    private final AnchorPane menuItemPane;
    private final CartItemController cartItemController;

    /**
     * Creates a new list cell.
     */
    public CartItemCell() {
        super();
        AnchorPane menuItemPane1;

        FXMLLoader menuItemPaneLoader = new FXMLLoader(getClass().getResource(
                "/ClientView/menu_item_pane.fxml"));
        try {
            menuItemPane1 = menuItemPaneLoader.load();
        } catch (IOException e) {
            menuItemPane1 = null;
            e.printStackTrace();
        }
        menuItemPane = menuItemPane1;
        cartItemController = menuItemPaneLoader.getController();
        setText(null);
    }


    /**
     * Fills the list cell with data.
     * @param item holds the data of the men
     * @param empty shows whether the cell is empty.
     */
    @Override
    public void updateItem(final CartItemClientViewModel item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setEditable(false);
            cartItemController.initMenuItem(item);
            setGraphic(menuItemPane);
        } else {
            setGraphic(null);
        }
    }
}