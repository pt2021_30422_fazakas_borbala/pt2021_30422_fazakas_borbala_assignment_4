package Presenter.ClientView;

import BLL.DeliveryService.IDeliveryService;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controls the pane for the client's cart, which allows the user to view the cart's content and
 * place the order.
 */
public class CartController {

    @FXML
    private ListView<CartItemClientViewModel> selectedProductsListView;

    @FXML
    private Button checkoutButton;

    @FXML
    private Label totalPriceLabel;

    private ObservableList<CartItemClientViewModel> selectedItems;

    private IDeliveryService deliveryService;

    @FXML
    void initialize() {
        selectedProductsListView.setCellFactory(list -> new CartItemCell());
    }

    /**
     * Initialises the delivery service and the list of items in the cart.
     * @param deliveryService is the service of the restaurant.
     * @param selectedItems is the list of items selected to the cart.
     */
    public void init(IDeliveryService deliveryService,
                     ObservableList<CartItemClientViewModel> selectedItems) {
        this.deliveryService = deliveryService;
        this.selectedItems = selectedItems;
        selectedProductsListView.setItems(this.selectedItems);
        this.selectedItems.addListener((ListChangeListener<CartItemClientViewModel>) change -> onSelectedAmountChanged());
        onSelectedAmountChanged();
    }

    /**
     * When the checkout button is clicked, the new order is placed.
     */
    public void onCheckoutButtonClicked() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Order");
        alert.setHeaderText("Are you sure you want to place this order? ");
        alert.setContentText("Total price: " + CartItemClientViewModel.getTotalPrice(selectedItems));

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            deliveryService.createOrder(selectedItems.stream()
                    .collect(Collectors.toMap(
                            CartItemClientViewModel::getTitle,
                            CartItemClientViewModel::getOrderedAmount)));
            onSuccessfulOrder();
        }
    }

    private void onSuccessfulOrder() {
        clearCart();
        ((Stage) selectedProductsListView.getScene().getWindow()).close();
        showSuccessfulOrderAlert();
    }

    private void showSuccessfulOrderAlert() {
        Alert successAlert = new Alert(Alert.AlertType.WARNING);
        successAlert.setTitle("Success");
        successAlert.setHeaderText("We received your order and will do our best to prepare the " +
                "meals as soon as possible.");
        successAlert.show();
    }

    private void clearCart() {
        List<CartItemClientViewModel> orderedItems = new ArrayList<>(selectedItems);
        for (CartItemClientViewModel orderedItem : orderedItems) {
            orderedItem.setOrderedAmount(0);
        }
    }

    private void onSelectedAmountChanged() {
        updatePriceLabel();
        updateCheckoutButtonAvailability();
    }

    private void updatePriceLabel() {
        totalPriceLabel.setText(String.valueOf(CartItemClientViewModel.getTotalPrice(selectedItems)));
    }

    private void updateCheckoutButtonAvailability() {
        checkoutButton.setDisable(selectedItems.size() == 0);
    }
}
