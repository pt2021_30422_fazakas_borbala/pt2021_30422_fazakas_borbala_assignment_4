package Presenter.ClientView;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Holds the data of a cart item, including the data of a menu item together with the ordered
 * amount.
 */
public class CartItemClientViewModel {
    private final String title;
    private final double price;
    private final int proteinsAmount;
    private final int fatsAmount;
    private final int sodiumAmount;
    private final int caloriesAmount;
    private final double rating;
    private final IntegerProperty orderedAmount;
    private final List<String> components;

    /**
     * Creates a new CartItem.
     * @param title is the title of the item in the cart.
     * @param price is the price of the item.
     * @param proteinsAmount represents the amount of proteins in the item.
     * @param fatsAmount represents the amount of fat in the item.
     * @param sodiumAmount represents the amount of sodium in the item.
     * @param caloriesAmount represents the amount of calories in the item.
     * @param rating is the rating of the item.
     * @param orderedAmount represents the ordered amount from the item.
     * @param components represents the subcomponents of the item.
     */
    public CartItemClientViewModel(String title, double price, int proteinsAmount, int fatsAmount
            , int sodiumAmount, int caloriesAmount, double rating, int orderedAmount,
                                   List<String> components) {
        this.title = Objects.requireNonNull(title);
        this.price = price;
        this.proteinsAmount = proteinsAmount;
        this.fatsAmount = fatsAmount;
        this.sodiumAmount = sodiumAmount;
        this.caloriesAmount = caloriesAmount;
        this.rating = rating;
        this.orderedAmount = new SimpleIntegerProperty(orderedAmount);
        this.components = new ArrayList<>(components);
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getProteinsAmount() {
        return proteinsAmount;
    }

    public int getFatsAmount() {
        return fatsAmount;
    }

    public int getSodiumAmount() {
        return sodiumAmount;
    }

    public int getCaloriesAmount() {
        return caloriesAmount;
    }

    public double getRating() {
        return rating;
    }

    public int getOrderedAmount() {
        return orderedAmount.get();
    }

    public IntegerProperty getObservableOrderedAmount() {
        return orderedAmount;
    }

    public void setOrderedAmount(int orderedAmount) {
        this.orderedAmount.setValue(orderedAmount);
    }

    public List<String> getComponents() {
        return components;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartItemClientViewModel)) return false;
        CartItemClientViewModel that = (CartItemClientViewModel) o;
        return title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    public static double getTotalPrice(List<CartItemClientViewModel> menuItems) {
        return menuItems.stream().mapToDouble(item -> item.getOrderedAmount() * item.getPrice()).sum();
    }
}
