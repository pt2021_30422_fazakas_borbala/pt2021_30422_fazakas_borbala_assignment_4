package Presenter.ClientView;

import BLL.DeliveryService.DeliveryService;
import BLL.Filters.MenuItemFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Filtering.FilterAccordionController;
import Presenter.Login.LoginPaneController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

/**
 * Controls the main pane for the client.
 */
public class ClientPaneController {
    @FXML
    private Button logoutButton;

    @FXML
    private AnchorPane filterPane;

    @FXML
    private AnchorPane parentPane;

    private DeliveryService deliveryService = null;
    private FilterAccordionController filterAccordionController;
    private ProductSelectionController productSelectionController;

    /** Setup pane. */
    @FXML
    public void initialize() {
        loadFilterAccordion();
    }

    /**
     * Sets the delivery service to communicate with.
     * @param deliveryService is the service representing the restaurant/
     */
    public void initDeliveryService(DeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
        loadProductSelectionPane();
        onApplyFiltersButtonClicked();
    }

    /**
     * When the logout button is clicked, the user is logged out and brought back to the login
     * pane.
     */
    public void onLogoutButtonClicked() {
        deliveryService.logOut();
        loadLoginStage();
        ((Stage) this.logoutButton.getScene().getWindow()).close();
    }

    /**
     * When the applyFiltersButton is clicked, the filters specified by the user are collected,
     * applied on the set of existing menu items and the filtered items are displayed.
     */
    public void onApplyFiltersButtonClicked() {
        List<MenuItemFilter> filters = null;
        try {
            filters = filterAccordionController.getFilters();
            List<CartItemClientViewModel> filteredMenuItems =
                    deliveryService.getAvailableFilteredMenuItems(filters);
            productSelectionController.updateFilteredItemsList(filteredMenuItems);
        } catch (InvalidUserInputException e) {
            // cannot filter
        }
    }

    private void loadLoginStage() {
        FXMLLoader loginPaneLoader = new FXMLLoader(getClass().getResource(
                "/login_pane.fxml"));
        AnchorPane loginPane = null;
        try {
            loginPane = loginPaneLoader.load();
            LoginPaneController loginPaneController = loginPaneLoader.getController();
            loginPaneController.initDeliveryService(deliveryService);
            Stage loginStage = new Stage();
            loginStage.setTitle("Login");
            loginStage.setScene(new Scene(loginPane));
            loginStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void loadFilterAccordion() {
        FXMLLoader filterAccordionLoader = new FXMLLoader(getClass().getResource(
                "/Filtering/filter_accordion.fxml"));
        Accordion filterAccordion = null;
        try {
            filterAccordion = filterAccordionLoader.load();
            this.filterAccordionController = filterAccordionLoader.getController();
            filterPane.getChildren().add(filterAccordion);
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void loadProductSelectionPane() {
        FXMLLoader productsPaneLoader = new FXMLLoader(getClass().getResource(
                "/ClientView/product_selection_pane.fxml"));
        AnchorPane productsPane = null;
        try {
            productsPane = productsPaneLoader.load();
            productSelectionController = productsPaneLoader.getController();
            productSelectionController.init(deliveryService);
            parentPane.getChildren().add(productsPane);
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }
}
