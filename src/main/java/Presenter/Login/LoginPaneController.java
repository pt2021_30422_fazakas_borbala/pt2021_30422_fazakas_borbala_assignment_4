package Presenter.Login;

import BLL.DeliveryService.DeliveryService;
import Model.Users.User;
import Presenter.AdminView.AdminPaneController;
import Presenter.ClientView.ClientPaneController;
import Presenter.EmployeeView.EmployeePaneController;
import Presenter.Util.AlertFactory;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controls the login pane which allows the user to log in or to opt for registration.
 */
public class LoginPaneController {
    /** Text field for entering the username. */
    @FXML
    public TextField userNameTextField;

    /** Text field for entering the password. */
    @FXML
    public TextField passwordField;

    /** Button to signal the intention of logging in with the specified data. */
    @FXML
    public Button loginButton;

    /** Button to signal the intention of registering a new user. */
    @FXML
    public Button registerButton;

    private DeliveryService deliveryService = null;

    /**
     * Sets the deliveryService.
     * @param deliveryService is the service to communicate with.
     */
    public void initDeliveryService(DeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
    }

    /**
     * Loads in a default window for an empliyee, indenepndenly form the login, to show how the
     * observable pattern for the employee works in case a client adds an order.
     */
    public void showEmployeeWindow() {
        loadDefaultEmployeeStage();
    }

    /**
     * Sends a request to the delivery service for login and handles the login result.
     */
    public void onLoginButtonClicked() {
        String userName = userNameTextField.getText();
        String password = passwordField.getText();
        if (deliveryService.logIn(userName, password)) {
            loadMainStage(deliveryService.getLoggedInUserType().get());
            ((Stage) this.userNameTextField.getScene().getWindow()).close();
        } else {
            AlertFactory.showLoginErrorAlert();
        }
    }

    /**
     * Opens a new window for registration.
     */
    public void onRegisterButtonClicked() {
        FXMLLoader registerPaneLoader = new FXMLLoader(getClass().getResource(
                "/register_pane.fxml"));
        try {
            AnchorPane registerPane = registerPaneLoader.load();
            RegisterPaneController registerPaneController = registerPaneLoader.getController();
            registerPaneController.initDeliveryService(deliveryService);
            Stage registerStage = new Stage();
            registerStage.setTitle("Register");
            registerStage.initOwner(this.userNameTextField.getScene().getWindow());
            registerStage.initModality(Modality.WINDOW_MODAL);
            registerStage.setScene(new Scene(registerPane));
            registerStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void loadMainStage(User.UserType userType) {
        FXMLLoader mainPaneLoader = new FXMLLoader(getClass().getResource(
                getMainPanePath(userType)));
        AnchorPane mainPane = null;
        try {
            mainPane = mainPaneLoader.load();
            initMainPaneDeliveryService(userType, mainPaneLoader);
            Stage mainStage = new Stage();
            mainStage.setTitle("Home");
            mainStage.setScene(new Scene(mainPane));
            mainStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private String getMainPanePath(User.UserType userType) {
        switch (userType) {
            case CLIENT:
                return "/ClientView/client_main_pane.fxml";
            case EMPLOYEE:
                return "/EmployeeView/employee_main_pane.fxml";
            case ADMIN:
                return "/AdminView/admin_main_pane.fxml";
        }
        return null;
    }

    private void initMainPaneDeliveryService(User.UserType userType, FXMLLoader paneLoader) {
        switch (userType) {
            case CLIENT:
                ((ClientPaneController) paneLoader.getController()).initDeliveryService(deliveryService);
                break;
            case EMPLOYEE:
                ((EmployeePaneController) paneLoader.getController()).initDeliveryService(deliveryService);
                break;
            case ADMIN:
                ((AdminPaneController) paneLoader.getController()).initDeliveryService(deliveryService);
                break;
        }
    }

    private void loadDefaultEmployeeStage() {
        FXMLLoader mainPaneLoader = new FXMLLoader(getClass().getResource(
                getMainPanePath(User.UserType.EMPLOYEE)));
        AnchorPane mainPane = null;
        try {
            mainPane = mainPaneLoader.load();
            initMainPaneDeliveryService(User.UserType.EMPLOYEE, mainPaneLoader);
            ((EmployeePaneController) mainPaneLoader.getController()).disableLogoutOption();
            Stage mainStage = new Stage();
            mainStage.setTitle("Home - Default Employee");
            mainStage.setScene(new Scene(mainPane));
            mainStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }
}
