package Presenter.Login;

import BLL.DeliveryService.IDeliveryService;
import BLL.Validators.UserValidation.UserValidator;
import BLL.Validators.ValidationResult;
import Model.Users.User;
import Presenter.Util.AlertFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controls the registration pane which allows the user to register a new user for the application.
 */
public class RegisterPaneController {
    /** Text field for entering the username. */
    @FXML
    public TextField userNameTextField;

    /** Text field for entering the password. */
    @FXML
    public PasswordField passwordField;

    /** Button to signal the intention of registering a new user with the specified data. */
    @FXML
    public Button registerButton;

    private IDeliveryService deliveryService = null;

    private final UserValidator userValidator = new UserValidator();

    /**
     * Sets the deliveryService.
     * @param deliveryService is the service to communicate with.
     */
    public void initDeliveryService(IDeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
    }

    /**
     * Sends a request to the delivery service for registration and handles the registration result.
     */
    public void onRegisterButtonClicked() {
        String userName = userNameTextField.getText();
        String password = passwordField.getText();

        ValidationResult<User> validationResult = userValidator.validate(userName, password);
        if (validationResult.isValid()) {
            if (!deliveryService.existsUserWithUsername(userName)) {
                deliveryService.registerClient(userName, password);
                ((Stage) this.userNameTextField.getScene().getWindow()).close();
            } else {
                AlertFactory.showDuplicateUsernameError(userName);
            }
        } else {
            AlertFactory.showInvalidDataError(validationResult);
        }
    }

}
