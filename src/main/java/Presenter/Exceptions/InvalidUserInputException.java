package Presenter.Exceptions;

/**
 * Exception thrown if any of the user inputs is invalid so that the expected command cannot be
 * executed.
 */
public class InvalidUserInputException extends Exception {
}
