package Presenter.Filtering;

import BLL.Filters.MenuItemFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Filtering.FilterControllers.FilterController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Controller for the accordion containing all filter panes for filtering menu items.
 */
public class FilterAccordionController {
    /** Pane for search keyword filtering. */
    @FXML
    public TitledPane searchKeywordsTitlePane;

    /** Pane for price filtering. */
    @FXML
    public TitledPane priceFilterTitlePane;

    /** Pane for calories amount filtering. */
    @FXML
    public TitledPane caloriesFilterTitlePane;

    /** Pane for fat amount filtering. */
    @FXML
    public TitledPane fatAmountFilterTitlePane;

    /** Pane for proteins amount filtering. */
    @FXML
    public TitledPane proteinsAmountFilterPane;

    /** Pane for sodium amount filtering. */
    @FXML
    public TitledPane sodiumAmountFilterTitlePane;

    /** Pane for rating filtering. */
    @FXML
    public TitledPane ratingFilterTitlePane;

    private Map<TitledPane, String> titlePanesToContentSource;

    private final List<FilterController> filterControllerList = new ArrayList<>();

    /** Setup accordin. */
    @FXML
    public void initialize() {
        titlePanesToContentSource = Map.of(
                searchKeywordsTitlePane, "/Filtering/FilterPanes/keyword_filter_pane.fxml",
                priceFilterTitlePane, "/Filtering/FilterPanes/price_filter_pane.fxml",
                caloriesFilterTitlePane, "/Filtering/FilterPanes/calories_filter_pane.fxml",
                fatAmountFilterTitlePane, "/Filtering/FilterPanes/fat_filter_pane.fxml",
                proteinsAmountFilterPane, "/Filtering/FilterPanes/proteins_filter_pane.fxml",
                sodiumAmountFilterTitlePane, "/Filtering/FilterPanes/sodium_filter_pane.fxml",
                ratingFilterTitlePane, "/Filtering/FilterPanes/rating_filter_pane.fxml"
        );
        loadFilterPanes();
    }

    /**
     * Collects and returns all filters from the individual filter controllers.
     * @return the list of filters selected by the user.
     * @throws InvalidUserInputException if any of the user data was invalid.
     */
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        List<List<MenuItemFilter>> filterLists = new ArrayList<>();
        for (FilterController filterController : filterControllerList) {
            filterLists.add(filterController.getFilters());
        }
        return filterLists.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    private void loadFilterPanes() {
        for (TitledPane titledPane : titlePanesToContentSource.keySet()) {
            FXMLLoader filterPaneLoader = new FXMLLoader(getClass().getResource(
                    titlePanesToContentSource.get(titledPane)));
            AnchorPane filterPane = null;
            try {
                filterPane = filterPaneLoader.load();
                this.filterControllerList.add(filterPaneLoader.getController());
                titledPane.setContent(filterPane);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
