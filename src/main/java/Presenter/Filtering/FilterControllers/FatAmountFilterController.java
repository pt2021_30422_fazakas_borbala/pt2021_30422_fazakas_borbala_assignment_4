package Presenter.Filtering.FilterControllers;

import BLL.Filters.FatsAmountFilter;
import BLL.Filters.MenuItemFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Util.AlertFactory;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Handles the pane which allows the user to specify a fat amount filter.
 */
public class FatAmountFilterController implements FilterController {
    /** TextField to specify the lower limit of fat amount. */
    @FXML
    public TextField minFatAmountTextField;

    /** TextField to specify the upper limit of fat amount. */
    @FXML
    public TextField maxFatAmountTextField;

    /** CheckBox to enable/disable the fat amount filter. */
    @FXML
    public CheckBox enableFilterCheckBox;

    /** Setup for pane. */
    @FXML
    public void initialize() {
        setupNumericInputs();
        enableFilterCheckBox.setOnAction(actionEvent -> onCheckBoxStateChange());
    }

    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        if (enableFilterCheckBox.isSelected()) {
            try {
                int minFatAmount = Integer.parseInt(minFatAmountTextField.getText());
                int maxFatAmount = Integer.parseInt(maxFatAmountTextField.getText());
                if (minFatAmount <= maxFatAmount) {
                    return List.of(new FatsAmountFilter(minFatAmount, maxFatAmount));
                } else {
                    AlertFactory.showMinimumLargerThanMaximumError("Fat Amount constraint");
                    throw new InvalidUserInputException();
                }
            } catch (NumberFormatException e) {
                AlertFactory.showIntegerNumberFormatError("The Fat Amount constraints");
                throw new InvalidUserInputException();
            }
        } else {
            return List.of();
        }
    }

    private void onCheckBoxStateChange() {
        if (!enableFilterCheckBox.isSelected()) {
            maxFatAmountTextField.setDisable(true);
            minFatAmountTextField.setDisable(true);
        } else {
            maxFatAmountTextField.setDisable(false);
            minFatAmountTextField.setDisable(false);
        }
    }

    private void setupNumericInputs() {
        minFatAmountTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        maxFatAmountTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
    }
}
