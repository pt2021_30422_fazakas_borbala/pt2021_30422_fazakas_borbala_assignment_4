package Presenter.Filtering.FilterControllers;

import BLL.Filters.MenuItemFilter;
import BLL.Filters.SodiumAmountFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Util.AlertFactory;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Handles the pane which allows the user to specify a sodium amount
 * filter.
 */
public class SodiumAmountFilterController implements FilterController {

    /** TextField to specify the lower limit of sodium amount. */
    @FXML
    public TextField minSodiumTextField;

    /** TextField to specify the upper limit of sodium amount. */
    @FXML
    public TextField maxSodiumTextField;

    /** CheckBox to enable/disable the sodium amount filter. */
    @FXML
    public CheckBox enableFilterCheckBox;

    /** Setup pane. */
    @FXML
    public void initialize() {
        setupNumericInputs();
        enableFilterCheckBox.setOnAction(actionEvent -> onCheckBoxStateChange());
    }

    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        if (enableFilterCheckBox.isSelected()) {
            try {
                int minSodium = Integer.parseInt(minSodiumTextField.getText());
                int maxSodium = Integer.parseInt(maxSodiumTextField.getText());
                if (minSodium <= maxSodium) {
                    return List.of(new SodiumAmountFilter(minSodium, maxSodium));
                } else {
                    AlertFactory.showMinimumLargerThanMaximumError("Sodium constraint");
                    throw new InvalidUserInputException();
                }
            } catch (NumberFormatException e) {
                AlertFactory.showIntegerNumberFormatError("The Sodium constraints");
                throw new InvalidUserInputException();
            }
        } else {
            return List.of();
        }
    }

    private void onCheckBoxStateChange() {
        if (!enableFilterCheckBox.isSelected()) {
            maxSodiumTextField.setDisable(true);
            minSodiumTextField.setDisable(true);
        } else {
            maxSodiumTextField.setDisable(false);
            minSodiumTextField.setDisable(false);
        }
    }

    private void setupNumericInputs() {
        minSodiumTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        maxSodiumTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
    }
}
