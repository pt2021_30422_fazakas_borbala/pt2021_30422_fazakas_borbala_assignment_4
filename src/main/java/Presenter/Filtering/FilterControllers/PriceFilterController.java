package Presenter.Filtering.FilterControllers;

import BLL.Filters.MenuItemFilter;
import BLL.Filters.PriceFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Util.AlertFactory;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Handles the pane which allows the user to specify a price filter.
 */
public class PriceFilterController implements FilterController {
    /** TextField to specify the lower limit of the price. */
    @FXML
    public TextField minPriceTextField;

    /** TextField to specify the upper limit of the price. */
    @FXML
    public TextField maxPriceTextField;

    /** CheckBox to enable/disable the fat amount filter. */
    @FXML
    public CheckBox enableFilterCheckBox;

    /** Setup for the pane. */
    @FXML
    public void initialize() {
        setupNumericInputs();
        enableFilterCheckBox.setOnAction(actionEvent -> onCheckBoxStateChange());
    }

    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        if (enableFilterCheckBox.isSelected()) {
            try {
                double minPrice = Double.parseDouble(minPriceTextField.getText());
                double maxPrice = Double.parseDouble(maxPriceTextField.getText());
                if (minPrice <= maxPrice) {
                    return List.of(new PriceFilter(minPrice, maxPrice));
                } else {
                    AlertFactory.showMinimumLargerThanMaximumError("price constraint");
                    throw new InvalidUserInputException();
                }
            } catch (NumberFormatException e) {
                AlertFactory.showFloatingPointNumberFormatError("The price constraints");
                throw new InvalidUserInputException();
            }
        } else {
            return List.of();
        }
    }

    private void onCheckBoxStateChange() {
        if (!enableFilterCheckBox.isSelected()) {
            maxPriceTextField.setDisable(true);
            minPriceTextField.setDisable(true);
        } else {
            maxPriceTextField.setDisable(false);
            minPriceTextField.setDisable(false);
        }
    }

    private void setupNumericInputs() {
        minPriceTextField.setTextFormatter(FormatterFactory.getDoubleFormatter());
        maxPriceTextField.setTextFormatter(FormatterFactory.getDoubleFormatter());
    }
}
