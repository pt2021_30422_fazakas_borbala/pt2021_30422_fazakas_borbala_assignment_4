package Presenter.Filtering.FilterControllers;

import BLL.Filters.MenuItemFilter;
import BLL.Filters.ProteinsAmountFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Util.AlertFactory;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Handles the pane which allows the user to specify a proteins amount filter.
 */
public class ProteinsFilterController implements FilterController {

    /** TextField to specify the lower limit of proteins amount. */
    @FXML
    public TextField minProteinsTextField;

    /** TextField to specify the lower limit of proteins amount. */
    @FXML
    public TextField maxProteinsTextField;

    /** CheckBox to enable/disable the proteins amount filter. */
    @FXML
    public CheckBox enableFilterCheckBox;

    /** Setup pane. */
    @FXML
    public void initialize() {
        setupNumericInputs();
        enableFilterCheckBox.setOnAction(actionEvent -> onCheckBoxStateChange());
    }

    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        if (enableFilterCheckBox.isSelected()) {
            try {
                int minProteins = Integer.parseInt(minProteinsTextField.getText());
                int maxProteins = Integer.parseInt(maxProteinsTextField.getText());
                if (minProteins <= maxProteins) {
                    return List.of(new ProteinsAmountFilter(minProteins, maxProteins));
                } else {
                    AlertFactory.showMinimumLargerThanMaximumError("Proteins constraint");
                    throw new InvalidUserInputException();
                }
            } catch (NumberFormatException e) {
                AlertFactory.showIntegerNumberFormatError("The Proteins constraints");
                throw new InvalidUserInputException();
            }
        } else {
            return List.of();
        }
    }

    private void onCheckBoxStateChange() {
        if (!enableFilterCheckBox.isSelected()) {
            maxProteinsTextField.setDisable(true);
            minProteinsTextField.setDisable(true);
        } else {
            maxProteinsTextField.setDisable(false);
            minProteinsTextField.setDisable(false);
        }
    }

    private void setupNumericInputs() {
        minProteinsTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        maxProteinsTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
    }
}
