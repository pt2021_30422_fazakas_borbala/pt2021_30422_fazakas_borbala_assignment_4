package Presenter.Filtering.FilterControllers;

import BLL.Filters.CaloriesFilter;
import BLL.Filters.MenuItemFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Util.AlertFactory;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Handles the pane which allows the user to specify a calories filter.
 */
public class CaloriesFilterController implements FilterController {
    /** TextField to specify the lower limit of calories amount. */
    @FXML
    public TextField minCaloriesTextField;

    /** TextField to specify the upper limit of calories amount. */
    @FXML
    public TextField maxCaloriesTextField;

    /** CheckBox to enable/disable the calories filter. */
    @FXML
    public CheckBox enableFilterCheckBox;

    /** Setup for the pane. */
    @FXML
    public void initialize() {
        setupNumericInputs();
        enableFilterCheckBox.setOnAction(actionEvent -> onCheckBoxStateChange());
    }

    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        if (enableFilterCheckBox.isSelected()) {
            try {
                int minCalories = Integer.parseInt(minCaloriesTextField.getText());
                int maxCalories = Integer.parseInt(maxCaloriesTextField.getText());
                if (minCalories <= maxCalories) {
                    return List.of(new CaloriesFilter(minCalories, maxCalories));
                } else {
                    AlertFactory.showMinimumLargerThanMaximumError("calories constraint");
                    throw new InvalidUserInputException();
                }
            } catch (NumberFormatException e) {
                AlertFactory.showIntegerNumberFormatError("The Calories constraints");
                throw new InvalidUserInputException();
            }
        } else {
            return List.of();
        }
    }

    private void onCheckBoxStateChange() {
        if (!enableFilterCheckBox.isSelected()) {
            maxCaloriesTextField.setDisable(true);
            minCaloriesTextField.setDisable(true);
        } else {
            maxCaloriesTextField.setDisable(false);
            minCaloriesTextField.setDisable(false);
        }
    }

    private void setupNumericInputs() {
        minCaloriesTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        maxCaloriesTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
    }
}
