package Presenter.Filtering.FilterControllers;

import BLL.Filters.MenuItemFilter;
import BLL.Filters.RatingFilter;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Util.AlertFactory;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Handles the pane which allows the user to specify a rating filter.
 */
public class RatingFilterController implements FilterController {

    /** TextField to specify the lower limit of the price. */
    @FXML
    public TextField minRatingTextField;

    /** Checkbox to enable/disable the rating filter. */
    @FXML
    public CheckBox enableFilterCheckBox;

    /** Setup pane. */
    @FXML
    public void initialize() {
        setupNumericInputs();
    }


    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() throws InvalidUserInputException {
        if (enableFilterCheckBox.isSelected()) {
            try {
                int minRating = Integer.parseInt(minRatingTextField.getText());
                return List.of(new RatingFilter(minRating));
            } catch (NumberFormatException e) {
                AlertFactory.showIntegerNumberFormatError("The Calories constraints");
                throw new InvalidUserInputException();
            }
        } else {
            return List.of();
        }
    }

    private void setupNumericInputs() {
        minRatingTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
    }
}
