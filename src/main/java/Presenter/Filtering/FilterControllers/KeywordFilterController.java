package Presenter.Filtering.FilterControllers;

import BLL.Filters.KeywordFilter;
import BLL.Filters.MenuItemFilter;
import Presenter.Util.AlertFactory;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.util.Callback;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles the pane which allows the user to specify keyword filters.
 */
public class KeywordFilterController implements FilterController {
    /** TextField for specifying a new keyword. */
    @FXML
    public TextField newKeywordTextField;

    /** Button for adding the keyword in the newKeywordTextField. */
    @FXML
    public Button addNewKeywordButton;

    /** List of current keywords. */
    @FXML
    public ListView<String> searchKeywordsListView;

    private static final int MIN_KEYWORD_LENGTH = 1;

    /** Setup pane. */
    @FXML
    void initialize() {
        searchKeywordsListView.setCellFactory(new Callback<ListView<String>,
                ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> list) {
                return new KeywordCell();
            }
        });
    }

    /**
     * When the newKeywordButton is clicked, the keyword in the textfild is added to the
     * existing search keywords.
     */
    public void onNewKeywordButtonClicked() {
        String newKeyword = newKeywordTextField.getText();
        if (newKeyword.length() >= MIN_KEYWORD_LENGTH) {
            if (!searchKeywordsListView.getItems().contains(newKeyword)) {
                searchKeywordsListView.getItems().add(newKeyword);
                newKeywordTextField.setText("");
            } else {
                AlertFactory.showDuplicateSearchKeywordError();
            }
        } else {
            AlertFactory.showEmptySearchKeywordError();
        }
    }

    /** {@inheritDoc} */
    @Override
    public List<MenuItemFilter> getFilters() {
        return searchKeywordsListView.getItems().stream().distinct()
                .map(KeywordFilter::new).collect(Collectors.toList());
    }

    private class KeywordCell extends ListCell<String> {
        private final Label keywordLabel;
        private final GridPane pane;

        public KeywordCell() {
            super();

            Button deleteButton = new Button("Delete");
            deleteButton.setOnAction(event -> onDeleteButtonClicked());

            keywordLabel = new Label();
            pane = new GridPane();
            pane.add(keywordLabel, 0, 0);
            pane.add(deleteButton, 1, 0);
            ColumnConstraints column1 = new ColumnConstraints(100, 100, Double.MAX_VALUE);
            column1.setHgrow(Priority.ALWAYS);
            ColumnConstraints column2 = new ColumnConstraints(100);
            pane.getColumnConstraints().addAll(column1, column2); // first column gets any extra width
            setText(null);
        }


        @Override
        public void updateItem(final String item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setEditable(false);
                keywordLabel.setText(item);
                setGraphic(pane);
            } else {
                setGraphic(null);
            }
        }

        private void onDeleteButtonClicked() {
            searchKeywordsListView.getItems().remove(getItem());
        }
    }

}
