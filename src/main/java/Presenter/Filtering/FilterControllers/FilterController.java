package Presenter.Filtering.FilterControllers;

import BLL.Filters.MenuItemFilter;
import Presenter.Exceptions.InvalidUserInputException;

import java.util.List;

/**
 * General interface for filter controllers, which allow the user to specify certain menu item
 * filters.
 */
public interface FilterController {
    /**
     * Returns the filter corresponding to the user input.
     * @return the filter corresponding to the user input.
     * @throws InvalidUserInputException in case the user input is invalid.
     */
    List<MenuItemFilter> getFilters() throws InvalidUserInputException;
}
