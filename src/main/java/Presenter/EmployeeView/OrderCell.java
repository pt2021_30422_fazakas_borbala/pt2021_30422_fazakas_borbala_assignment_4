package Presenter.EmployeeView;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * Represents a list cell for an order.
 */
public class OrderCell extends ListCell<OrderEmployeeViewModel> {
    private final AnchorPane orderPane;
    private final OrderController orderController;

    /**
     * Creates a new cell.
     */
    public OrderCell() {
        super();
        AnchorPane orderPane1;

        FXMLLoader menuItemPaneLoader = new FXMLLoader(getClass().getResource(
                "/EmployeeView/order_pane.fxml"));
        try {
            orderPane1 = menuItemPaneLoader.load();
        } catch (IOException e) {
            orderPane1 = null;
            e.printStackTrace();
        }
        orderPane = orderPane1;
        orderController = menuItemPaneLoader.getController();
        setText(null);
    }


    /**
     * Fills the cell with the data of an order.
     * @param item holds the data of the order.
     * @param empty shows whether the cell should be empty.
     */
    @Override
    public void updateItem(final OrderEmployeeViewModel item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setEditable(false);
            orderController.intOrder(item);
            setGraphic(orderPane);
        } else {
            setGraphic(null);
        }
    }
}
