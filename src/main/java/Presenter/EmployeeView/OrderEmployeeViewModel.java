package Presenter.EmployeeView;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * ViewModel for an order from the employee's point of view.
 */
public class OrderEmployeeViewModel {
    private final String clientName;
    private final LocalDateTime dateTime;
    private final Map<String, Integer> orderedProductNamesToAmount;

    /**
     * Creates a new viemodel for an order.
     * @param clientName is the name of the client who placed the order.
     * @param dateTime is the time when the order was placed.
     * @param orderedProductNamesToAmount is a map from the name of the ordered products to their
     *                                   ordered quantity.
     */
    public OrderEmployeeViewModel(String clientName, LocalDateTime dateTime, Map<String, Integer> orderedProductNamesToAmount) {
        this.clientName = clientName;
        this.dateTime = dateTime;
        this.orderedProductNamesToAmount = orderedProductNamesToAmount;
    }

    public String getClientName() {
        return clientName;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Map<String, Integer> getOrderedProductNamesToAmount() {
        return orderedProductNamesToAmount;
    }
}
