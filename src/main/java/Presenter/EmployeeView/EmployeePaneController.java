package Presenter.EmployeeView;

import BLL.DeliveryService.DeliveryService;
import Presenter.Login.LoginPaneController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 * Controls the main pane for the employee. Listens to new orders in the deliveryService.
 */
public class EmployeePaneController implements PropertyChangeListener {

    /** Buttor for signaling intention of logging out. */
    @FXML
    private Button logoutButton;

    /** List of orders placed by clients since the login of the employee. */
    @FXML
    private ListView<OrderEmployeeViewModel> ordersListView;

    private DeliveryService deliveryService;

    @FXML
    public void initialize() {
        ordersListView.setCellFactory(list -> new OrderCell());
    }

    public void initDeliveryService(DeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
        this.deliveryService.addPropertyChangeListener(DeliveryService.EventType.NEW_ORDER.name()
                , this);
    }

    /**
     * Disables the logout option.
     */
    public void disableLogoutOption() {
        logoutButton.setDisable(true);
    }

    /**
     * When the logout button is clicked, the user is logged out andbrought back to the login
     * page.
     */
    public void onLogoutButtonClicked() {
        deliveryService.logOut();
        loadLoginStage();
        ((Stage) this.logoutButton.getScene().getWindow()).close();
    }

    private void loadLoginStage() {
        FXMLLoader loginPaneLoader = new FXMLLoader(getClass().getResource(
                "/login_pane.fxml"));
        AnchorPane loginPane = null;
        try {
            loginPane = loginPaneLoader.load();
            LoginPaneController loginPaneController = loginPaneLoader.getController();
            loginPaneController.initDeliveryService(deliveryService);
            Stage loginStage = new Stage();
            loginStage.setTitle("Login");
            loginStage.setScene(new Scene(loginPane));
            loginStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /**
     * The controller listens to new orders placed by clients in the deliveryService and displays
     * them in the listview.
     * @param evt is the event that occurred in the deliveryService.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(DeliveryService.EventType.NEW_ORDER.name())) {
            ordersListView.getItems().add((OrderEmployeeViewModel) evt.getNewValue());
        }
    }
}
