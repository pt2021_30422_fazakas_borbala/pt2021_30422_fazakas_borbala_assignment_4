package Presenter.EmployeeView;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

/**
 * Controls the pane representing an order.
 */
public class OrderController {

    @FXML
    private Label dateTimeLabel;

    @FXML
    private ListView<String> orderedItemsListView;

    @FXML
    private Label clientNameLabel;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(
            "yyyy.MM.dd HH:mm");

    /**
     * Fills the pane with the data of an order.
     * @param order is the order whose data is displayed.
     */
    public void intOrder(OrderEmployeeViewModel order) {
        clientNameLabel.setText(order.getClientName());
        dateTimeLabel.setText(DATE_TIME_FORMATTER.format(order.getDateTime()));
        orderedItemsListView.setItems(FXCollections.observableList(
                order.getOrderedProductNamesToAmount()
                        .entrySet().stream()
                        .map(entry -> entry.getValue() + " portions of " + entry.getKey())
                        .collect(Collectors.toList())));
    }
}
