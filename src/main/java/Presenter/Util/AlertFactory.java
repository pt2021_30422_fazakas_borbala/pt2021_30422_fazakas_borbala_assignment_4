package Presenter.Util;

import BLL.Validators.ValidationResult;
import javafx.scene.control.Alert;

/**
 * Generates alerts corresponding to different errors.
 */
public class AlertFactory {

    /**
     * Shows an alert in case login fails.
     */
    public static void showLoginErrorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Login Failed");
        alert.setHeaderText("This data doesn't seem right. Please check that both your username " +
                "and password are correct and try again!");
        alert.show();
    }

    /**
     * Shows an alert in case the user tries to save an empty search keyword.
     */
    public static void showEmptySearchKeywordError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Keyword Creation Failed");
        alert.setHeaderText("The serach keyword must not be empty!");
        alert.show();
    }

    /**
     * Shows an alert in case a search keyword is added twice.
     */
    public static void showDuplicateSearchKeywordError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Keyword Creation Failed");
        alert.setHeaderText("This search keyword already exists!");
        alert.show();
    }

    /**
     * Shows an alert in case a value which is supposed to be an integer cannot be converted to
     * an integer.
     * @param role specifies the role of the input which has the invalid value.
     */
    public static void showIntegerNumberFormatError(String role) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Parameters");
        alert.setHeaderText(role + " should have a integer values. Please correct the parameters " +
                "and try again");
        alert.show();
    }

    /**
     * Shows an alert in case a value which is supposed to be an  double cannot be converted to
     * an double.
     * @param role specifies the role of the input which has the invalid value.
     */
    public static void showFloatingPointNumberFormatError(String role) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Parameters");
        alert.setHeaderText(role + " should have a decimal values. Please correct the parameters " +
                "and try again");
        alert.show();
    }

    /**
     * Shows an alert in case a value which is supposed to be an integer cannot be converted to
     * an integer.
     * @param role specifies the role of the input which has the invalid value.
     */
    public static void showMinimumLargerThanMaximumError(String role) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Parameters");
        alert.setHeaderText("The lower limit for the " + role + " should be <= than the upper " +
                "limit");
        alert.show();
    }

    /**
     * Shows an alert in case the input specified by the user assumes that the end time of a user
     * is before the starting time.
     */
    public static void showStartTimeAfterEndTimeError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Parameters");
        alert.setHeaderText("The start time should be before the end time");
        alert.show();
    }

    /**
     * Shows an error if the data specified by the user cannot be successfully validated, and
     * displays the error message of the validation result.
     * @param result is the result of the validation.
     */
    public static void showInvalidDataError(ValidationResult result) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Data");
        alert.setHeaderText(result.getErrorMessage());
        alert.show();
    }

    /**
     * Shows an alert in case a user tries to register with a username which is already taken.
     * @param userName is the duplicated username.
     */
    public static void showDuplicateUsernameError(String userName) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("UserName taken");
        alert.setHeaderText("The username " + userName + " is already taken, please choose " +
                "another one");
        alert.show();
    }

    /**
     * Shows an alert in case the admin tries to assign to a product a name which is already
     * taken by another product.
     * @param productName is the taken productName.
     */
    public static void showDuplicateProductNameError(String productName) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Product name taken");
        alert.setHeaderText("The product name " + productName + " is already taken, please choose " +
                "another one");
        alert.show();
    }

    /**
     * Shows an alert in case the admin tries to save a composite product with no subcomponents.
     */
    public static void showMissingCOmponentsForCompositeProductError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Missing Components");
        alert.setHeaderText("A composite product should have at least one component. Please " +
                "select a component!");
        alert.show();
    }
}

