package Presenter.AdminView;

import BLL.DeliveryService.DeliveryService;
import Presenter.Login.LoginPaneController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller for the main pane for the admin.
 */
public class AdminPaneController {

    @FXML
    private Tab manageProductsTab;

    @FXML
    private Tab generateReportsTab;

    @FXML
    private Button logoutButton;

    private DeliveryService deliveryService;

    /**
     * Initializes the delivery service.
     * @param deliveryService is the service representing the restaurant.
     */
    public void initDeliveryService(DeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
        try {
            setupProductManagementTab();
            setupReportGenerationTab();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /**
     * When the logout button is clicked, the user is logged out and brought back to the login
     * pane.
     */
    public void onLogoutButtonClicked() {
        deliveryService.logOut();
        loadLoginStage();
        ((Stage) this.logoutButton.getScene().getWindow()).close();
    }

    private void loadLoginStage() {
        FXMLLoader loginPaneLoader = new FXMLLoader(getClass().getResource(
                "/login_pane.fxml"));
        AnchorPane loginPane = null;
        try {
            loginPane = loginPaneLoader.load();
            LoginPaneController loginPaneController = loginPaneLoader.getController();
            loginPaneController.initDeliveryService(deliveryService);
            Stage loginStage = new Stage();
            loginStage.setTitle("Login");
            loginStage.setScene(new Scene(loginPane));
            loginStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void setupProductManagementTab() throws IOException {
        FXMLLoader productsPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/manage_products_pane.fxml"));
        AnchorPane productsPane = productsPaneLoader.load();
        ((ProductsManagementController) productsPaneLoader.getController()).initDeliveryService(deliveryService);
        manageProductsTab.setContent(productsPane);
    }

    private void setupReportGenerationTab() throws IOException {
        FXMLLoader reportsPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/generate_reports_pane.fxml"));
        AnchorPane reportsPane = reportsPaneLoader.load();
        ((ReportGenerationController) reportsPaneLoader.getController()).initDeliveryService(deliveryService);
        generateReportsTab.setContent(reportsPane);
    }
}
