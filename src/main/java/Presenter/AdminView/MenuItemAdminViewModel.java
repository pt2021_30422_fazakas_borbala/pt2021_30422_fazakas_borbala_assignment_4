package Presenter.AdminView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * View model representing a menu item.
 */
public class MenuItemAdminViewModel {
    private final String title;
    private final double price;
    private final int proteinsAmount;
    private final int fatsAmount;
    private final int sodiumAmount;
    private final int caloriesAmount;
    private final double rating;
    private final List<String> components;
    private final boolean available;

    /**
     * Creates a new view model.
     * @param title is the title of the item in the cart.
     * @param price is the price of the item.
     * @param proteinsAmount represents the amount of proteins in the item.
     * @param fatsAmount represents the amount of fat in the item.
     * @param sodiumAmount represents the amount of sodium in the item.
     * @param caloriesAmount represents the amount of calories in the item.
     * @param rating is the rating of the item.
     * @param components represents the subcomponents of the item.
     * @param available shows the availability of the product.
     */
    public MenuItemAdminViewModel(String title, double price, int proteinsAmount, int fatsAmount
            , int sodiumAmount, int caloriesAmount, double rating,
                                  List<String> components, boolean available) {
        this.title = Objects.requireNonNull(title);
        this.price = price;
        this.proteinsAmount = proteinsAmount;
        this.fatsAmount = fatsAmount;
        this.sodiumAmount = sodiumAmount;
        this.caloriesAmount = caloriesAmount;
        this.rating = rating;
        this.components = new ArrayList<>(components);
        this.available = available;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getProteinsAmount() {
        return proteinsAmount;
    }

    public int getFatsAmount() {
        return fatsAmount;
    }

    public int getSodiumAmount() {
        return sodiumAmount;
    }

    public int getCaloriesAmount() {
        return caloriesAmount;
    }

    public double getRating() {
        return rating;
    }

    public List<String> getComponents() {
        return components;
    }

    public boolean isAvailable() {
        return available;
    }

    public boolean isCompositeProduct() {
        return getComponents().size() > 0;
    }

    /**
     * Computes the total price of a list of menu items.
     * @param menuItems is the list of menuitems whose price is computed.
     * @return the total price of menuitems.
     */
    public static double getTotalPrice(List<MenuItemAdminViewModel> menuItems) {
        return menuItems.stream().mapToDouble(MenuItemAdminViewModel::getPrice).sum();
    }
}
