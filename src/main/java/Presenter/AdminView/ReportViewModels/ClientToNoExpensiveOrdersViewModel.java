package Presenter.AdminView.ReportViewModels;

/**
 * ViewModel holding the data of a client with the number of the expensive orders the client has placed.
 */
public class ClientToNoExpensiveOrdersViewModel {
    private final String clientName;
    private final long noExpensiveOrders;

    /**
     * Creates a new viewmodel.
     * @param clientName is the name of the client.
     * @param noExpensiveOrders is the number of the expensive orders the client has placed.
     */
    public ClientToNoExpensiveOrdersViewModel(String clientName, long noExpensiveOrders) {
        this.clientName = clientName;
        this.noExpensiveOrders = noExpensiveOrders;
    }

    public String getClientName() {
        return clientName;
    }

    public long getNoExpensiveOrders() {
        return noExpensiveOrders;
    }
}
