package Presenter.AdminView.ReportViewModels;

/**
 * Types of reports that can be generated for the admin.
 */
public enum ReportType {
    GET_ORDERS_IN_TIME_INTERVAL("Get the orders in a specific time interval"),
    GET_FREQUENTLY_ORDERED_PRODUCTS("Get the frequently ordered products"),
    GET_CLIENTS_WITH_MANY_EXPENSIVE_ORDERS("Get the clients with many expensive orders"),
    GET_PRODUCTS_ORDERED_ON_A_SPECIFIC_DAY("Get the products ordered on a specific day");

    private final String displayedValue;

    ReportType(String displayedValue) {
        this.displayedValue = displayedValue;
    }

    @Override
    public String toString() {
        return displayedValue;
    }
}
