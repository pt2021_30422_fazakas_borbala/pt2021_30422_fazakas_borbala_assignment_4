package Presenter.AdminView.ReportViewModels;

import java.time.LocalDateTime;

/**
 * ViewModel representing an order.
 */
public class OrderViewModel {
    private final int orderId;
    private final String clientName;
    private final LocalDateTime dateTime;
    private final double totalPrice;

    /**
     * Creates a new viewmodel.
     * @param orderId is the id of the order.
     * @param clientName is the name of the client who placed the order.
     * @param dateTime is the time when the order was placed.
     * @param totalPrice is the total price of the order.
     */
    public OrderViewModel(int orderId, String clientName, LocalDateTime dateTime, double totalPrice) {
        this.orderId = orderId;
        this.clientName = clientName;
        this.dateTime = dateTime;
        this.totalPrice = totalPrice;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getClientName() {
        return clientName;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public double getTotalPrice() {
        return totalPrice;
    }
}
