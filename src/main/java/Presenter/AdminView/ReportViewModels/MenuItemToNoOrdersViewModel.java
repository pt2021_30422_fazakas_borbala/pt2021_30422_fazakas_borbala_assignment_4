package Presenter.AdminView.ReportViewModels;

/**
 * Represents a viewmodel for a menu item together with the number of times it has been ordered.
 */
public class MenuItemToNoOrdersViewModel {
    private final String title;
    private final double price;
    private final int caloriesAmount;
    private final int proteinsAmount;
    private final int fatsAmount;
    private final int sodiumAmount;
    private final double rating;
    private final int noOrders;

    /**
     * Constructs a new viewmodel.
     * @param title is the title of the menuitem.
     * @param price is the price of the menuitem.
     * @param caloriesAmount is the amount of calories in the menuitem.
     * @param proteinsAmount is the amount of proteins in the menuitem.
     * @param fatsAmount is the amount of fats in the menuitem.
     * @param sodiumAmount is the amount of sodium in the menuitem.
     * @param rating is the rating the menuitem.
     * @param noOrders is the number of times the menu item has been ordered.
     */
    public MenuItemToNoOrdersViewModel(String title, double price, int caloriesAmount, int proteinsAmount, int fatsAmount, int sodiumAmount, double rating, int noOrders) {
        this.title = title;
        this.price = price;
        this.caloriesAmount = caloriesAmount;
        this.proteinsAmount = proteinsAmount;
        this.fatsAmount = fatsAmount;
        this.sodiumAmount = sodiumAmount;
        this.rating = rating;
        this.noOrders = noOrders;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public int getCaloriesAmount() {
        return caloriesAmount;
    }

    public int getProteinsAmount() {
        return proteinsAmount;
    }

    public int getFatsAmount() {
        return fatsAmount;
    }

    public int getSodiumAmount() {
        return sodiumAmount;
    }

    public double getRating() {
        return rating;
    }

    public int getNoOrders() {
        return noOrders;
    }
}
