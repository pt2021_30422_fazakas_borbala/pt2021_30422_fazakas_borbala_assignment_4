package Presenter.AdminView.ReportControllers;

import Presenter.AdminView.ReportViewModels.ClientToNoExpensiveOrdersViewModel;
import Presenter.Util.FormatterFactory;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * Report controller for the report about the clients who placed at least a specified amount of 
 * order of at least a specified price.
 */
public class ClientsWithManyExpensiveOrdersReportController extends ReportController {

    @FXML
    private TextField minOrderNoField;

    @FXML
    private TextField minPriceTextField;

    @FXML
    private TableView<ClientToNoExpensiveOrdersViewModel> reportTable;

    /** Setup pane. */
    @FXML
    public void initialize() {
        minOrderNoField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        minPriceTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        setupColumns(reportTable, ClientToNoExpensiveOrdersViewModel.class);
    }

    /**
     * When the generate button is clicked, the report is generated and the result is displayed
     * in the table.
     */
    public void onGenerateButtonClicked() {
        List<ClientToNoExpensiveOrdersViewModel> clients =
                deliveryService.getClientsWithManyExpensiveOrders(Integer.parseInt(minOrderNoField.getText()),
                        Integer.parseInt(minPriceTextField.getText()));
        reportTable.setItems(FXCollections.observableList(clients));
    }
}
