package Presenter.AdminView.ReportControllers;

import Presenter.AdminView.ReportViewModels.MenuItemToNoOrdersViewModel;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.time.LocalDate;
import java.util.List;

/**
 * Report controller for the report about the products which have been ordered on a specific day.
 */
public class ProductsOnDayReportController extends ReportController {

    @FXML
    private DatePicker datePicker;

    @FXML
    private TableView<MenuItemToNoOrdersViewModel> reportTable;

    /** Setup pane. */
    @FXML
    public void initialize() {
        setupColumns();
        datePicker.setValue(LocalDate.now());
    }

    /**
     * When the generate button is clicked, the report is generated and the result is displayed
     * in the table.
     */
    public void onGenerateButtonClicked() {
        List<MenuItemToNoOrdersViewModel> productsOnSpeicifcDay =
                deliveryService.getMenuItemsOrderedOnSpecificDay(datePicker.getValue());
        reportTable.setItems(FXCollections.observableList(productsOnSpeicifcDay));
    }

    private void setupColumns() {
        TableColumn<MenuItemToNoOrdersViewModel, String> itemNameColumn = new TableColumn<>(
                "Product Name");
        itemNameColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        itemNameColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.35));
        reportTable.getColumns().add(itemNameColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> itemPriceColumn = new TableColumn<>(
                "Price");
        itemPriceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        itemPriceColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(itemPriceColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> caloriesColumn = new TableColumn<>(
                "Calories");
        caloriesColumn.setCellValueFactory(new PropertyValueFactory<>("caloriesAmount"));
        caloriesColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(caloriesColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> fatColumn = new TableColumn<>(
                "Fats");
        fatColumn.setCellValueFactory(new PropertyValueFactory<>("fatsAmount"));
        fatColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(fatColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> proteinsColumn = new TableColumn<>(
                "Proteins");
        proteinsColumn.setCellValueFactory(new PropertyValueFactory<>("proteinsAmount"));
        proteinsColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(proteinsColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> sodiumAmountColumn = new TableColumn<>(
                "Sodium");
        sodiumAmountColumn.setCellValueFactory(new PropertyValueFactory<>("sodiumAmount"));
        sodiumAmountColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(sodiumAmountColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> ratingColumn = new TableColumn<>(
                "Rating");
        ratingColumn.setCellValueFactory(new PropertyValueFactory<>("rating"));
        ratingColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(ratingColumn);

        TableColumn<MenuItemToNoOrdersViewModel, String> noOrdersColumn = new TableColumn<>(
                "No. orders");
        noOrdersColumn.setCellValueFactory(new PropertyValueFactory<>("noOrders"));
        noOrdersColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.09));
        reportTable.getColumns().add(noOrdersColumn);
    }
}
