package Presenter.AdminView.ReportControllers;

import Presenter.AdminView.ReportViewModels.OrderViewModel;
import Presenter.Util.AlertFactory;
import com.calendarfx.view.TimeField;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Report controller for the report about the orders placed in a specific time interval,
 * independently from the date.
 */
public class OrdersInTimeIntervalReportController extends ReportController {

    private final TimeField startTimeField = new TimeField();

    private final TimeField endTimeField = new TimeField();

    @FXML
    private AnchorPane startTimeContainer;

    @FXML
    private AnchorPane endTimeContainer;

    @FXML
    private TableView<OrderViewModel> reportTable;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");

    /** Setup pane. */
    @FXML
    public void initialize() {
        startTimeContainer.getChildren().add(startTimeField);
        endTimeContainer.getChildren().add(endTimeField);
        setupColumns();
    }

    /**
     * When the generate button is clicked, the report is generated and the result is displayed
     * in the table.
     */
    public void onGenerateButtonClicked() {
        LocalTime startTime = startTimeField.getValue();
        LocalTime endTime = endTimeField.getValue();

        if (endTime.isBefore(startTime)) {
            AlertFactory.showStartTimeAfterEndTimeError();
            reportTable.getItems().clear();
        } else {
            List<OrderViewModel> ordersInTimeInterval =
                    deliveryService.getOrdersInTimeInterval(startTime, endTime);
            reportTable.setItems(FXCollections.observableList(ordersInTimeInterval));
        }
    }

    private void setupColumns() {
        TableColumn<OrderViewModel, String> orderIdColumn = new TableColumn<>(
                "Order Id.");
        orderIdColumn.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        orderIdColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.1));
        reportTable.getColumns().add(orderIdColumn);

        TableColumn<OrderViewModel, String> clientNameColumn = new TableColumn<>(
                "Client");
        clientNameColumn.setCellValueFactory(new PropertyValueFactory<>("clientName"));
        clientNameColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.3));
        reportTable.getColumns().add(clientNameColumn);

        TableColumn<OrderViewModel, LocalDateTime> dateTimeColumn = new TableColumn<>(
                "DateTime");
        dateTimeColumn.setCellValueFactory(new PropertyValueFactory<>("dateTime"));
        dateTimeColumn.setCellFactory(tc -> new TableCell<>() {
            @Override
            protected void updateItem(LocalDateTime dateTime, boolean empty) {
                super.updateItem(dateTime, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(DATE_TIME_FORMATTER.format(dateTime));
                }
            }
        });
        dateTimeColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.4));
        reportTable.getColumns().add(dateTimeColumn);

        TableColumn<OrderViewModel, String> fatColumn = new TableColumn<>(
                "Total Price");
        fatColumn.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));
        fatColumn.prefWidthProperty().bind(reportTable.widthProperty().multiply(0.15));
        reportTable.getColumns().add(fatColumn);
    }
}

