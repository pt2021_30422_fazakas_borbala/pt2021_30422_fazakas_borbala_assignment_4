package Presenter.AdminView.ReportControllers;

import BLL.DeliveryService.IDeliveryService;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.lang.reflect.Field;

/**
 * Controller for a report generator.
 */
public class ReportController {
    protected IDeliveryService deliveryService;

    /**
     * Constructs a ReportController.
     * @param deliveryService is the serrvice representingg the restaurant.
     */
    public void initDeliveryService(IDeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    /**
     * Sets up the columns of a tableview considering all fields of a class.
     * @param tableView is the table whose columns are set up.
     * @param type is the type of objects with which the table is going to be filled.
     * @param <T> is the type of the table. Must corredpond to the type.
     */
    protected <T> void setupColumns(TableView<T> tableView, Class<T> type) {
        Field[] entityFields = type.getDeclaredFields();
        for (Field field : entityFields) {
            TableColumn<T, String> column =
                    new TableColumn<>(field.getName());
            column.setCellValueFactory(new PropertyValueFactory<>(field.getName()));
            tableView.getColumns().add(column);
        }
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

}
