package Presenter.AdminView;

import BLL.DeliveryService.DeliveryService;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

/**
 * List cell representing a menu item.
 */
public class MenuItemCell extends ListCell<MenuItemAdminViewModel> {
    private final AnchorPane menuItemPane;
    private final MenuItemController menuItemController;
    private final DeliveryService deliveryService;

    /**
     * Constructs a new list cell.
     * @param deliveryService is the service representing the restaurant.
     */
    public MenuItemCell(DeliveryService deliveryService) {
        super();
        this.deliveryService = deliveryService;
        AnchorPane menuItemPane1;

        FXMLLoader menuItemPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/menu_item_pane.fxml"));
        try {
            menuItemPane1 = menuItemPaneLoader.load();
        } catch (IOException e) {
            menuItemPane1 = null;
            e.printStackTrace();
        }
        menuItemPane = menuItemPane1;
        menuItemController = menuItemPaneLoader.getController();
        setText(null);
    }


    /**
     * Updates the cell with the data of a new menuItem.
     * @param item is the item represented by the cell.
     * @param empty shows whether the cell is empty.
     */
    @Override
    public void updateItem(final MenuItemAdminViewModel item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setEditable(false);
            menuItemController.init(deliveryService, item);
            setGraphic(menuItemPane);
        } else {
            setGraphic(null);
        }
    }
}
