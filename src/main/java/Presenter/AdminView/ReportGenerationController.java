package Presenter.AdminView;

import BLL.DeliveryService.DeliveryService;
import Presenter.AdminView.ReportControllers.ReportController;
import Presenter.AdminView.ReportViewModels.ReportType;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller for the tab which allows the admin to generate reports.
 */
public class ReportGenerationController {

    @FXML
    private ChoiceBox<ReportType> reportTypeChoiceBox;

    @FXML
    private AnchorPane reportContainer;

    private DeliveryService deliveryService;

    private final Map<ReportType, String> reportTypeToPaneSource =
            Map.of(ReportType.GET_ORDERS_IN_TIME_INTERVAL, "/AdminView/ReportPanes" +
                            "/orders_in_time_interval_report.fxml",
                    ReportType.GET_PRODUCTS_ORDERED_ON_A_SPECIFIC_DAY, "/AdminView/ReportPanes" +
                            "/products_ordered_on_day_report.fxml",
                    ReportType.GET_FREQUENTLY_ORDERED_PRODUCTS, "/AdminView/ReportPanes" +
                            "/frequently_ordered_products_report.fxml",
                    ReportType.GET_CLIENTS_WITH_MANY_EXPENSIVE_ORDERS, "/AdminView/ReportPanes" +
                            "/clients_with_many_expensive_orders_report.fxml");
    private final Map<ReportType, AnchorPane> reporTypeToPane = new HashMap<>();

    /**
     * Sets the deliveryService.
     * @param deliveryService is the service representing the restaurant.
     */
    public void initDeliveryService(DeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
        reportTypeChoiceBox.setItems(FXCollections.observableList(Arrays.asList(ReportType.values())));
        for (ReportType reportType : reportTypeToPaneSource.keySet()) {
            setupReportPane(reportType);
        }
        reportTypeChoiceBox.getSelectionModel().selectedItemProperty().addListener((observableValue, reportType, t1) -> {
            reportContainer.getChildren().clear();
            reportContainer.getChildren().add(reporTypeToPane.get(t1));
        });
        reportTypeChoiceBox.setValue(ReportType.GET_ORDERS_IN_TIME_INTERVAL);
    }

    private void setupReportPane(ReportType reportType) {
        FXMLLoader reportPaneLoader = new FXMLLoader(getClass().getResource(
                reportTypeToPaneSource.get(reportType)));
        AnchorPane reportPane = null;
        try {
            reportPane = reportPaneLoader.load();
            reporTypeToPane.put(reportType, reportPane);
            ((ReportController) reportPaneLoader.getController())
                    .initDeliveryService(deliveryService);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
