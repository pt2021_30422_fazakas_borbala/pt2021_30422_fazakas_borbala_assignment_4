package Presenter.AdminView;

import BLL.DeliveryService.DeliveryService;
import Presenter.AdminView.ProductCreationModification.BaseProductDataSepcification.ModifyBaseProdController;
import Presenter.AdminView.ProductCreationModification.CompositeProductDataSepcification.ModifyCompProductController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Controller for the pane repserenting a menu item.
 */
public class MenuItemController {
    @FXML
    private Label titleLabel;

    @FXML
    private Label priceLabel;

    @FXML
    private Label proteinsAmountLabel;

    @FXML
    private Label fatsAmountLabel;

    @FXML
    private Label sodiumAmountLabel;

    @FXML
    private Label caloriesAmountLabel;

    @FXML
    private Label orderedAmountLabel;

    @FXML
    private ListView<String> componentsListView;

    @FXML
    private Label componentsLabel;

    @FXML
    private Label ratingLabel;

    @FXML
    private Button changeAvailabilityButtton;

    private MenuItemAdminViewModel menuItem;
    private DeliveryService deliveryService;

    private static final DecimalFormat df = new DecimalFormat("###.###");

    /**
     * Initializes the pane.
     * @param deliveryService is the service representing the restaurant.
     * @param menuItem is the item represented by the pane.
     */
    public void init(DeliveryService deliveryService, MenuItemAdminViewModel menuItem) {
        this.deliveryService = deliveryService;
        this.menuItem = menuItem;
        fillLabels();
        setupAvailabilityChangeButton();
        showComponentsList();
    }

    private void fillLabels() {
        titleLabel.setText(menuItem.getTitle());
        priceLabel.setText(df.format(menuItem.getPrice()));
        proteinsAmountLabel.setText(String.valueOf(menuItem.getProteinsAmount()));
        fatsAmountLabel.setText(String.valueOf(menuItem.getFatsAmount()));
        sodiumAmountLabel.setText(String.valueOf(menuItem.getSodiumAmount()));
        caloriesAmountLabel.setText(String.valueOf(menuItem.getCaloriesAmount()));
        ratingLabel.setText(df.format(menuItem.getRating()));
    }


    /**
     * When the edit button is clicked, a new frame is opened for editing the data of the menu
     * item.
     */
    public void onEditButtonClicked() {
        if (menuItem.isCompositeProduct()) {
            editCompositeProduct();
        } else {
            editBaseProduct();
        }
    }

    private void editCompositeProduct() {
        FXMLLoader modifyProductPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/CompositeProductDataSpecification/modify_composite_product_pane.fxml"));
        try {
            AnchorPane modifyProductPane = modifyProductPaneLoader.load();
            ModifyCompProductController modifyCompProductController =
                    modifyProductPaneLoader.getController();
            modifyCompProductController.init(deliveryService, menuItem);
            Stage modifyProductStage = new Stage();
            modifyProductStage.setTitle("Modify Product");
            modifyProductStage.initOwner(this.changeAvailabilityButtton.getScene().getWindow());
            modifyProductStage.initModality(Modality.WINDOW_MODAL);
            modifyProductStage.setScene(new Scene(modifyProductPane));
            modifyProductStage.setResizable(false);
            modifyProductStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /**
     * When the changeAvailabilityButton is clicked, the item is removed/added to the menu,
     * setting it to unavailable/available for the clients.
     */
    public void onChangeAvailabilityChangeButtonClicked() {
        if (menuItem.isAvailable()) {
            deliveryService.deleteProduct(menuItem.getTitle());
        } else {
            deliveryService.restoreProduct(menuItem.getTitle());
        }
    }

    private void setupAvailabilityChangeButton() {
        if (menuItem.isAvailable()) {
            changeAvailabilityButtton.setText("Remove from menu");
        } else {
            changeAvailabilityButtton.setText("Add to menu");
        }
    }

    private void editBaseProduct() {
        FXMLLoader modifyProductPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/BaseProductDataSpecification/modify_base_product_pane.fxml"));
        try {
            AnchorPane modifyProductPane = modifyProductPaneLoader.load();
            ModifyBaseProdController modifyBaseProductController =
                    modifyProductPaneLoader.getController();
            modifyBaseProductController.init(deliveryService, menuItem);
            Stage modifyProductStage = new Stage();
            modifyProductStage.setTitle("Modify Product");
            modifyProductStage.initOwner(this.changeAvailabilityButtton.getScene().getWindow());
            modifyProductStage.initModality(Modality.WINDOW_MODAL);
            modifyProductStage.setScene(new Scene(modifyProductPane));
            modifyProductStage.setResizable(false);
            modifyProductStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void showComponentsList() {
        boolean isCompositeProduct = menuItem.isCompositeProduct();
        if (isCompositeProduct) {
            componentsListView.setItems(FXCollections.observableList(menuItem.getComponents()));
        }
        componentsLabel.setVisible(isCompositeProduct);
        componentsListView.setVisible(isCompositeProduct);
    }
}
