package Presenter.AdminView;

import BLL.DeliveryService.DeliveryService;
import BLL.Filters.MenuItemFilter;
import Presenter.AdminView.ProductCreationModification.CreateProductController;
import Presenter.Exceptions.InvalidUserInputException;
import Presenter.Filtering.FilterAccordionController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Controller for the tab which allows admins to manage the products' data.
 */
public class ProductsManagementController implements PropertyChangeListener {

    @FXML
    private AnchorPane filterPane;

    @FXML
    private ListView<MenuItemAdminViewModel> filteredProductsListView;

    private FilterAccordionController filterAccordionController;

    private DeliveryService deliveryService;

    private ObservableList<MenuItemAdminViewModel> filteredItems;

    /** Setup pane. */
    @FXML
    private void initialize() {
        loadFilterAccordion();
        filteredItems = FXCollections.observableArrayList();
    }

    /**
     * Sets the deliveryService.
     * @param deliveryService is the service representing the restaurant.
     */
    public void initDeliveryService(DeliveryService deliveryService) {
        if (this.deliveryService != null) {
            throw new IllegalStateException("DeliveryService already initialised");
        }
        this.deliveryService = deliveryService;
        onApplyFiltersButtonClicked();
        filteredProductsListView.setCellFactory(list -> new MenuItemCell(deliveryService));
        filteredProductsListView.setItems(filteredItems);
        deliveryService.addPropertyChangeListener(DeliveryService.EventType.MENU_ITEM_DATA_CHANGE.name(), this);
    }

    /**
     * When the applyFiltersButton is clicked, the filters specified by the user are collected,
     * and the newly filtered items are displayed.
     */
    public void onApplyFiltersButtonClicked() {
        List<MenuItemFilter> filters = null;
        try {
            filters = filterAccordionController.getFilters();
            List<MenuItemAdminViewModel> filteredMenuItems = deliveryService.getFilteredMenuItems(filters);
            filteredItems.clear();
            filteredItems.addAll(filteredMenuItems);
        } catch (InvalidUserInputException e) {
            // cannot filter
        }
    }

    /**
     * This controller listens to changed in the products' data in the deliveryService to reflect
     * them immediately.
     * @param evt is the event that occurred in the deliveryService.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        onApplyFiltersButtonClicked();
    }

    /**
     * When the createProduct button is clicke, a new frame is opened which allows the admin to
     * create a new base or composte product.
     */
    public void onCreateProductButtonClicked() {
        FXMLLoader createProductPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/create_product_pane.fxml"));
        try {
            AnchorPane createProductPane = createProductPaneLoader.load();
            CreateProductController createProductController =
                    createProductPaneLoader.getController();
            createProductController.initDeliveryService(deliveryService);
            Stage createProductStage = new Stage();
            createProductStage.setTitle("Create New Product");
            createProductStage.initOwner(this.filteredProductsListView.getScene().getWindow());
            createProductStage.initModality(Modality.WINDOW_MODAL);
            createProductStage.setScene(new Scene(createProductPane));
            createProductStage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /**
     * When the loadProductsFromCSV button is clicked, the base items in the application's CSV
     * file are loaded into the menu of the restaurant.
     */
    public void onLoadProductsFromCSVButtonClicked() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Product Loading");
        alert.setHeaderText("Are you sure you want to load the products from the CSV file? ");
        alert.setContentText("By loading those products, you'll lose data of existing products " +
                "with the same name as any product in the CSV file");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            deliveryService.loadBaseProductsFromCSV();
        }
    }

    private void loadFilterAccordion() {
        FXMLLoader filterAccordionLoader = new FXMLLoader(getClass().getResource(
                "/Filtering/filter_accordion.fxml"));
        Accordion filterAccordion = null;
        try {
            filterAccordion = filterAccordionLoader.load();
            this.filterAccordionController = filterAccordionLoader.getController();
            filterPane.getChildren().add(filterAccordion);
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }
}
