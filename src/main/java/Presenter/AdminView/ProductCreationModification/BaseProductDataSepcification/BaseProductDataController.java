package Presenter.AdminView.ProductCreationModification.BaseProductDataSepcification;

import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.Util.FormatterFactory;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * Controller for the pane for specifying the data of a base product.
 */
public class BaseProductDataController {
    /** Text field for specifying the title of the menuitem. */
    @FXML
    public TextField nameTextField;

    /** Text field for specifying the price of the menuitem. */
    @FXML
    public TextField priceTextField;

    /** Text field for specifying the amount of calories in the menuitem. */
    @FXML
    public TextField caloriesTextField;

    /** Text field for specifying the amount of proteins in the menuitem. */
    @FXML
    public TextField proteinsTextField;

    /** Text field for specifying the amount of fats in the menuitem. */
    @FXML
    public TextField fatsTextField;

    /** Text field for specifying the amount of sodium in the menuitem. */
    @FXML
    public TextField sodiumTextField;

    /** Text field for specifying the availability status of the menuitem. */
    @FXML
    public RadioButton availableRadiuButton;

    @FXML
    public void initialize() {
        setupNumericInputs();
    }

    private void setupNumericInputs() {
        caloriesTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        proteinsTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        fatsTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        sodiumTextField.setTextFormatter(FormatterFactory.getIntegerFormatter());
        priceTextField.setTextFormatter(FormatterFactory.getDoubleFormatter());
    }

    /**
     * Fills the values of the input fields with the data of the given item.
     *
     * @param menuItem is the item whose data is set to the input fields.
     */
    public void fillFields(MenuItemAdminViewModel menuItem) {
        nameTextField.setPromptText(menuItem.getTitle());
        nameTextField.setText(menuItem.getTitle());
        priceTextField.setPromptText(String.valueOf(menuItem.getPrice()));
        priceTextField.setText(String.valueOf(menuItem.getPrice()));
        caloriesTextField.setPromptText(String.valueOf(menuItem.getCaloriesAmount()));
        caloriesTextField.setText(String.valueOf(menuItem.getCaloriesAmount()));
        proteinsTextField.setPromptText(String.valueOf(menuItem.getProteinsAmount()));
        proteinsTextField.setText(String.valueOf(menuItem.getProteinsAmount()));
        fatsTextField.setPromptText(String.valueOf(menuItem.getFatsAmount()));
        fatsTextField.setText(String.valueOf(menuItem.getFatsAmount()));
        sodiumTextField.setPromptText(String.valueOf(menuItem.getSodiumAmount()));
        sodiumTextField.setText(String.valueOf(menuItem.getSodiumAmount()));
        availableRadiuButton.setSelected(menuItem.isAvailable());
    }
}
