package Presenter.AdminView.ProductCreationModification.BaseProductDataSepcification;

import BLL.DeliveryService.DeliveryService;
import BLL.Validators.BaseProductValidation.BaseProductValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;
import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.Util.AlertFactory;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/** Controller for modifying the data of a base product. */
public class ModifyBaseProdController {
    @FXML
    private AnchorPane dataPane;

    private AnchorPane baseProductDataPane;

    private DeliveryService deliveryService;

    private BaseProductDataController baseProductDataController;

    private final BaseProductValidator baseProductValidator = new BaseProductValidator();

    private MenuItemAdminViewModel menuItem;

    /** Steup pane. */
    @FXML
    public void initialize() {
        setupBaseProductDataPane();
        dataPane.getChildren().add(baseProductDataPane);
    }

    /**
     * Sets the deliveryservice and the menuItem which is to be modified.
     * @param deliveryService is the service representing the restaurant.
     * @param menuItem is the menuItem represented by this pane.
     */
    public void init(DeliveryService deliveryService, MenuItemAdminViewModel menuItem) {
        this.deliveryService = deliveryService;
        this.menuItem = menuItem;
        baseProductDataController.fillFields(menuItem);
    }

    private void setupBaseProductDataPane() {
        FXMLLoader baseProductDataPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/BaseProductDataSpecification/base_product_data_pane.fxml"));
        try {
            baseProductDataPane = baseProductDataPaneLoader.load();
            baseProductDataController = baseProductDataPaneLoader.getController();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /** When the modifyButton is clicked, the newly specified data of the item is saved. */
    public void onModifyButtonClicked() {
        if (menuItem.getTitle().equals(baseProductDataController.nameTextField.getText()) ||
                !deliveryService.existsProductWithTitle(baseProductDataController.nameTextField.getText())) {
            ValidationResult<BaseProduct> validationResult =
                    baseProductValidator.validate(baseProductDataController.nameTextField.getText(),
                            Integer.parseInt(baseProductDataController.caloriesTextField.getText()),
                            Integer.parseInt(baseProductDataController.proteinsTextField.getText()),
                            Integer.parseInt(baseProductDataController.fatsTextField.getText()),
                            Integer.parseInt(baseProductDataController.sodiumTextField.getText()),
                            Double.parseDouble(baseProductDataController.priceTextField.getText()),
                            baseProductDataController.availableRadiuButton.isSelected());
            if (validationResult.isValid()) {
                deliveryService.modifyBaseProduct(menuItem.getTitle(),
                        baseProductDataController.nameTextField.getText(),
                        Integer.parseInt(baseProductDataController.caloriesTextField.getText()),
                        Integer.parseInt(baseProductDataController.proteinsTextField.getText()),
                        Integer.parseInt(baseProductDataController.fatsTextField.getText()),
                        Integer.parseInt(baseProductDataController.sodiumTextField.getText()),
                        Double.parseDouble(baseProductDataController.priceTextField.getText()),
                        baseProductDataController.availableRadiuButton.isSelected()
                );
                onSuccessfulProductModification(baseProductDataController.nameTextField.getText());
            } else {
                AlertFactory.showInvalidDataError(validationResult);
            }
        } else {
            AlertFactory.showDuplicateProductNameError(baseProductDataController.nameTextField.getText());
        }
    }

    private void onSuccessfulProductModification(String productName) {
        ((Stage) this.dataPane.getScene().getWindow()).close();
        Alert successAlert = new Alert(Alert.AlertType.WARNING);
        successAlert.setTitle("Success");
        successAlert.setHeaderText("Product " + productName + " successfully modified and the new" +
                " data is saved in the menu!");
        successAlert.show();
    }
}
