package Presenter.AdminView.ProductCreationModification;

import BLL.DeliveryService.DeliveryService;
import BLL.Validators.BaseProductValidation.BaseProductValidator;
import BLL.Validators.CompositeProductValidator.CompositeProductValidator;
import BLL.Validators.ValidationResult;
import Model.Products.BaseProduct;
import Model.Products.CompositeProduct;
import Presenter.AdminView.ProductCreationModification.BaseProductDataSepcification.BaseProductDataController;
import Presenter.AdminView.ProductCreationModification.CompositeProductDataSepcification.CompositeProductDataController;
import Presenter.Util.AlertFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller for the pane which allows the user to create a ne wproduct.
 */
public class CreateProductController {

    @FXML
    private Button createButton;

    @FXML
    private RadioButton compositeProductRadioButton;

    @FXML
    private RadioButton baseProductRadioButton;

    @FXML
    private AnchorPane dataPane;

    private AnchorPane baseProductDataPane;

    private AnchorPane compositeProductDataPane;

    private DeliveryService deliveryService;

    private BaseProductDataController baseProductDataController;

    private CompositeProductDataController compositeProductDataController;

    private final BaseProductValidator baseProductValidator = new BaseProductValidator();

    private final CompositeProductValidator compositeProductValidator = new CompositeProductValidator();

    /** Setup pane. */
    @FXML
    public void initialize() {
        ToggleGroup toggleGroup = new ToggleGroup();
        compositeProductRadioButton.setToggleGroup(toggleGroup);
        baseProductRadioButton.setToggleGroup(toggleGroup);
        toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle t1) {
                if (t1.equals(baseProductRadioButton)) {
                    dataPane.getChildren().clear();
                    dataPane.getChildren().add(baseProductDataPane);
                } else {
                    dataPane.getChildren().clear();
                    dataPane.getChildren().add(compositeProductDataPane);
                }
            }
        });
    }

    /**
     * Initializes the delivery service.
     * @param deliveryService is the service representing the restaurant.
     */
    public void initDeliveryService(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
        setupBaseProductDataPane();
        setupCompositeProductDataPane();
        dataPane.getChildren().add(baseProductDataPane);
    }

    /**
     * When the button is clicked, a new product is created based on the data specified by the user.
     */
    public void onCreateButtonClicked() {
        if (baseProductRadioButton.isSelected()) {
            createBaseProduct();
        } else {
            createCompositeProduct();
        }
    }

    private void setupBaseProductDataPane() {
        FXMLLoader baseProductDataPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/BaseProductDataSpecification/base_product_data_pane.fxml"));
        try {
            baseProductDataPane = baseProductDataPaneLoader.load();
            baseProductDataController = baseProductDataPaneLoader.getController();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void setupCompositeProductDataPane() {
        FXMLLoader compositeProductDataPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/CompositeProductDataSpecification/composite_product_data_pane.fxml"));
        try {
            compositeProductDataPane = compositeProductDataPaneLoader.load();
            compositeProductDataController = compositeProductDataPaneLoader.getController();
            compositeProductDataController.initDeliveryService(deliveryService);
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    private void createBaseProduct() {
        if (!deliveryService.existsProductWithTitle(baseProductDataController.nameTextField.getText())) {
            ValidationResult<BaseProduct> validationResult =
                    baseProductValidator.validate(baseProductDataController.nameTextField.getText(),
                            Integer.parseInt(baseProductDataController.caloriesTextField.getText()),
                            Integer.parseInt(baseProductDataController.proteinsTextField.getText()),
                            Integer.parseInt(baseProductDataController.fatsTextField.getText()),
                            Integer.parseInt(baseProductDataController.sodiumTextField.getText()),
                            Double.parseDouble(baseProductDataController.priceTextField.getText()),
                            baseProductDataController.availableRadiuButton.isSelected());
            if (validationResult.isValid()) {
                deliveryService.createBaseProduct(baseProductDataController.nameTextField.getText(),
                        Integer.parseInt(baseProductDataController.caloriesTextField.getText()),
                        Integer.parseInt(baseProductDataController.proteinsTextField.getText()),
                        Integer.parseInt(baseProductDataController.fatsTextField.getText()),
                        Integer.parseInt(baseProductDataController.sodiumTextField.getText()),
                        Double.parseDouble(baseProductDataController.priceTextField.getText()),
                        baseProductDataController.availableRadiuButton.isSelected()
                );
                onSuccessfulProductCreation(baseProductDataController.nameTextField.getText());
            } else {
                AlertFactory.showInvalidDataError(validationResult);
            }
        } else {
            AlertFactory.showDuplicateProductNameError(baseProductDataController.nameTextField.getText());
        }
    }

    private void createCompositeProduct() {
        if (!deliveryService.existsProductWithTitle(compositeProductDataController.nameTextField.getText())) {
            ValidationResult<CompositeProduct> validationResult =
                    compositeProductValidator.validate(compositeProductDataController.nameTextField.getText(),
                            compositeProductDataController.availableRadioButton.isSelected());
            if (validationResult.isValid()) {
                if (compositeProductDataController.getSelectedComponentNames().size() > 0) {
                    deliveryService.createCompositeProduct(
                            compositeProductDataController.nameTextField.getText(),
                            compositeProductDataController.getSelectedComponentNames(),
                            compositeProductDataController.availableRadioButton.isSelected());
                    onSuccessfulProductCreation(compositeProductDataController.nameTextField.getText());
                } else {
                    AlertFactory.showMissingCOmponentsForCompositeProductError();
                }
            } else {
                AlertFactory.showInvalidDataError(validationResult);
            }
        } else {
            AlertFactory.showDuplicateProductNameError(compositeProductDataController.nameTextField.getText());
        }
    }

    private void onSuccessfulProductCreation(String productName) {
        ((Stage) this.dataPane.getScene().getWindow()).close();
        Alert successAlert = new Alert(Alert.AlertType.WARNING);
        successAlert.setTitle("Success");
        successAlert.setHeaderText("Product " + productName + " successfully added to the menu");
        successAlert.show();
    }
}
