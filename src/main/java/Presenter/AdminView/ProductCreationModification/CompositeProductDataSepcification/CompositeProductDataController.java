package Presenter.AdminView.ProductCreationModification.CompositeProductDataSepcification;

import BLL.DeliveryService.IDeliveryService;
import BLL.Filters.KeywordFilter;
import BLL.Filters.MenuItemFilter;
import BLL.Filters.NoComponentFilter;
import Presenter.AdminView.MenuItemAdminViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for the pane for specifying the data of a base product.
 */
public class CompositeProductDataController {
    /** Input field for specifying the name of the menu item. */
    @FXML
    public TextField nameTextField;

    /** Input field for specifying the availability of the menu item. */
    @FXML
    public RadioButton availableRadioButton;

    @FXML
    private ListView<MenuItemAdminViewModel> selectedComponentsList;

    @FXML
    private ListView<MenuItemAdminViewModel> possibleComponentsList;

    @FXML
    private TextField keywordsTextField;

    @FXML
    private Label totalPriceLabel;

    private MenuItemAdminViewModel menuItem;

    private ObservableList<MenuItemAdminViewModel> possibleComponents;

    private ObservableList<MenuItemAdminViewModel> selectedComponents;

    private IDeliveryService deliveryService;

    /** Setup pane. */
    @FXML
    public void initialize() {
        possibleComponents = FXCollections.observableArrayList();
        selectedComponents = FXCollections.observableArrayList();
        selectedComponentsList.setCellFactory(string -> new ComponentCell("Remove", s -> selectedComponents.remove(s)));
        possibleComponentsList.setCellFactory(string -> new ComponentCell("Add", s -> selectedComponents.add(s)));
        selectedComponentsList.setItems(selectedComponents);
        possibleComponentsList.setItems(possibleComponents);
        selectedComponents.addListener((ListChangeListener<MenuItemAdminViewModel>) change -> onSelectedComponentsChanged());
    }

    /**
     * Sets the delivery service.
     * @param deliveryService representing the restaurant.
     */
    public void initDeliveryService(IDeliveryService deliveryService) {
        this.deliveryService = deliveryService;
        onApplyFiltersButtonClicked();
    }

    /**
     * Sets the data of the pane to the one of menuItem.
     * @param menuItem is the item represented by the pane.
     */
    public void initMenuItem(MenuItemAdminViewModel menuItem) {
        this.menuItem = menuItem;
        fillFields();
    }

    /**
     * Returns the list of selected components for the menuItem.
     * @return the list of selected components.
     */
    public List<String> getSelectedComponentNames() {
        return selectedComponents
                .stream()
                .map(MenuItemAdminViewModel::getTitle).collect(Collectors.toList());
    }

    private void onSelectedComponentsChanged() {
        totalPriceLabel.setText(String.valueOf(MenuItemAdminViewModel.getTotalPrice(selectedComponents)));
    }

    private void fillFields() {
        nameTextField.setText(menuItem.getTitle());
        nameTextField.setPromptText(menuItem.getTitle());
        availableRadioButton.setSelected(menuItem.isAvailable());
        selectedComponents.addAll(menuItem.getComponents().stream()
                .map(name -> deliveryService.getProductWithTitle(name).get())
                .collect(Collectors.toList()));
    }

    /**
     * When the filters button is clicked, the new keyword fliter is applied and the filtered
     * items form the deliveryService are displayed.
     */
    public void onApplyFiltersButtonClicked() {
        String[] keywords = keywordsTextField.getText().split(" ");
        List<MenuItemFilter> menuItemFilters = new ArrayList<>();
        for (String keyword : keywords) {
            menuItemFilters.add(new KeywordFilter(keyword));
        }
        if (this.menuItem != null) {
            menuItemFilters.add(new NoComponentFilter(menuItem.getTitle())); //avoid self-dependency
        }
        List<MenuItemAdminViewModel> possibleItems =
                deliveryService.getFilteredMenuItems(menuItemFilters);
        possibleComponents.clear();
        possibleComponents.addAll(possibleItems);
    }
}
