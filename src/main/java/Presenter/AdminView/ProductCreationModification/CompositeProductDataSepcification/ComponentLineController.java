package Presenter.AdminView.ProductCreationModification.CompositeProductDataSepcification;

import Presenter.AdminView.MenuItemAdminViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.util.function.Consumer;

/**
 * Controller for a pane representing the component of a composite product.
 */
public class ComponentLineController {

    @FXML
    private Label componentNameLabel;

    @FXML
    private Button addRemoveButton;

    /**
     * Initializes the content of the pane.
     * @param component is the component represented by the pane.
     * @param buttonText is the text displayed on the button of the pane.
     * @param onButtonClick is the action that takes place when the button is clicked.
     */
    public void init(MenuItemAdminViewModel component, String buttonText,
                     Consumer<MenuItemAdminViewModel> onButtonClick) {
        componentNameLabel.setText(component.getTitle());
        addRemoveButton.setText(buttonText);
        addRemoveButton.setOnAction(actionEvent -> onButtonClick.accept(component));
    }
}
