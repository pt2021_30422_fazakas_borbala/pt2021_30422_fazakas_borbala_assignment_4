package Presenter.AdminView.ProductCreationModification.CompositeProductDataSepcification;

import BLL.DeliveryService.DeliveryService;
import BLL.Validators.CompositeProductValidator.CompositeProductValidator;
import BLL.Validators.ValidationResult;
import Model.Products.CompositeProduct;
import Presenter.AdminView.MenuItemAdminViewModel;
import Presenter.Util.AlertFactory;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller for the pane which allows the admin to modify the data of a composte product.
 */
public class ModifyCompProductController {
    @FXML
    private AnchorPane dataPane;

    private AnchorPane compositeProductDataPane;

    private DeliveryService deliveryService;

    private CompositeProductDataController compositeProductDataController;

    private final CompositeProductValidator compositeProductValidator = new CompositeProductValidator();

    private MenuItemAdminViewModel menuItem;

    /** Setup pane. */
    @FXML
    public void initialize() {
        setupCompositeProductDataPane();
        dataPane.getChildren().add(compositeProductDataPane);
    }

    /**
     * Initializez the content of the pane and the service to communicate with.
     * @param deliveryService is the service representing the restaurant.
     * @param menuItem is the item to modify.
     */
    public void init(DeliveryService deliveryService, MenuItemAdminViewModel menuItem) {
        this.deliveryService = deliveryService;
        this.menuItem = menuItem;
        compositeProductDataController.initDeliveryService(deliveryService);
        compositeProductDataController.initMenuItem(menuItem);
    }

    private void setupCompositeProductDataPane() {
        FXMLLoader compositeProductDataPaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/CompositeProductDataSpecification/composite_product_data_pane.fxml"));
        try {
            compositeProductDataPane = compositeProductDataPaneLoader.load();
            compositeProductDataController = compositeProductDataPaneLoader.getController();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /**
     * When the modifyButton is clicked, the newly specified data of the item is validated, and if
     * it is correct, it is saved.
     */
    public void onModifyButtonClicked() {
        if (menuItem.getTitle().equals(compositeProductDataController.nameTextField.getText()) ||
                !deliveryService.existsProductWithTitle(compositeProductDataController.nameTextField.getText())) {
            ValidationResult<CompositeProduct> validationResult =
                    compositeProductValidator.validate(compositeProductDataController.nameTextField.getText(),
                            compositeProductDataController.availableRadioButton.isSelected());
            if (validationResult.isValid()) {
                if (compositeProductDataController.getSelectedComponentNames().size() > 0) {
                    deliveryService.modifyCompositeProduct(menuItem.getTitle(),
                            compositeProductDataController.nameTextField.getText(),
                            compositeProductDataController.getSelectedComponentNames(),
                            compositeProductDataController.availableRadioButton.isSelected());
                    onSuccessfulProductModification(compositeProductDataController.nameTextField.getText());
                } else {
                    AlertFactory.showMissingCOmponentsForCompositeProductError();
                }
            } else {
                AlertFactory.showInvalidDataError(validationResult);
            }
        } else {
            AlertFactory.showDuplicateProductNameError(compositeProductDataController.nameTextField.getText());
        }
    }

    private void onSuccessfulProductModification(String productName) {
        ((Stage) this.dataPane.getScene().getWindow()).close();
        Alert successAlert = new Alert(Alert.AlertType.WARNING);
        successAlert.setTitle("Success");
        successAlert.setHeaderText("Product " + productName + " successfully modified and the new" +
                " data is saved in the menu!");
        successAlert.show();
    }
}
