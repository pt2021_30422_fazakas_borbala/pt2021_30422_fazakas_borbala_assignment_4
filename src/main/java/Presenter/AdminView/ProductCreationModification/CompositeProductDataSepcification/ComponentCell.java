package Presenter.AdminView.ProductCreationModification.CompositeProductDataSepcification;

import Presenter.AdminView.MenuItemAdminViewModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * List Cell representing a component from a composite product.
 */
public class ComponentCell extends ListCell<MenuItemAdminViewModel> {
    private final AnchorPane componentPane;
    private final ComponentLineController componentLineController;
    private final Consumer<MenuItemAdminViewModel> onButtonClick;
    private final String buttonText;

    /**
     * Creates a list cell.
     * @param buttonText is the text displayed on the button of the cell.
     * @param onButtonClick is the action which takes place when the button of the cell is clicked.
     */
    public ComponentCell(String buttonText, Consumer<MenuItemAdminViewModel> onButtonClick) {
        super();
        AnchorPane componentPane1;
        this.onButtonClick = onButtonClick;
        this.buttonText = buttonText;

        FXMLLoader componentLinePaneLoader = new FXMLLoader(getClass().getResource(
                "/AdminView/ProductCreationModification/CompositeProductDataSpecification/component_line.fxml"));
        try {
            componentPane1 = componentLinePaneLoader.load();
        } catch (IOException e) {
            componentPane1 = null;
            e.printStackTrace();
        }
        componentPane = componentPane1;
        componentLineController = componentLinePaneLoader.getController();
        setText(null);
    }


    /**
     * Updates the data of the cell with the data of a menuitem.
     * @param item is the item represented by the cell.
     * @param empty shows whether the cell is empty.
     */
    @Override
    public void updateItem(final MenuItemAdminViewModel item, boolean empty) {
        super.updateItem(item, empty);
        if (item != null) {
            setEditable(false);
            componentLineController.init(item, buttonText, onButtonClick);
            setGraphic(componentPane);
        } else {
            setGraphic(null);
        }
    }
}