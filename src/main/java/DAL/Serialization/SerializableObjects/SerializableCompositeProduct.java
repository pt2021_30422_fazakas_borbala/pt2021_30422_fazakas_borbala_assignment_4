package DAL.Serialization.SerializableObjects;

import Model.Products.CompositeProduct;
import Model.Products.MenuItem;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A serializable form of a composite product, containing a list of component names instead of
 * the actual components.
 */
public class SerializableCompositeProduct {
    private final int id;
    private final String title;
    private final boolean available;
    private final List<String> componentNames;
    private final Double rating;

    private SerializableCompositeProduct(@JsonProperty("id") int id,
                                         @JsonProperty("title") String title,
                                         @JsonProperty("available") boolean available,
                                         @JsonProperty("componentNames") List<String> componentNames,
                                         @JsonProperty("rating") Double rating) {
        this.id = id;
        this.title = title;
        this.available = available;
        this.componentNames = componentNames;
        this.rating = rating;
    }

    /**
     * Builds a serializable composite product from a composite product.
     *
     * @param compositeProduct is the product to bring in a serializable form.
     * @return the serializable composite product.
     */
    public static SerializableCompositeProduct generateSerializableCompositeProduct(CompositeProduct compositeProduct) {
        List<String> componentNames =
                compositeProduct.getComponents().stream().map(MenuItem::getTitle).collect(Collectors.toList());
        return new SerializableCompositeProduct(
                compositeProduct.getId(),
                compositeProduct.getTitle(),
                compositeProduct.isAvailable(), componentNames,
                compositeProduct.getRating().orElse(null));
    }

    /**
     * Generates an empty composite product from the deserialized data, but without any components.
     *
     * @return the generates composite product.
     */
    public CompositeProduct generateEmptyCompositeProduct() {
        return new CompositeProduct(id, title, rating, available, new ArrayList<>());
    }
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAvailable() {
        return available;
    }

    public List<String> getComponentNames() {
        return componentNames;
    }

    public Double getRating() {
        return rating;
    }
}
