package DAL.Serialization.SerializableObjects;

import Model.Orders.Order;
import Model.Products.MenuItem;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A serializable form of an order, containing a list of product names instead of
 * the actual ordered products.
 */
public class SerializableOrderData {
    private final int orderId;
    private final int clientId;
    private final LocalDateTime dateTime;
    private final Map<String, Integer> orderedItemNamesToAmount;

    private SerializableOrderData(@JsonProperty("orderId") int orderId,
                                  @JsonProperty("clientId") int clientId,
                                  @JsonProperty("dateTime") LocalDateTime dateTime,
                                  @JsonProperty("orderedItemsNamesToAmount") Map<String, Integer> orderedItemNamesToAmount) {
        this.orderId = orderId;
        this.clientId = clientId;
        this.dateTime = Objects.requireNonNull(dateTime);
        this.orderedItemNamesToAmount = orderedItemNamesToAmount;
    }

    /**
     * Builds a serializable order from an order.
     *
     * @param order                   is the product to bring in a serializable form.
     * @param orderedProductsToAmount is the map of ordered products to the ordered quantities.
     * @return the serializable order.
     */
    public static SerializableOrderData generateSerializableOrderData(Order order, Map<MenuItem,
            Integer> orderedProductsToAmount) {
        Map<String, Integer> orderedProductNamesToAmount =
                orderedProductsToAmount.entrySet().stream()
                        .map(productToAmount -> new AbstractMap.SimpleEntry<>(productToAmount.getKey().getTitle(), productToAmount.getValue()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return new SerializableOrderData(order.getOrderId(), order.getClientId(),
                order.getDateTime(), orderedProductNamesToAmount);
    }

    /**
     * Generates an order mapped to the ordered products, based on a map of existing products.
     *
     * @param namesToMenuItems is the map of existing product names to the products.
     * @return the order mapped to the ordered products and quantities.
     */
    public AbstractMap.SimpleEntry<Order, Map<MenuItem, Integer>> generateOrderToProductsEntry(Map<String, MenuItem> namesToMenuItems) {
        Map<MenuItem, Integer> orderedItemsToAmount =
                orderedItemNamesToAmount.entrySet().stream()
                        .collect(Collectors.toMap(entry -> namesToMenuItems.get(entry.getKey()),
                                Map.Entry::getValue));
        return new AbstractMap.SimpleEntry<>(new Order(orderId, clientId, dateTime),
                orderedItemsToAmount);
    }

    public int getOrderId() {
        return orderId;
    }

    public int getClientId() {
        return clientId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Map<String, Integer> getOrderedItemNamesToAmount() {
        return orderedItemNamesToAmount;
    }
}
