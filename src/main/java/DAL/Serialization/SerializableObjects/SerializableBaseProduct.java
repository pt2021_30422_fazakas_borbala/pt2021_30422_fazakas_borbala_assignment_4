package DAL.Serialization.SerializableObjects;

import Model.Products.BaseProduct;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A serializable form of a base product.
 */
public class SerializableBaseProduct {
    private final BaseProduct baseProduct;

    private SerializableBaseProduct(@JsonProperty("id") int id,
                                    @JsonProperty("title") String title,
                                    @JsonProperty("rating") double rating,
                                    @JsonProperty("calories") int calories,
                                    @JsonProperty("proteinsAmount") int proteinsAmount,
                                    @JsonProperty("fatsAmount") int fatsAmount,
                                    @JsonProperty("sodiumAmount") int sodiumAmount,
                                    @JsonProperty("price") double price,
                                    @JsonProperty("available") boolean available) {
        this.baseProduct = new BaseProduct(id, title, rating, calories, proteinsAmount, fatsAmount,
                sodiumAmount, price, available);
    }

    /**
     * Constructs a serializable base product from a base product.
     *
     * @param baseProduct is the product to bring in a serializable form.
     */
    public SerializableBaseProduct(BaseProduct baseProduct) {
        this.baseProduct = baseProduct;
    }

    @JsonIgnore
    public BaseProduct getBaseProduct() {
        return baseProduct;
    }

    public int getId() {
        return baseProduct.getId();
    }

    public String getTitle() {
        return baseProduct.getTitle();
    }

    public double getRating() {
        return baseProduct.getRating().orElse(null);
    }

    public int getCalories() {
        return baseProduct.getCalories();
    }

    public int getProteinsAmount() {
        return baseProduct.getProteinsAmount();
    }

    public int getFatsAmount() {
        return baseProduct.getFatsAmount();
    }

    public int getSodiumAmount() {
        return baseProduct.getSodiumAmount();
    }

    public double getPrice() {
        return baseProduct.getPrice();
    }

    public boolean isAvailable() {
        return baseProduct.isAvailable();
    }
}
