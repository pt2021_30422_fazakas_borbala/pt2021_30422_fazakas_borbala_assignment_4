package DAL.Serialization.SerializableObjects;

import Model.Users.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A serializable form of a user.
 */
public class SerializableUser {
    private final User user;

    private SerializableUser(@JsonProperty("id") int id,
                             @JsonProperty("userName") String userName,
                             @JsonProperty("password") String password,
                             @JsonProperty("role") User.UserType role) {
        this.user = new User(id, userName, password, role);
    }

    /**
     * Constructs a serializable user from a base user.
     *
     * @param user is the user to bring in a serializable form.
     */
    public SerializableUser(User user) {
        this.user = user;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public int getId() {
        return user.getId();
    }

    public String getUserName() {
        return user.getUserName();
    }

    public String getPassword() {
        return user.getPassword();
    }

    public User.UserType getRole() {
        return user.getType();
    }
}
