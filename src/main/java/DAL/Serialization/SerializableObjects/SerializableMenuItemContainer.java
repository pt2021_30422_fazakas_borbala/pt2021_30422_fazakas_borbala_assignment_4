package DAL.Serialization.SerializableObjects;

import Model.Products.BaseProduct;
import Model.Products.CompositeProduct;
import Model.Products.MenuItem;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Contains the data of serializable base products and composite products, and manager their
 * reconstruction after deserialization.
 */
public class SerializableMenuItemContainer {
    private final List<SerializableBaseProduct> serializableBaseProducts;
    private final List<SerializableCompositeProduct> serializableCompositeProducts;

    /**
     * Constructs an item container.
     *
     * @param items is the list of items to contain.
     */
    public SerializableMenuItemContainer(List<MenuItem> items) {
        serializableBaseProducts = new ArrayList<>();
        serializableCompositeProducts = new ArrayList<>();
        items.forEach(item -> {
            if (item instanceof BaseProduct) {
                serializableBaseProducts.add(new SerializableBaseProduct((BaseProduct) item));
            } else {
                serializableCompositeProducts.add(SerializableCompositeProduct
                        .generateSerializableCompositeProduct((CompositeProduct) item));
            }
        });
    }

    private SerializableMenuItemContainer(@JsonProperty("serializableBaseProducts") List<SerializableBaseProduct> baseProducts,
                                          @JsonProperty("serializableCompositeProducts") List<SerializableCompositeProduct> compositeProducts) {
        this.serializableBaseProducts = baseProducts;
        this.serializableCompositeProducts = compositeProducts;
    }

    public List<SerializableBaseProduct> getSerializableBaseProducts() {
        return serializableBaseProducts;
    }

    public List<SerializableCompositeProduct> getSerializableCompositeProducts() {
        return serializableCompositeProducts;
    }

    /**
     * Rebuilds the actual menu items from their serializable forms, after deserialization. Given
     * that the serializable forms contain only component names instead of actual components for
     * composite products, this method searches for the corresponding products and rebuilds the
     * list of components.
     *
     * @return the list of menu items.
     */
    @JsonIgnore
    public Map<String, MenuItem> getMenuItems() {
        Map<String, MenuItem> result = new HashMap<>();
        serializableBaseProducts.forEach(serializableBaseProduct ->
                result.put(serializableBaseProduct.getTitle(), serializableBaseProduct.getBaseProduct()));
        serializableCompositeProducts.forEach(serializableCompositeProduct ->
                result.put(serializableCompositeProduct.getTitle(),
                        serializableCompositeProduct.generateEmptyCompositeProduct()));
        serializableCompositeProducts.forEach(serializableCompositeProduct ->
                ((CompositeProduct) result.get(serializableCompositeProduct.getTitle()))
                        .updateComponents(
                                serializableCompositeProduct
                                        .getComponentNames().stream()
                                        .map(result::get)
                                        .collect(Collectors.toList())));
        return result;
    }
}
