package DAL.Serialization;

import DAL.Serialization.SerializableObjects.SerializableMenuItemContainer;
import DAL.Serialization.SerializableObjects.SerializableOrderData;
import DAL.Serialization.SerializableObjects.SerializableUser;
import Model.Orders.Order;
import Model.Products.MenuItem;
import Model.Users.User;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;

import java.io.*;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class responsible for serializing and deserializing data.
 */
public class Serializer {
    public static final String USERS_FILE = "users.json";
    public static final String MENU_ITEMS_FILE = "menu_items.json";
    public static final String ORDERS_FILE = "orders.json";

    /**
     * Serializes a list of users and saves the data in a file.
     *
     * @param users is the list of users to serialize.
     */
    public static void serializeUsers(List<User> users) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try (OutputStream outputStream = new FileOutputStream(USERS_FILE)) {
            mapper.writeValue(outputStream,
                    users.stream()
                            .map(Serializer::getSerializableObject)
                            .collect(Collectors.toList()));
        } catch (IOException e) { //todo: exception
            e.printStackTrace();
        }
    }

    /**
     * Deserializes a list of users from a file and stores them in a map.
     *
     * @return a map of usernames to the users.
     */
    public static Map<String, User> deserializeUsers() {
        ObjectMapper mapper = new ObjectMapper();
        try (InputStream inputStream = new FileInputStream(USERS_FILE)) {
            List<SerializableUser> users = mapper.readValue(inputStream, new TypeReference<>() {
            });
            return users.stream()
                    .collect(Collectors.toMap(SerializableUser::getUserName,
                            SerializableUser::getUser));
        } catch (IOException e) { //todo: exception
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Serializes a list of menu items and saves the data in a file.
     *
     * @param menuItems is the list of menu items to serialize.
     */
    public static void serializeMenuItems(List<MenuItem> menuItems) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        SerializableMenuItemContainer container = new SerializableMenuItemContainer(menuItems);
        //todo
        try (OutputStream outputStream = new FileOutputStream(MENU_ITEMS_FILE)) {
            mapper.writeValue(outputStream, container);
        } catch (IOException e) { //todo: exception
            e.printStackTrace();
        }
    }

    /**
     * Deserializes a list of menu items from a file and stores them in a map.
     *
     * @return a map of menu item names to the menu items.
     */
    public static Map<String, MenuItem> deserializeMenuItems() {
        ObjectMapper mapper = new ObjectMapper();
        try (InputStream inputStream = new FileInputStream(MENU_ITEMS_FILE)) {
            SerializableMenuItemContainer container = mapper.readValue(inputStream,
                    SerializableMenuItemContainer.class);
            return container.getMenuItems();
        } catch (IOException e) { //todo: exception
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Serializes a map of orders to the ordered products mapped to the ordered amounts and saves
     * the data in a file.
     *
     * @param orderToMenuItemAndAmount is the map of orders to the ordered products mapped to the
     *                                 ordered amounts.
     */
    public static void serializeOrders(Map<Order, Map<MenuItem, Integer>> orderToMenuItemAndAmount) {
        JsonFactory jsonFactory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(jsonFactory);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        // StdDateFormat is ISO8601 since jackson 2.9
        mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
        mapper.findAndRegisterModules();
        List<SerializableOrderData> objectsToWrite =
                orderToMenuItemAndAmount.entrySet().stream()
                        .map(Serializer::getSerializableObject)
                        .collect(Collectors.toList());
        try (OutputStream outputStream = new FileOutputStream(ORDERS_FILE)) {
            mapper.writeValue(outputStream, objectsToWrite);
        } catch (IOException e) { //todo: exception
            e.printStackTrace();
        }
    }

    /**
     * Deserializes the data of orders previously saved in a file.
     *
     * @param namesToMenuItems is the map of the existing menu item names to the menu items,
     *                         required to rebuild the data of orders.
     * @return a map of orders to the ordered products mapped to the ordered amounts.
     */
    public static Map<Order, Map<MenuItem, Integer>> deserializeOrders(Map<String, MenuItem> namesToMenuItems) {
        JsonFactory jsonFactory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(jsonFactory);
        mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
        mapper.findAndRegisterModules();
        try (InputStream inputStream = new FileInputStream(ORDERS_FILE)) {
            List<SerializableOrderData> orderDataList = mapper.readValue(inputStream,
                    new TypeReference<>() {
                    });
            return orderDataList.stream()
                    .map(orderData -> orderData.generateOrderToProductsEntry(namesToMenuItems))
                    .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        } catch (IOException e) { //todo: exception
            e.printStackTrace();
            return null;
        }
    }

    private static SerializableUser getSerializableObject(User user) {
        return new SerializableUser(user);
    }

    private static SerializableOrderData getSerializableObject(Map.Entry<Order,
            Map<MenuItem, Integer>> orderToItemAmounts) {
        return SerializableOrderData.generateSerializableOrderData(orderToItemAmounts.getKey(),
                orderToItemAmounts.getValue());
    }
}
