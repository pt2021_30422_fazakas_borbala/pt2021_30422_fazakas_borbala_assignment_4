package DAL.BillGeneration;

import Model.Orders.Order;
import Model.Products.MenuItem;
import Model.Users.User;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * BillGenerator is reposnible for creating a bill for a specific order, in a pdf file containing
 * data about the client, the products and the total sum to pay.
 */

public class BillGenerator {
    private static final Font TITLE_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 36,
            Font.BOLD);
    private static final Font SMALL_BOLD_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    private static final Font SMALL_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 12);


    private static final String FILE_PATH_FORMAT = "Bills/Bill-%s.pdf";
    private static final String LOGO_PATH = "src/main/resources/img/restaurant_logo.png";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(
            "yyyy.MM.dd HH:mm:ss");
    private static final DateTimeFormatter FILE_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(
            "yyyy_MM_dd-HH_mm_ss");

    /**
     * Creates a pdf representing a bill for the order, sent by the client for the specified
     * some specified products.
     *
     * @param client                 is the user who placed the order.
     * @param order                  is the order that was placed.
     * @param orderedItemsToQuantity is the map of ordered products to the ordered quantities.
     */
    public static void createBill(User client, Order order,
                                  Map<MenuItem, Integer> orderedItemsToQuantity) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(String.format(FILE_PATH_FORMAT,
                    order.getDateTime().format(FILE_DATE_TIME_FORMATTER))));
            document.open();
            addMetaData(document);
            addContent(document, client, order, orderedItemsToQuantity);
            document.close();
        } catch (DocumentException | IOException e) {
            // todo LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
        }
    }

    private static void addMetaData(Document document) {
        document.addTitle("Order Bill");
        document.addSubject("Order");
        document.addAuthor("Bori");
        document.addCreator("Bori");
    }

    private static void addContent(Document document, User client, Order order,
                                   Map<MenuItem, Integer> orderedItemsToQuantity)
            throws DocumentException, IOException {
        addPrefaceParagraph(document);
        addDataParagraph(document, client, order, orderedItemsToQuantity);
        addTotalParagraph(document, order, orderedItemsToQuantity);
    }

    private static void addPrefaceParagraph(Document document) throws DocumentException, IOException {
        Paragraph preface = new Paragraph();
        Image image = Image.getInstance(LOGO_PATH);
        image.setAlignment(Element.ALIGN_CENTER);
        image.scaleAbsolute(100, 100);
        preface.add(image);
        addEmptyLine(preface, 1);
        Paragraph billPara = new Paragraph("Bill", TITLE_FONT);
        billPara.setAlignment(Element.ALIGN_CENTER);
        preface.add(billPara);
        addEmptyLine(preface, 1);
        document.add(preface);
    }

    private static void addDataParagraph(Document document, User client, Order order, Map<MenuItem,
            Integer> orderedItemsToQuantity)
            throws DocumentException {
        Paragraph dataPara = new Paragraph();
        Paragraph clientPara = new Paragraph();
        Chunk clientHeader = new Chunk("Customer: ", SMALL_BOLD_FONT);
        Chunk clientName = new Chunk(client.getUserName(), SMALL_FONT);
        clientPara.add(clientHeader);
        clientPara.add(clientName);
        dataPara.add(clientPara);
        Paragraph orderPara = new Paragraph();
        Chunk orderHeader = new Chunk("Ordered items: ", SMALL_BOLD_FONT);
        orderPara.add(orderHeader);
        addMenuItemTable(orderPara, orderedItemsToQuantity);
        dataPara.add(orderPara);
        document.add(dataPara);
    }

    private static void addTotalParagraph(Document document, Order order,
                                          Map<MenuItem, Integer> orderedItemsToQuantity) throws DocumentException {
        Paragraph totalPara = new Paragraph();
        addEmptyLine(totalPara, 2);
        Paragraph pricePara = new Paragraph("Total Price: " + Order.computePrice(orderedItemsToQuantity),
                SMALL_BOLD_FONT);
        pricePara.setAlignment(Element.ALIGN_RIGHT);
        totalPara.add(pricePara);
        addEmptyLine(totalPara, 1);
        Paragraph datePara =
                new Paragraph("Date: " + order.getDateTime().format(DATE_TIME_FORMATTER),
                        SMALL_FONT);
        datePara.setAlignment(Element.ALIGN_RIGHT);
        totalPara.add(datePara);
        document.add(totalPara);
    }

    private static void addMenuItemTable(Paragraph paragraph,
                                         Map<MenuItem, Integer> orderedItemsToQuantity) throws DocumentException {
        PdfPCell c1, c2, c3;
        PdfPTable table = new PdfPTable(3);
        table.setHeaderRows(1);
        table.setWidths(new int[]{2, 1, 1});
        c1 = new PdfPCell(new Paragraph("Ordered Product", SMALL_BOLD_FONT));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c2 = new PdfPCell(new Paragraph("Ordered Quantity", SMALL_BOLD_FONT));
        c2.setHorizontalAlignment(Element.ALIGN_CENTER);
        c3 = new PdfPCell(new Paragraph("Unit Price", SMALL_BOLD_FONT));
        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.addCell(c2);
        table.addCell(c3);
        for (MenuItem menuItem : orderedItemsToQuantity.keySet()) {
            c1 = new PdfPCell(Phrase.getInstance(menuItem.getTitle()));
            table.addCell(c1);
            c2 = new PdfPCell(Phrase.getInstance(orderedItemsToQuantity.get(menuItem).toString()));
            table.addCell(c2);
            c3 = new PdfPCell(Phrase.getInstance(String.valueOf(menuItem.computePrice())));
            table.addCell(c3);
        }
        paragraph.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

}
