package DAL.CSVReader;

import BLL.DeliveryService.DeliveryServiceProductManager;
import Model.Products.BaseProduct;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads the base items from a CSV file.
 */
public class CSVBaseItemsReader {

    /**
     * Reads and returns the base items in the specified CSV file.
     *
     * @param fileName is the name of the file to read from.
     * @return the list of base products in th file.
     */
    public static List<BaseProduct> loadBaseProducts(String fileName,
                                                     DeliveryServiceProductManager productManager) {
        List<BaseProduct> products = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(fileName))) {
            in.readLine(); //header
            String line;
            while ((line = in.readLine()) != null) {
                String[] items = line.split(",");
                if (productManager.getMenuItemsId(items[0]).isPresent()) {
                    products.add(new BaseProduct(
                            productManager.getMenuItemsId(items[0]).get(),
                            items[0],
                            Double.parseDouble(items[1]),
                            Integer.parseInt(items[2]),
                            Integer.parseInt(items[3]),
                            Integer.parseInt(items[4]),
                            Integer.parseInt(items[5]),
                            Double.parseDouble(items[6]),
                            true));
                } else {
                    products.add(new BaseProduct(
                            productManager.generateMenuItemId(),
                            items[0],
                            Double.parseDouble(items[1]),
                            Integer.parseInt(items[2]),
                            Integer.parseInt(items[3]),
                            Integer.parseInt(items[4]),
                            Integer.parseInt(items[5]),
                            Double.parseDouble(items[6]),
                            true));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return products;
    }
}
