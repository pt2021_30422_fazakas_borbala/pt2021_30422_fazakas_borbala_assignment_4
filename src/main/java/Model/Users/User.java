package Model.Users;

import java.util.Objects;

/**
 * Holds the data of an application user.
 */
public class User {
    private final int id;
    private final String userName;
    private final String password;
    private final UserType type;

    /**
     * UserType defines the role and permissions of the user in the application
     */
    public enum UserType {
        ADMIN,
        /**
         * Can manipulate products.
         */
        EMPLOYEE,
        CLIENT /** Can place orders for products. */
    }

    /**
     * Constructs a new user.
     *
     * @param id       is the id of the new user.
     * @param userName is the name of the new user.
     * @param password is the password of the new user.
     * @param type     is the type of the new user.
     */
    public User(int id, String userName, String password, UserType type) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.type = Objects.requireNonNull(type);
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public UserType getType() {
        return type;
    }

    /**
     * Compares this user to another object. They are equal if they are both users and have the
     * same id.
     *
     * @param o is the object to compare with.
     * @return true if the tqo objects are equal, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
