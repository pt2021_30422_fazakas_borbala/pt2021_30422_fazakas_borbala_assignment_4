package Model.Orders;

import Model.Products.MenuItem;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

/**
 * Holds the data of an order placed by a client.
 */
public class Order {
    private final int orderId;
    private final int clientId;
    private final LocalDateTime dateTime;

    /**
     * Builds an order.
     *
     * @param orderId  is the unique id of the order.
     * @param clientId is the id of the client who placed the order.
     * @param date     is the datetime at which the order was placed.
     */
    public Order(int orderId, int clientId, LocalDateTime date) {
        this.orderId = orderId;
        this.clientId = clientId;
        this.dateTime = Objects.requireNonNull(date);
    }

    public int getOrderId() {
        return orderId;
    }

    public int getClientId() {
        return clientId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    /**
     * Two orders are equal, if both their ids, clientIds and times are equal.
     *
     * @param o is the object to compare with.
     * @return true if this object is equal to o, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return orderId == order.orderId && clientId == order.clientId && dateTime.equals(order.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, clientId, dateTime);
    }

    /**
     * Computes the price of a list of items mapped to their quantity.
     *
     * @param itemsToQuantity is the list of items mapped to their quantity..
     * @return the total price of the products.
     */
    public static double computePrice(Map<MenuItem, Integer> itemsToQuantity) {
        return itemsToQuantity.keySet().stream().mapToDouble(item -> itemsToQuantity.get(item) * item.computePrice()).sum();
    }
}
