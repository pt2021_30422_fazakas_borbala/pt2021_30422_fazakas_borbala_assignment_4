package Model.Products;

import java.io.Serializable;

/**
 * Represents a base product not composed of any further products.
 */
public class BaseProduct extends MenuItem implements Serializable {
    private int calories;
    private int proteinsAmount;
    private int fatsAmount;
    private int sodiumAmount;
    private double price;

    /**
     * Builds a new base product.
     *
     * @param title          is the title of the new base product.
     * @param rating         is the rating of the new base product.
     * @param calories       specifies the number of calories in the new base product.
     * @param proteinsAmount specifies the amount of proteins in the new base product.
     * @param fatAmount      specifies the amount of fat in the new base product.
     * @param sodiumAmount   specifies the amount of sodium in the new base product.
     * @param price          specifies the price of the new base product.
     * @param available      specifies the availability of the new base product.
     */
    public BaseProduct(int id, String title, Double rating, int calories, int proteinsAmount,
                       int fatAmount,
                       int sodiumAmount, double price, boolean available) {
        super(id, title, available, rating);
        this.calories = calories;
        this.proteinsAmount = proteinsAmount;
        this.fatsAmount = fatAmount;
        this.sodiumAmount = sodiumAmount;
        this.price = price;
    }

    @Override
    public int getCalories() {
        return calories;
    }

    @Override
    public boolean contains(MenuItem menuItem) {
        return this.equals(menuItem);
    }

    @Override
    public boolean contains(String menuItemName) {
        return this.getTitle().equals(menuItemName);
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    @Override
    public int getProteinsAmount() {
        return proteinsAmount;
    }

    public void setProteinsAmount(int proteinsAmount) {
        this.proteinsAmount = proteinsAmount;
    }

    @Override
    public int getFatsAmount() {
        return fatsAmount;
    }

    public void setFatsAmount(int fatAmount) {
        this.fatsAmount = fatAmount;
    }

    @Override
    public int getSodiumAmount() {
        return sodiumAmount;
    }

    public void setSodiumAmount(int sodiumAmount) {
        this.sodiumAmount = sodiumAmount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double computePrice() {
        return getPrice();
    }
}
