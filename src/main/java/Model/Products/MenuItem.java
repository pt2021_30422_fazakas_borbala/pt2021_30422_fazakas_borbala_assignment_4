package Model.Products;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * General form of a menu item.
 */
public abstract class MenuItem implements Serializable {
    private final int id;
    private String title;
    private boolean available;
    private final Double rating;

    /**
     * Builds a new menu item.
     *
     * @param title     is the title of the new menu item.
     * @param available specifies the availability of the menu item to clients.
     * @param rating    specifies the rating of the menu item.
     */
    public MenuItem(int id, String title, boolean available, Double rating) {
        this.id = id;
        this.title = Objects.requireNonNull(title);
        this.available = available;
        this.rating = rating;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = Objects.requireNonNull(title);
    }

    public abstract double computePrice();

    public abstract int getProteinsAmount();

    public abstract int getSodiumAmount();

    public abstract int getFatsAmount();

    public Optional<Double> getRating() {
        return Optional.ofNullable(rating);
    }

    public abstract int getCalories();

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    /**
     * Specifies whether this item contains directly or indirectly, as component, the given
     * menuItem.
     *
     * @param menuItem is the item which is searches as a component.
     * @return true if this item contains the searched menu item, false otherwise.
     */
    public abstract boolean contains(MenuItem menuItem);

    /**
     * Specifies whether this item contains directly or indirectly, as component, a menu item
     * with the given name.
     *
     * @param menuItemName is the item's name which is searches as a component.
     * @return true if this item contains the searched menu item, false otherwise.
     */
    public abstract boolean contains(String menuItemName);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MenuItem)) return false;
        MenuItem menuItem = (MenuItem) o;
        return id == menuItem.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
