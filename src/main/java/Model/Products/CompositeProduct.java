package Model.Products;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a composite product composed of further products.
 */
public class CompositeProduct extends MenuItem implements Serializable {
    private List<MenuItem> components;

    public CompositeProduct(int id, String title, Double rating,
                            boolean available, List<MenuItem> components) {
        super(id, title, available, rating);
        this.components = components;
    }

    @Override
    public double computePrice() {
        return components.stream().mapToDouble(MenuItem::computePrice).sum();
    }

    @Override
    public int getProteinsAmount() {
        return components.stream().mapToInt(MenuItem::getProteinsAmount).sum();
    }

    @Override
    public int getSodiumAmount() {
        return components.stream().mapToInt(MenuItem::getSodiumAmount).sum();
    }

    @Override
    public int getFatsAmount() {
        return components.stream().mapToInt(MenuItem::getFatsAmount).sum();
    }

    @Override
    public int getCalories() {
        return components.stream().mapToInt(MenuItem::getCalories).sum();
    }

    @Override
    public boolean contains(MenuItem menuItem) {
        return this.equals(menuItem) || components.stream().anyMatch(component -> component.contains(menuItem));
    }

    @Override
    public boolean contains(String menuItemName) {
        return this.getTitle().equals(menuItemName) || components.stream().anyMatch(component -> component.contains(menuItemName));
    }

    public List<MenuItem> getComponents() {
        return new ArrayList<>(components);
    }

    public void updateComponents(List<MenuItem> components) {
        this.components = components;
    }

    /**
     * Specifier whether this product contains as a subcomponent (but is not equal to) the
     * searched component.
     *
     * @param searchedComponent is the component searched in the components of this item.
     * @return true if this product contains the searched component as a subcomponent, false
     * otherwise.
     */
    public boolean containsAsComponent(MenuItem searchedComponent) {
        return components.contains(searchedComponent) ||
                components.stream()
                        .filter(component -> component instanceof CompositeProduct)
                        .anyMatch(component -> ((CompositeProduct) component).containsAsComponent(searchedComponent));
    }
}
